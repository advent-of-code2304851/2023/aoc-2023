use geo::prelude::Area;
use geo::{Polygon, LineString, Coord};

use crate::get_inputs::*;

#[derive(Clone)]
#[allow(unused_variables, dead_code)]
struct Vert {
    x: f64,
    y: f64,
}
impl Vert {
    #[allow(unused_variables, dead_code)]
    pub fn display( &self ) -> String {
        let output: String = format!("x: {}, y: {}", self.x, self.y);
        return output;
    }
}

#[allow(unused_variables, dead_code)]
#[derive(Clone)]
pub struct Action {
    pub facing: char,
    pub length: i64,
}
impl Action{
    pub fn display(&self) -> String {
        let output:String = format!(" {}, {}",self.facing, self.length);
        return output;
    }
}

pub fn get_actions(filename: &str) -> Vec<Action> {
    let mut actions:Vec<Action> = Vec::new();
    let sgamedata = get_file(filename);
    for (_i, line) in sgamedata.lines().enumerate() {
        let sdata: String = line.split(' ').nth(2).unwrap().to_string();   
        let hexnum: String = sdata[2..7].to_string();
        let length = i64::from_str_radix(&hexnum, 16).expect("Failure");
        let directions: [char; 4] = ['R', 'D', 'L', 'U'];
        let face_index: usize = sdata.chars().nth(7).expect("Failed get char 7th").to_digit(10).expect("Failure get base10") as usize;
        let facing: char = directions[face_index];

        actions.push(Action { facing, length });
    }
    return actions;
}

pub fn get_verts(actions: &Vec<Action>) -> Vec<Coord> {
    let mut verts =  vec![ Coord { x: 0.0, y: 0.0 }];
    let mut x = verts.last().unwrap().x;
    let mut y = verts.last().unwrap().y;

    let actions_len = actions.len() -1;
    let mut lastface = 'U';

    for (i, action) in actions.iter().enumerate() {
        let thisface = action.facing;
        let nextface;
        if i != actions_len {
            nextface = actions[i+1].facing;
        } else {
            nextface = 'R';
        }

        let mut length = action.length as f64;
        match thisface {
            'R' => {
                match (lastface, nextface) {
                    ('U','D') => { length += 1f64 },
                    ('D','U') => { length -= 1f64 },
                    (_,_) => {},
                }
                x += length;
            },
            'D' => {
                match (lastface, nextface) {
                    ('R','L') => { length += 1f64 },
                    ('L','R') => { length -= 1f64 },
                    (_,_) => {},
                }
                y += length;
            },
            'L' => {
                match (lastface, nextface) {
                    ('D','U') => { length += 1f64 },
                    ('U','D') => { length -= 1f64 },
                    (_,_) => {},
                }
                x -= length;
            },
            'U' => {
                match (lastface, nextface) {
                    ('L','R') => { length += 1f64 },
                    ('R','L') => { length -= 1f64 },
                    (_,_) => {},
                }
                y -= length;
            },
            _ => {},
        }
        verts.push(Coord { x, y });
        lastface = thisface;
    }
    return verts;
}

pub fn polygon_area( verts: Vec<Coord>) -> f64 {
    let linestring = LineString::from(verts.clone());
    let polygon = Polygon::new(linestring, vec![]);
    let area: f64 = polygon.unsigned_area();
    return area;    
}