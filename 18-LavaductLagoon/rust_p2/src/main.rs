mod get_inputs;

mod gamedata;
use gamedata::*;

fn main() {
    //let gd = get_actions("GameData-Custom1.dat");
    //let gd = get_actions("GameData-Custom2.dat");
    //let gd = get_actions("GameData-Test.dat");
    let gd = get_actions("GameData.dat");
  
    for action in &gd {
        println!("GameData: {:?}", action.display() );
    }

    let verts = get_verts(&gd);

    for vert in &verts {
        println!(" x:{}, y:{}",vert.x, vert.y);
    }

    let area:f64 = polygon_area(verts);
    println!(" The area of this polygon is : {}", area);
}
