
pub fn get_file(filename: &str) -> String {
    use std::env;
    use std::fs;
    use std::process;
    let current_dir = env::current_dir().unwrap();
    let path = current_dir.join("..").join(filename);
    let sgamedata:String;

    if !path.exists() {
        println!("File does not exist : {}", path.display());
        process::exit(1);
    }else{
        match fs::read_to_string(path) {
            Ok(content) => {
                sgamedata = content;
            }
            Err(e) => {
                println!("Error reading game data {e}");
                process::exit(1);
            }
        }
    };
    return sgamedata
}