mod get_inputs;

mod gamedata;
use gamedata::*;

fn main() {
    //let mut gd = GameData::init_game("GameData-Test.dat");
    let mut gd = GameData::init_game("GameData.dat");
    
//    println!("{}",gd.actions_disp());
//    println!("{}",gd.map_disp());

    gd.get_loop();
    println!("{}",gd.map_disp());
    println!(" Volume of trech : {}", gd.get_volume());

    gd.area_fill();
    println!("{}",gd.map_disp());
    println!(" Volume of trech : {}", gd.get_volume());

}
