#[derive(Clone)]
pub struct Action {
    pub facing: char,
    pub steps: i32,
    pub colour: String,
}

pub fn get_actions(filename: &str) -> Vec<Action> {
    let mut actions:Vec<Action> = Vec::new();
    let sgamedata = get_file(filename);
    for line in sgamedata.lines() {
        let vline:Vec<&str> = line.split(' ').collect();   
        actions.push(Action{
            facing:  vline[0].chars().next().expect("String is empty or not a single char"),
            steps: vline[1].parse().expect("Not a valid number durring parce of string to i32"),
            colour: vline[2].to_string(),
        });
    }
    return actions;
}

fn get_file(filename: &str) -> String {
    use std::env;
    use std::fs;
    use std::process;
    let current_dir = env::current_dir().unwrap();
    let path = current_dir.join("..").join(filename);
    let sgamedata:String;

    if !path.exists() {
        println!("File does not exist : {}", path.display());
        process::exit(1);
    }else{
        match fs::read_to_string(path) {
            Ok(content) => {
                sgamedata = content;
            }
            Err(e) => {
                println!("Error reading game data {e}");
                process::exit(1);
            }
        }
    };
    return sgamedata
}