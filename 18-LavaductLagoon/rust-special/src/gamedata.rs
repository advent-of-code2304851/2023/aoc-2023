
use crate::get_inputs::*;

#[derive(Clone)]
//#[allow(unused_variables, dead_code)]
struct Loc {
    x: i32,
    y: i32,
}
impl Loc {
    pub fn display( &self ) -> String {
        let output = format!("x: {}, y: {}", self.x, self.y);
        return output;
    }
}

#[allow(unused_variables, dead_code)]
pub struct GameData {
    pub actions: Vec<Action>,
    pub map: Vec<Vec<char>>,
    max: Loc,
    start: Loc,
}

#[allow(unused_variables, dead_code)]
impl GameData {
    pub fn init_game(filename: &str) -> GameData {
        let actions: Vec<Action> = get_actions(filename);

        let mut min_x: i32 = 0;
        let mut min_y: i32 = 0;
        let mut max_x: i32 = 0;
        let mut max_y: i32 = 0;
        let mut cur_x: i32 = 0;
        let mut cur_y: i32 = 0;

        for action in &actions {
            //print!("current Loc -- x:{}, y:{}  ---  ", cur_x, cur_y);
            //print!("GameData Actions: {},{}  ---  ", action.facing, action.steps);
            
            match action.facing {
                'R' => { 
                    cur_x += action.steps;
                    if cur_x > max_x { max_x = cur_x };
                }
                'L' => {
                    cur_x -= action.steps;
                    if cur_x < min_x { min_x = cur_x }
                }
                'D' => { 
                    cur_y += action.steps;
                    if cur_y > max_y { max_y = cur_y };
                }
                'U' => {
                    cur_y -= action.steps;
                    if cur_y < min_y { min_y = cur_y }
                }
                _ => {}
            }
            //print!("min (x:{}, y:{}) --- ", min_x, min_y);
            //println!("max (x:{}, y:{})", max_x, max_y);
        }

        let start_x = min_x.abs() ;
        let start_y = min_y.abs() ;
        max_x += min_x.abs() + 1 ;
        max_y += min_y.abs() + 1 ;
 
        let start: Loc = Loc{ x : start_x, y : start_y};
        let max: Loc = Loc{x : max_x + 1, y : max_y + 1};
        let map = vec![vec!['.'; max_x as usize]; max_y as usize];

        println!("Max {}", max.display() );
        println!("Start {}", start.display() );

        return GameData { actions, map, max, start};
    }

    pub fn get_loop(&mut self) {
        let mut cur: Loc = self.start.clone();
        for action in &self.actions {
            let count = action.steps;

            //print!(" facing:{}, Steps:{} :: ", action.facing, count );
            match action.facing {
                'R' => {
                    for i in 1..=count {
                        let x = (cur.x + i) as usize;
                        let y = (cur.y) as usize;
                        self.map[y][x] = '#';
                    }   
                    cur.x += count;
                },
                'L' => {
                    for i in 1..=count {
                        let x = (cur.x - i) as usize;
                        let y = (cur.y) as usize;
                        self.map[y][x] = '#';
                    }   
                    cur.x -= count;
                },
                'D' => {
                    for i in 1..=count {
                        let x = (cur.x) as usize;
                        let y = (cur.y + i) as usize;
                        self.map[y][x] = '#';
                    }   
                    cur.y += count;
                },
                'U' => {
                    for i in 1..=count {
                        let x = (cur.x) as usize;
                        let y = (cur.y - i) as usize;
                        self.map[y][x] = '#';
                    }   
                    cur.y -= count;
                },
                _ => {},
            }
            //println!("Current Loc (x:{}, y:{})",cur.x, cur.y);
        }
    }

    pub fn area_fill(&mut self) {
        let mut cur: Loc = self.start.clone();
        for action in &self.actions {
            let steps = action.steps;

            //println!("Fill facing:{}, Steps:{} :: ", action.facing, steps );
            let inc: i32;
            match action.facing {
                'R' => {
                    cur.x += steps;
                },
                'L' => {
                    cur.x -= steps;
                },
                'D' => {
                    for i in 0..=steps {
                        let x = (cur.x) as usize;
                        let y = (cur.y + i) as usize;
                        //println!("Path - Cur:{}+{}",cur.display(), i);
                        let mut x2 = x;
                        loop {
                            x2 -= 1;
                            if self.map[y][x2] == '#' { break };
                            self.map[y][x2] = '#';
                        }
                        //println!("{}",self.map_disp());
                    }   
                    cur.y += steps;

                },
                'U' => {
                    for i in 0..=steps {
                        let x = (cur.x) as usize;
                        let y = (cur.y - i) as usize;
                        //println!("Path - Cur:{}+{}",cur.display(), i);
                        let mut x2 = x;
                        loop {
                            x2 += 1;
                            if self.map[y][x2] == '#' { break };
                            self.map[y][x2] = '#';
                        }
                        //println!("{}",self.map_disp());
                    }   
                    cur.y -= steps;
                },
                _ => {},
            }
        }    
    }

    pub fn get_volume(&self) -> usize {
        let count = &self.map.iter()
            .flat_map(|inner_vec| inner_vec.iter())
            .filter(|&&c| c == '#')
            .count();

        return *count;
    }

//----------Printers------------------------------------------------------------------------------------
    pub fn actions_disp(&self) -> String {
        let mut fmtstring = String::new();
        for action in &self.actions {
            fmtstring += &format!("{}, {}, {}\n", action.facing, action.steps, action.colour );
        }
        return fmtstring;
    }

    pub fn map_disp(&self) -> String {
        let mut fmtstring = String::new();
        for line in &self.map {
            let sline:String = line.iter().collect();
            fmtstring += &format!("{}\n", sline );
        }
        return fmtstring;
    }
}