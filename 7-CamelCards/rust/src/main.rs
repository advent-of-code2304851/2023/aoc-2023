use std::collections::HashMap;
#[derive(Clone)]
#[derive(Debug)]
struct Hand {
    cards: String,
    bid: i64,
    htype: i64,
    rank: i64,
}
    
fn main() {
    let s_hands = get_file("GameData-D7P1.txt".to_string());
    let mut hands = get_hands(s_hands);
    let mut total_winnings = 0i64;

    //std::process::exit(0);

    for hand in &mut hands {
        hand.htype = score_hand(hand.cards.clone());
    };

    hands.sort_by(|a, b| {
        let htype_order = a.htype.partial_cmp(&b.htype).unwrap();
        if htype_order == std::cmp::Ordering::Equal {
            a.cards.cmp(&b.cards)
        } else {
            htype_order
        }
    });
    
    let mut i:i64 =1;
    for hand in &mut hands {
        hand.rank = i;
        total_winnings += hand.rank * hand.bid;
        i += 1;
/*
        print!(" hand: {},\t",hand.cards);
        print!(" bid: {},\t",hand.bid);
        print!(" htype: {},\t",hand.htype);
        print!(" rank: {},",hand.rank);
        println!("hand score: {},", hand.rank * hand.bid);
 */
    };
    println!("Total Winnings : {}", total_winnings);
}

fn get_file(filename: String) -> String {
    use std::env;
    use std::fs;
    let current_dir = env::current_dir().unwrap();
    let path = current_dir.join(filename);
    let content = fs::read_to_string(path).unwrap();
    return content;
}

fn get_hands(s_hands: String) -> Vec<Hand> {
    let mut hands = Vec::new();
    for s_hand in s_hands.lines() {
        let parts: Vec<&str> =s_hand.split_ascii_whitespace().collect();
        let hand = Hand {
            cards: parts[0].to_string().replace("A","E")
                                       .replace("K","D")
                                       .replace("Q","C")
                                       .replace("J","B")
                                       .replace("T","A") ,
            bid:   parts[1].parse().unwrap_or(0),
            htype: 0i64,
            rank: 0i64,
        };
        hands.push(hand);
    }
    return hands;
}

fn score_hand(hand: String) -> i64 {
    #[derive(Debug)]
    struct Data {
        s: char,
        n: i64,
    }
    let mut htype = 0i64;
    let mut map: HashMap<char, i64> = HashMap::new();

    for ch in hand.chars() {  // I which I new how this worked !!!!!!!!!!!!!!
        let count = map.entry(ch).or_insert(0);
        *count += 1;
    }

    let mut data_vec: Vec<Data> = map.into_iter()
        .map(|(s, n)| Data { s, n })
        .collect();
    
    data_vec.sort_by(|a, b| a.n.cmp(&b.n).reverse());

    htype = match data_vec[0].n {
        a if a == 5 => { 7i64 },
        a if a == 4 => { 6i64 },
        a if a == 3 && data_vec[1].n == 2 => { 5i64 },
        a if a == 3 && data_vec[1].n == 1 => { 4i64 },
        a if a == 2 && data_vec[1].n == 2 => { 3i64 },
        a if a == 2 && data_vec[1].n == 1 => { 2i64 },
        a if a == 1 => { 1i64 },
        _ => { 0i64 },
    };

    return htype;
}



/*
Here is an example code for sorting based on an external list, rather than Alpha Numeric.  with the help of ChatGPT.
If i implemented this, I woulnt have to replace the TJQKA with ABCDE, then sort normally.
fn main() {
    let mut my_vec = vec![
        MyStruct { i64_column: 2, string_column: "9K9J5".to_string() },
        MyStruct { i64_column: 3, string_column: "3433A".to_string() },
        MyStruct { i64_column: 2, string_column: "K9Q5Q".to_string() },
        MyStruct { i64_column: 3, string_column: "JTKKK".to_string() },
        MyStruct { i64_column: 3, string_column: "J2KKK".to_string() },
    ];

    let order = ['1', '2', '3', '4', '5', '6', '7', '8', '9' ,'T', 'J', 'Q', 'K' ];

    my_vec.sort_by(|a, b| {
        if a.i64_column == b.i64_column {
            order.iter().position(|&x| x == a.string_column.chars().next().unwrap())
                .unwrap()
                .cmp(&order.iter().position(|&x| x == b.string_column.chars().next().unwrap()).unwrap())
        } else {
            a.i64_column.cmp(&b.i64_column)
        }
    });

    println!("{:?}", my_vec);
}
*/