mod get_inputs;
use get_inputs::*;

#[derive(Clone, Copy)]
struct TileLoc {
    x: usize,
    y: usize,
}

fn main() {
    //let filename = "GameData-MyTest.dat";
    //let filename = "GameData-test.dat";
    //let filename = "GameData-small.dat";
    let filename = "GameData.dat";
    let mut gamedata = get_gamedata(filename);

    prntvvc(&gamedata);
    let facing = "right";
    let cur_tile = TileLoc { x: 0, y: 0 };

    process_branch(&mut gamedata, cur_tile, facing);
    prntvvc(&gamedata);

    let activated: usize = gamedata.iter().flat_map(|tilevec| tilevec.iter()).filter(|s|s.state != ' ').count();
    println!("Total Activated Tiles : {}", activated);
}

fn prntvvc(vvc: &Vec<Vec<Tile>>) {
    for vec in vvc {
        let item:String = vec.iter().map(|s| s.item).collect();
        let state:String = vec.iter().map(|s| s.state).collect();
        println!("[{}]   [{}]", item, state);
    } 
}

#[allow(dead_code)]
fn process_branch(gamedata: &mut Vec<Vec<Tile>>, mut cur_tile: TileLoc, mut facing: &str) {
    let width =  gamedata[0].len().clone();
    let hight =  gamedata.len().clone();

    println!("=========================");
    loop {
        let tile =  &mut gamedata[cur_tile.y][cur_tile.x];
        println!("cur x : {},  cur y {}, char is {}, facing {} ", cur_tile.x, cur_tile.y, tile.item, facing);
        match tile.item {
            '.' => {  // remember to remove the forks.
                match facing {
                    "right" => { 
                        match tile.state {
                            ' '  => { tile.state = '-' }
                            '|'  => { tile.state = '+' }
                             _   => { break }  //handles + and - as well as invalid char
                        }
                        if cur_tile.x + 1 > width - 1 { break; }; 
                        cur_tile.x += 1; }

                    "left"  => { 
                        match tile.state {
                            ' '  => { tile.state = '-' }
                            '|'  => { tile.state = '+' }
                             _   => { break }  //handles + and - as well as invalid char
                        }
                        if cur_tile.x == 0 { break; }; 
                        cur_tile.x -= 1; }

                    "down"  => { 
                        match tile.state {
                            ' '  => { tile.state = '|' }
                            '-'  => { tile.state = '+' }
                             _   => { break }  //handles + and - as well as invalid char
                        }
                        if cur_tile.y + 1 > hight -1 { break; }; 
                        cur_tile.y += 1; 
                    }

                    "up"    => { 
                        match tile.state {
                            ' '  => { tile.state = '|' }
                            '-'  => { tile.state = '+' }
                             _   => { break }  //handles + and - as well as invalid char
                        }
                        if cur_tile.y == 0 { break; }; 
                        cur_tile.y -= 1; }

                    _       => {}
                }
            }
            '\\' => {
                tile.state = '+';
                match facing {
                    "right" => { 
                        println!("cur_tile.y : {}, hight : {}",cur_tile.y, hight);
                        if cur_tile.y + 1 > hight -1 { break; }; 
                        facing = "down";
                        cur_tile.y += 1; 
                    }

                    "left"  => { 
                        if cur_tile.y == 0 { break; }; 
                        facing = "up";
                        cur_tile.y -= 1; 
                    }

                    "down"  => { 
                        if cur_tile.x + 1 > width -1 { break; }; 
                        facing = "right";
                        cur_tile.x += 1; 
                    }

                    "up"    => { 
                        if cur_tile.x == 0 { break; }; 
                        facing = "left";
                        cur_tile.x -= 1; 
                    }
                    _       => {}
                }
            }

            '/' => {
                tile.state = '+';
                match facing {
                    "right" => { 
                        if cur_tile.y == 0 { break; }; 
                        facing = "up";
                        cur_tile.y -= 1; 
                    }

                    "left"  => { 
                        if cur_tile.y + 1 > hight -1 { break; }; 
                        facing = "down";
                        cur_tile.y += 1; 
                    }

                    "down"  => { 
                        if cur_tile.x == 0 { break; }; 
                        facing = "left";
                        cur_tile.x -= 1; 
                    }

                    "up"    => { 
                        if cur_tile.x + 1 > width -1 { break; }; 
                        facing = "right";
                        cur_tile.x += 1; 
                    }

                    _       => {}
                }
            }

            '|' => {
                tile.state = '+';
                match facing {
                    "down" => { 
                        if cur_tile.y + 1 > hight -1 { break; }; 
                        cur_tile.y += 1; 
                    }

                    "up" => { 
                        if cur_tile.y == 0 { break; }; 
                        cur_tile.y -= 1; 
                    }

                    "left" | "right"  => { 
                        if cur_tile.y > 0 { 
                            let new_tile = TileLoc{ x: cur_tile.x, y: cur_tile.y-1};
                            facing = "up";
                            process_branch(gamedata, new_tile, facing);
                        }
                        if cur_tile.y + 1 <= hight -1 { 
                            let new_tile = TileLoc{ x: cur_tile.x, y: cur_tile.y+1};
                            facing = "down";
                            process_branch(gamedata, new_tile, facing);
                        }; 
                    }

                    _ => {}
                }
            }    

            '-' => {
                tile.state = '+';
                match facing {
                    "right" => { 
                        if cur_tile.x + 1 > width -1 { break; }; 
                        cur_tile.x += 1; 
                    }

                    "left" => { 
                        if cur_tile.x == 0 { break; }; 
                        cur_tile.x -= 1; 
                    }

                    "up" | "down"  => { 
                        if cur_tile.x > 0 { 
                            let new_tile = TileLoc{ x: cur_tile.x-1, y: cur_tile.y};
                            facing = "left";
                            process_branch(gamedata, new_tile, facing);
                        }
                        if cur_tile.x + 1 <= width -1 { 
                            let new_tile = TileLoc{ x: cur_tile.x+1, y: cur_tile.y};
                            facing = "right";
                            process_branch(gamedata, new_tile, facing);
                        }; 
                    }

                    _ => {}
                }
            }

            _ => { break; }
        }
    };
 
}


