use std::process;

#[derive(Clone, Copy)]
pub struct Tile {
    pub item: char,
    pub state: char,
}


#[allow(dead_code)]
pub fn get_gamedata(filename: &str) -> Vec<Vec<Tile>> {
    let s_gamedata = get_file(filename);

    let mut gamedata: Vec<Vec<Tile>> = Vec::new();

    //for (i,line) in s_gamedata.split("\n").enumerate() {
    for line in s_gamedata.split("\n") {
        let mut vline:Vec<Tile> = Vec::new();
        for c in line.chars() {
            if c != '\r' {   // funcking dos   crlf. 
                let tile = Tile{ 
                    item: c,
                    state: ' ',
                };
                vline.push(tile);
            }   
        };
        gamedata.push(vline);
    }

    return gamedata;
}

pub fn get_file(filename: &str) -> String {
    use std::env;
    use std::fs;
    let current_dir = env::current_dir().unwrap();
    let path = current_dir.join("..").join(filename);
    if !path.exists() {
        println!("File does not exist : {}", path.display());
        process::exit(1);
    }else{
        let content = fs::read_to_string(path).unwrap();
        return content;
    };
}

