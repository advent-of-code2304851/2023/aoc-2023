mod get_inputs;
use get_inputs::*;

#[derive(Clone, Copy)]
struct TileLoc {
    x: usize,
    y: usize,
}

fn main() {
    //let filename = "GameData-MyTest.dat";
    let filename = "GameData-test.dat";
    //let filename = "GameData.dat";
    let mut gamedata = get_gamedata(filename);

    //println!("sGame Data : \n{:?}", gamedata);
    prntvvc(&gamedata);
    let mut facing = "right";
    let cur_tile = TileLoc { x: 0, y: 0 };

    next_tile(&mut gamedata, cur_tile, &mut facing);
    prntvvc(&gamedata);

    let activated: usize = gamedata.iter().flat_map(|tilevec| tilevec.iter()).filter(|s|s.state != ' ').count();
    println!("Total Activated Tiles : {}", activated);
}

fn prntvvc(vvc: &Vec<Vec<Tile>>) {
    for vec in vvc {
        let item:String = vec.iter().map(|s| s.item).collect();
        let state:String = vec.iter().map(|s| s.state).collect();
        println!("[{}]   [{}]", item, state);
    } 
}


fn next_tile(gamedata: &mut Vec<Vec<Tile>>, mut cur_tile: TileLoc, mut facing: &str) {
    let width =  gamedata[0].len().clone();
    let hight =  gamedata.len().clone();
    let tile =  &mut gamedata[cur_tile.y][cur_tile.x];
    //tile.state='#';
    //let mut new_tile = TileLoc { x: 0, y:0 };
    println!("{} at x:{}, y:{}",tile.item, cur_tile.x, cur_tile.y);
    match tile.item {
        '.' => {

            match facing {
                "right" => {
                    if cur_tile.x < width-1 && tile.state != '-' && tile.state != '+' {
                        cur_tile.x += 1;
                        if tile.state == '|' {tile.state = '+'} else {tile.state='-'};
                        next_tile(gamedata, cur_tile, facing);
                    } else {
                        if tile.state == '|' {tile.state = '+'} else {tile.state='-'};
                    }
                }
                "left" => {
                    if cur_tile.x > 0 && tile.state != '-' && tile.state != '+' {
                        if tile.state == '|' {tile.state = '+'} else {tile.state='-'};
                        cur_tile.x -= 1;
                        next_tile(gamedata, cur_tile, facing);
                    } else {
                        if tile.state == '|' {tile.state = '+'} else {tile.state='-'};
                    }
                }    
                "down" => {
                    if cur_tile.y < hight-1 && tile.state != '|' && tile.state != '+' {
                        if tile.state == '-' {tile.state = '+'} else {tile.state='|'};
                        cur_tile.y += 1;
                        next_tile(gamedata, cur_tile, facing);
                    } else {
                        if tile.state == '-' {tile.state = '+'} else {tile.state='|'};
                    }
                }
                "up" => {
                    if cur_tile.y > 0 && tile.state != '|' && tile.state != '+' {
                        if tile.state == '-' {tile.state = '+'} else {tile.state='|'};
                        cur_tile.y -= 1;
                        next_tile(gamedata, cur_tile, facing);
                    } else {
                        if tile.state == '-' {tile.state = '+'} else {tile.state='|'};
                    }
                }
                &_ => {}               
            }
        }
        '|' => {
            tile.state='+';
            match facing {
                "right" | "left" => { // will turn up and down.  so check y > 0, and y < gamedata.len()
                    if cur_tile.y < hight-1 {
                        let new_tile = TileLoc{ x: cur_tile.x, y: cur_tile.y+1};
                        facing = "down";
                        next_tile(gamedata, new_tile, facing);
                    }
                    if cur_tile.y > 0 {
                        let new_tile = TileLoc{ x: cur_tile.x, y: cur_tile.y-1};
                        facing = "up";
                        next_tile(gamedata, new_tile, facing);
                    } 
                }
                "down" => {
                    if cur_tile.y < hight-1 {
                        cur_tile.y += 1;
                        next_tile(gamedata, cur_tile, facing);
                    } 
                }
                "up" => {
                    if cur_tile.y > 0 {
                        cur_tile.y -= 1;
                        next_tile(gamedata, cur_tile, facing);
                    } 
                }
                &_ => {}               
            }
        }
        '-' => {
            tile.state='+';
            match facing {
                "up" | "down" => { 
                    if cur_tile.x > 0 {  
                        let new_tile = TileLoc{ x: cur_tile.x-1, y: cur_tile.y};
                        facing = "left";
                        next_tile(gamedata, new_tile, facing);
                    } 
                    if cur_tile.x < width-1 {
                        let new_tile = TileLoc{ x: cur_tile.x+1, y: cur_tile.y};
                        facing = "right";
                        next_tile(gamedata, new_tile, facing);
                    }
                }
                "right" => {
                    if cur_tile.y < hight-1 {
                        cur_tile.x += 1;
                        next_tile(gamedata, cur_tile, facing);
                    } 
                }
                "left" => {
                    if cur_tile.y > 0 {
                        cur_tile.x -= 1;
                        next_tile(gamedata, cur_tile, facing);
                    } 
                }
                &_ => {}               
            }
       }
        '/' => {
            tile.state='+';
            match facing {
                "right" => {
                    if cur_tile.y > 0 {
                        cur_tile.y -= 1;
                        facing = "up";
                        next_tile(gamedata, cur_tile, facing);
                    } 
                }
                "left" => {
                    if cur_tile.y < hight-1 {
                        cur_tile.y += 1;
                        facing = "down";
                        next_tile(gamedata, cur_tile, facing);
                    } 
                }    
                "down" => {
                    if cur_tile.x > 0 {
                        cur_tile.x -= 1;
                        facing = "left";
                        next_tile(gamedata, cur_tile, facing);
                    } 
                }
                "up" => {
                    if cur_tile.x < width-1 {
                        cur_tile.x += 1;
                        facing = "right";
                        next_tile(gamedata, cur_tile, facing);
                    } 
                }
                &_ => {}               
            }
        }
        '\\' => {
            tile.state='+';
            println!("{} at x:{}, y:{}",tile.item, cur_tile.x, cur_tile.y);
            match facing {
                "right" => {
                    if cur_tile.y < hight-1 {
                        cur_tile.y += 1;
                        facing = "down";
                        next_tile(gamedata, cur_tile, facing);
                    } 
                }
                "left" => {
                    if cur_tile.y > 0 {
                        cur_tile.y -= 1;
                        facing = "up";
                        next_tile(gamedata, cur_tile, facing);
                    } 
                }    
                "down" => {
                    if cur_tile.x < width-1 {
                        cur_tile.x += 1;
                        facing = "right";
                        next_tile(gamedata, cur_tile, facing);
                    } 
                }
                "up" => {
                    if cur_tile.x > 0 {
                        cur_tile.x -= 1;
                        facing = "left";
                        next_tile(gamedata, cur_tile, facing);
                    } 
                }
                &_ => {}               
            }

        }
        _ => {}
    }
    
}