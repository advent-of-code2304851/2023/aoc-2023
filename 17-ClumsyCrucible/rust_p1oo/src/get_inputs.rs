pub fn get_gamedata(filename: &str) -> Vec<Vec<u8>> {
    let mut gamedata:Vec<Vec<u8>> = Vec::new();
    let sgamedata = get_file(filename);
    for line in sgamedata.lines() {
        let vline:Vec<u8>;
        vline = line.chars().map(|c| c.to_digit(10).unwrap() as u8).collect();
        gamedata.push(vline);
    }
    return gamedata
}

fn get_file(filename: &str) -> String {
    use std::env;
    use std::fs;
    use std::process;
    let current_dir = env::current_dir().unwrap();
    let path = current_dir.join("..").join(filename);
    let sgamedata:String;

    if !path.exists() {
        println!("File does not exist : {}", path.display());
        process::exit(1);
    }else{
        match fs::read_to_string(path) {
            Ok(content) => {
                sgamedata = content;
            }
            Err(e) => {
                println!("Error reading game data {e}");
                process::exit(1);
            }
        }
    };
    return sgamedata
}