pub fn next_block(&mut self) {
    loop {
        let branch_len = self.branch.len();
        let thisfacing: u8 = self.branch[branch_len-1].facing;

        // if 3 faces in a row, remove that face so you don't go more than 3 in a row.
        if branch_len > 2 {
            if (thisfacing == self.branch[branch_len-2].facing) && (thisfacing == self.branch[branch_len-3].facing) {
                let _ = &mut self.branch[branch_len-1].faces.retain(|x| *x != thisfacing);
            }
        }

        // check if we are at the exit of the map bump best_cost if curr is better.
        // backup until curr block has at least 1 remaining face.
        if self.branch[branch_len -1].x == self.width -1 && self.branch[branch_len -1].y == self.hight -1 {
            if self.best_cost > 0 && self.curr_cost < self.best_cost {
                self.best_cost = self.curr_cost;
            };
            // call backup fn  that backs up until finding a block that has a face to try.
        };

        // if there are any faces left set facing and break
        // else if branch_len = 1 break
        //      else previouse pop current_block from branch.
        //let mut nextfacing: u8;

        // if new block is on existing path, then backup.

        //if faces > 0 move next face
        let nextfacing: u8;
        if self.branch[branch_len-1].faces.len() > 0  {
            nextfacing = self.branch[branch_len-1].faces.pop().unwrap();
        } else {
            
            if self.branch.len() == 1 {
                // there are not more moves, and back at beginning so end somehow.
                break
            } else {
                self.branch.pop();
            }
        }
            // put code here to move ahead one block in the branch

    }
}
