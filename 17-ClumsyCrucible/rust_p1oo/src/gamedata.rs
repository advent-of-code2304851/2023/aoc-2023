use crate::get_inputs::*;
//---------------------------------------------------------------
//  Block Stuff
//---------------------------------------------------------------

pub const DIRECTIONS: [&str; 4] = ["UP","RIGHT","DOWN","LEFT"];
pub const UP: u8 = 0;
pub const RIGHT: u8 = 1;
pub const DOWN: u8 = 2;
pub const LEFT: u8 = 3;

#[allow(unused_variables, dead_code)]
#[derive(Clone)]
pub struct Block {
    pub x: usize,
    pub y: usize,
    pub facing: u8,
    pub faces: Vec<u8>,
}
#[allow(unused_variables, dead_code)]
impl Block {
    pub fn disp(&self, layout: char) -> String {
        let mut sfaces = String::new();

        //let faces = self.faces.clone();

        for (i, s) in self.faces.iter().map(|u| DIRECTIONS[*u as usize]).enumerate() {
                if i == 0 { sfaces += s; } else { sfaces += &(",".to_string() + s) }
        }
        match layout {
            'h' => format!("  x:{}, y:{}, facing: {}, faces: {:?}", self.x, self.y, DIRECTIONS[self.facing as usize], sfaces),
            'v' => format!("  x:{}, y:{}\n  facing: {}\n  faces: {:?}", self.x, self.y, DIRECTIONS[self.facing as usize], sfaces),
            _ => {""}.to_string(),
        }   
    }
}

//---------------------------------------------------------------
//  GameData Stuff
//---------------------------------------------------------------
#[allow(unused_variables, dead_code)]
pub struct GameData {
    pub map:        Vec<Vec<u8>>,
    pub width:      usize,
    pub hight:      usize,
    pub branch:     Vec<Block>,
    pub best_cost:  usize,
    pub curr_cost:  usize,
}
impl GameData {
    pub fn new(filename: &str) -> GameData {
        let map = get_gamedata(filename);

        let width = map[0].len();
        let hight = map.len();
        let mut branch = Vec::new();
        branch.push( Block {
            x: 0,
            y: 0,
            facing: RIGHT,
            faces: vec![DOWN,RIGHT],
        });

        GameData {
            map,
            width,
            hight,
            branch,
            best_cost: 0,
            curr_cost: 0,
        }
    }

// ********************************************************************************************************************
    pub fn next_block(&mut self) {
    'move_test: {
        let branch_len = self.branch.len();
        let xlimit: usize = self.width;
        let ylimit: usize = self.hight;
        let next_x: usize;
        let next_y: usize;

        let curr_block = match self.branch.last_mut() { //set curr_block to the last block on the branch.
            Some(cb) => {
                cb.disp('v');
                cb
            },
            None => break 'move_test,
        };
        let this_x = curr_block.x.clone();
        let this_y = curr_block.y.clone();
        
        if this_x == xlimit-1 && this_y == ylimit-1 {  //**************************************************************************************************************** @Last Block
            self.best_cost = self.curr_cost.clone();

            println!("@ Last: {}, Curr Cost: {}, Best Cost: {}", curr_block.disp('h').clone(), self.curr_cost, self.best_cost );
            self.curr_cost -= self.map[this_y][this_x] as usize;
            _ = self.branch.pop();


            let curr_block = match self.branch.last_mut() { //set curr_block to the last block on the branch.
                Some(cb) => { cb },
                None => { break 'move_test },
            };
    
            println!("@ Last: {}, Curr Cost: {}, Best Cost: {}", curr_block.disp('h').clone(), self.curr_cost, self.best_cost );
            
            break 'move_test;

        }

        if let Some(next_from) = curr_block.faces.pop() {      // get next face from curr_block if exists.
//            println!("curr_block: {}",curr_block.disp('h'));

            let mut next_faces: Vec<u8> = Vec::new();
            match next_from {
                LEFT => {
                    next_faces.extend(vec![UP,LEFT,DOWN]);
                    next_x = this_x.clone() - 1;
                    next_y = this_y.clone();
                },
                RIGHT => {
                    next_faces.extend(vec![DOWN,RIGHT,UP]);
                    next_x = this_x.clone() + 1;
                    next_y = this_y.clone();
                },
                UP => {
                    next_faces.extend(vec![RIGHT,UP,LEFT]);
                    next_x = this_x.clone();
                    next_y = this_y.clone() - 1;
                },
                DOWN => {
                    next_faces.extend(vec![LEFT,DOWN,RIGHT]);
                    next_x = this_x.clone();
                    next_y = this_y.clone() + 1;
                },
            _ => { break 'move_test },
            }
            if next_x == 0        { let _ = &next_faces.retain(|d| *d != LEFT); };
            if next_x == xlimit-1 { let _ = &next_faces.retain(|d| *d != RIGHT); };
            if next_y == 0        { let _ = &next_faces.retain(|d| *d != UP); };
            if next_y == ylimit-1 { let _ = &next_faces.retain(|d| *d != DOWN); };
            
            if branch_len > 1 {
                if (next_from == self.branch[branch_len-1].facing.clone()) && (next_from == self.branch[branch_len-2].facing.clone()) {
                    let _ = &next_faces.retain(|d| *d != next_from);
                }                        
            }

            let block_exists = self.branch.iter().any(|s| s.x == next_x && s.y == next_y); 
            if block_exists {
                break 'move_test
            }


            let new_block_cost: usize = self.map[this_y][this_x] as usize;

            if self.best_cost == 0 || self.best_cost < self.curr_cost + self.map[this_y][this_x] as usize {
                let new_block = Block {
                    x: next_x,
                    y: next_y,
                    facing: next_from,
                    faces: next_faces,
                };
                println!("Move  : {}, Curr Cost: {}, Best Cost: {}", new_block.disp('h'), self.curr_cost, self.best_cost );

                self.branch.push( new_block);
                self.curr_cost += new_block_cost;

            } else if self.best_cost > self.curr_cost + self.map[this_y][this_x] as usize{
                break 'move_test
            }

        } else {
            println!("NoMove: {}, Curr Cost: {}, Best Cost: {}", curr_block.disp('h').clone(), self.curr_cost, self.best_cost );
            self.curr_cost -= self.map[this_y][this_x] as usize;
            _ = self.branch.pop();


            let curr_block = match self.branch.last_mut() { //set curr_block to the last block on the branch.
                Some(cb) => { cb },
                None => { break 'move_test },
            };
    
            println!("NoMove: {}, Curr Cost: {}, Best Cost: {}", curr_block.disp('h').clone(), self.curr_cost, self.best_cost );
            
            break 'move_test;
        }

    }
    }

    //fn branch_drop_last(&self) {      }
    
    //----Printers---------------------------------------------------------------------------
    pub fn out_map(&self) {
        for line in &self.map { println!("{:?}",line) };
    }

   pub fn out_branch(&self) {
        for (i, block) in &mut self.branch.iter().enumerate() {
            println!(" {} - {}", i, block.disp('h'));
        }
        println!(" --- Curr Cost: {},  Best Cost: {}",self.curr_cost, self.best_cost);
    }
}


// *******************************************************************
// 
// new version now working for:
//   pushing new block onto branch
//   limit of 3 in on direction
//   end if cross existing part of branch
//   Some elements of curr_cost implemented
//   add if no more faces, backup & decrement curr_cost

// need to add:
//   finish all curr_cost counter
//   add best_cost counter when reach map(max_y, max_x), and backup & decrement curr_cost
//   add end branch if curr_cost exceeds best_cost
