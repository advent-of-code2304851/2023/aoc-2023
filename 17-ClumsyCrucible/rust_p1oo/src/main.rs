#[allow(dead_code)] 
mod gamedata;
use gamedata::*;

mod get_inputs;

fn main() {
    let mut gd: GameData = GameData::new("GameData-Test.dat");

    gd.out_map();
   
    let blen = gd.branch.len() -1;
    let curr_block = &mut gd.branch[blen];
   
    println!("Starting Block : {}", curr_block.disp('h'));
   
    println!("\n");

    for _ in 0..160 {
        gd.next_block();
        gd.branch[gd.branch.len()-1].disp('v');
    }
    println!("Complete Branch : \n");
    //gd.out_branch();

}

