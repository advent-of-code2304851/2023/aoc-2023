#[allow(dead_code)]
fn test_bounds(usize) {
    let value: usize = 10;
    let add_value: usize = 5;
    let sub_value: usize = 15;
    let range = 0..20;

    let add_result = value.checked_add(add_value).filter(|&v| range.contains(&v));
    let sub_result = value.checked_sub(sub_value).filter(|&v| range.contains(&v));

    match add_result {
        Some(v) => println!("Addition result within range: {}", v),
        None => println!("Addition would put the value out of range!"),
    }

    match sub_result {
        Some(v) => println!("Subtraction result within range: {}", v),
        None => println!("Subtraction would put the value out of range!"),
    }
}