mod get_inputs;
//use std::option;

use get_inputs::*;


#[warn(unused_variables)]
const LEFT: u8 = 1u8;
const UP:   u8 = 2u8;
const RIGHT:u8 = 3u8;
const DOWN: u8 = 4u8;

fn main() {
    //let gamedata = get_gamedata("GameData.dat");
    let gamedata = get_gamedata("GameData-Test.dat");
    let hight = gamedata.len()-1;
    let width = gamedata[0].len()-1;

    let mut branch:Vec<Block> = Vec::new();
    let mut cur_block = Block { x: 0, y: 0 , faces: vec![ DOWN, RIGHT, UP ], facing: 3};
    let mut best_branch_cost: usize = 0;
    let mut curr_branch_cost: usize = 0;
    let _branch_cost:usize = 0;
    let _inline: u8 = 0;


    let mut temp_counter:u32 = 0;

    println!("--------------------------------------------------------------------------------------------");
    print_vec(&gamedata);
    println!("--------------------------------------------------------------------------------------------");

    loop {

        let mut longrun: u8 = 0;

        if branch.len() >= 1 {
            let bl: usize = branch.len()-1;
            if bl >= 1 && cur_block.facing == branch[bl].facing && cur_block.facing == branch[bl-1].facing {
                longrun = cur_block.facing;
            }
        }

        if longrun != 0 { cur_block.faces.retain(|&x| x != longrun) };

        if cur_block.faces.len() > 0 {
            cur_block.facing = cur_block.faces.pop().unwrap(); 
            match get_next_block(cur_block.clone(), width, hight) {
                Some( nb) => {   // I have a valid facing, and a move
                    if best_branch_cost == 0 || (best_branch_cost > 0 && best_branch_cost > curr_branch_cost + gamedata[nb.y][nb.x]) {

                        branch.push(cur_block.clone());
                        curr_branch_cost += gamedata[nb.y][nb.x];
                        print!("|| Current : x:{}, y:{}, faces:{:?}, facing:{} ", cur_block.x, cur_block.y, cur_block.faces, cur_block.facing);
                        cur_block = nb;
                        println!(" -> Next : x:{}, y:{}, faces:{:?}, curr_cost:{}, best_cost:{}", cur_block.x, cur_block.y, cur_block.faces, curr_branch_cost, best_branch_cost);
                    } else {
                        println!("### - Dead Branch - cost too high   Cur:{} ,  Best:{}", curr_branch_cost + gamedata[nb.y][nb.x], best_branch_cost);
                    }
                },

                None => {

                },
            };
        } else { // No more faces.. so backup
            curr_branch_cost -= gamedata[cur_block.y][cur_block.x];
            cur_block = branch.pop().unwrap();
            print!("*no more faces* x:{}, y:{}, faces:{:?}, facing:{}, cur_cost:{} best_cost:{}", cur_block.x, cur_block.y, cur_block.faces, cur_block.facing, curr_branch_cost, best_branch_cost);
        }; 



        // if last block... bottom right corner - to indicate branch completed, so backup branch.
        // also add a test if curr_branch is allready greater than best_branch then stop. backup. next path.
        if cur_block.x == width && cur_block.y == hight {
            if best_branch_cost == 0 || curr_branch_cost < best_branch_cost {
                best_branch_cost = curr_branch_cost;
            }
            curr_branch_cost -= gamedata[cur_block.y][cur_block.x];
            cur_block = branch.pop().unwrap();
        }

        //------escape stuff below-------
        // if back at first block and not where to go. 
        if cur_block.x == 0 && cur_block.y == 0 && cur_block.faces.len() == 0 { break };

        // Emergency runaway and Debug break condition
        temp_counter += 1; 
        if temp_counter > 100000 { break };
    }
    
    println!("\n branch path : {} - {}", curr_branch_cost, best_branch_cost);
    print_branch(&branch);
}


//----------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------



// try returning an option... if no move then return none otherwize return next_block
fn get_next_block(cur_block:Block, width:usize, hight:usize) -> Option<Block> {

    match cur_block.facing {
        UP => {
                if cur_block.y > 0 {
                    let vec = vec![ RIGHT, UP, LEFT ];
                    //print!(" longrun:{} check - vec was :{:?}",longrun, vec);
                    //vec.retain( |&x| x != longrun);
                    //println!(" - vec now :{:?}",vec);
                    Some( Block { 
                            x:      cur_block.x,
                            y:      cur_block.y - 1,
                            faces:  vec,
                            facing: cur_block.facing,
                        }
                    )
                } else { 
                    None
                }
            },

        RIGHT => {
                if cur_block.x < width { 
                    let vec = vec![ DOWN, RIGHT, UP ];
                    //print!(" longrun:{} check - vec was :{:?}",longrun,vec);
                    //vec.retain( |&x| x != longrun);
                    //println!(" - vec now :{:?}",vec);
                    Some( Block { 
                            x:      cur_block.x + 1,
                            y:      cur_block.y,
                            faces:  vec,
                            facing: cur_block.facing,
                        }
                    )
                } else { 
                    None
                }
            },

        DOWN => {
                if cur_block.y < hight { 
                    let vec = vec![ LEFT, DOWN, RIGHT ];
                    //print!(" longrun:{} check - vec was :{:?}",longrun,vec);
                    //vec.retain( |&x| x != longrun);
                    //println!(" - vec now :{:?}",vec);
                    Some( Block { 
                            x:      cur_block.x,
                            y:      cur_block.y + 1,
                            faces:  vec,
                            facing: cur_block.facing,
                        }
                    )
                } else { 
                    None
                }
            },
        
        LEFT => {
                if cur_block.x > 0 { 
                    let vec = vec![ UP, LEFT, DOWN ];
                    //print!(" longrun:{} check - vec was :{:?}",longrun,vec);
                    //vec.retain( |&x| x != longrun);
                    //println!(" - vec now :{:?}",vec);
                    Some( Block { 
                            x:      cur_block.x - 1,
                            y:      cur_block.y,
                            faces:  vec,
                            facing: cur_block.facing,
                        }
                    )
                } else { 
                    None
                }
            },
        
        _ => { None },
    }

}