#[derive(Clone)]
pub struct Block {
    pub x: usize,
    pub y: usize,
    pub faces: Vec<u8>,
    pub facing: u8,
}


pub fn get_gamedata(filename: &str) -> Vec<Vec<usize>> {
    let mut gamedata:Vec<Vec<usize>> = Vec::new();
    let sgamedata = get_file(filename);
    for line in sgamedata.lines() {
        let vline:Vec<usize>;
        vline = line.chars().map(|c| c.to_digit(10).unwrap() as usize).collect();
        gamedata.push(vline);
    }
    return gamedata
}

fn get_file(filename: &str) -> String {
    use std::env;
    use std::fs;
    use std::process;
    let current_dir = env::current_dir().unwrap();
    let path = current_dir.join("..").join(filename);
    let sgamedata:String;

    if !path.exists() {
        println!("File does not exist : {}", path.display());
        process::exit(1);
    }else{
        match fs::read_to_string(path) {
            Ok(content) => {
                sgamedata = content;
            }
            Err(e) => {
                println!("Error reading game data {e}");
                process::exit(1);
            }
        }
    };
    return sgamedata
}


pub fn print_vec(vvu: &Vec<Vec<usize>>) {
    for vec in vvu {
        let s: String = vec.iter().map(|u| u.to_string() ).collect();
        println!("[{}]", s);
    }
}

pub fn print_branch(branch: &Vec<Block>) {
    for block in branch {
        println!("x:{} y:{} faces:{:?} facing:{}", block.x, block.y, block.faces, block.facing);
    }
}
