---
title: Learning Rust using Advent Of Code 2023 problems 
author: Dave Laundry
date: January 1, 2024
editor_options:
  markdown:
    wrap: 72
# AOC-2023
## The Purpose of this repository:
	
	Firstly: I have thought for some time as a "non experienced" programmer, that I should lear to use some form of version control.
	This is mostly becuase of how frequently I change course only to decide I wish I could see my earlier code, or
	I just plane lost everything due to a power fail or such.
	
	Secondly: I was introduced to Advent of Code as I was trying to learn to code in Rust for fun, not for work.
	The main problem I have always had when trying to learn a new programming language, is thinking of problems to solve in
	order to practice coding.  I have found that Advent of Code, introduces problem (in a bit of a silly way for fun) that I would not
	have came up with on my own, forcing me to learn interesting ways to use rust to accomplish these problems.
	
	Advent of Code beutifly solves this by providing a very larg inventory of problems to solve, in coding 
	language newtral way.  Meening you can read the problems, attempt to solve them in what ever computer language you wish,
	then submit the answere to the Advento Of Code sight, to confirm success.
	GitLab not only provides the meens of backing up my files, but also, alows me to move between PCs as I work on the problems,
	and keep them all up to date.  If your here you most likely allreay understand this, but it is all part of my learning experience 
	which I am documenting here.
	
	in Gitlab, I have created a group for Advent of Code, and for 2023 I have created a repository. wich each Day/problem of 2023, a separate rust 
	folder where I store the prolbem description, the test data file, main data file, a file for my initial approach thoughts and a folder for the 
	rust project.  My intention was that if I also want to learn python or another language, or simply attempt to solve the problem in another way,
	I would just base it out of this folder creating a new rust or python sub folder for this code.

	If anyone where to stumble accros this space, and find any thing interesting, they would be welcome to it.  In the more likely case someone stumbles
	repository, and sees improvements that could be made, or solutions to some of the Day x, Part 2's that tatally sumpted me, I would very much
	appreciate their comments.  Comments could be made in either my projects "Approach-[short explanaton].txt", or even better would be a rust-alternate# 
	folder, with compilable example.  Remeber this is me documenting my learning experience, so polite critisism is welcome.

	One thing I have not done yet is try to use the testing abilities in cargo/rust.  This seems like an excelent way to confirm the testing data where 
	we know the answere, as per Advent of Codes structure.  I have never used a feature like this.  When I was young and 
	learning Basic/Paskal/Cobal/6502-6800-8086 assembly I had never heard of any automated testing features, nor version management systems like Git.














## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your filess

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/advent-of-code2304851/2023/aoc-2023.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/advent-of-code2304851/2023/aoc-2023/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

	

***

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## License
This is not an application project, but a practice space,  I do not know if there is any need at all for any license.
I consider every code here free to read or use or politely critisize.

## Project status
I'm currently seeing this as a work in progress, where there may not be an end.
once I finish Advent of Code 2023, or at least complete as many as I"m able, I will likely just move on to another year of example problems.