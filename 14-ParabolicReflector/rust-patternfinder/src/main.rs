mod get_inputs;
use get_inputs::*;


fn main() {
    let filename:String = "GameData-Results.dat".to_string();
    let results = get_gamedata(filename);
    println!(" Results : {:?}",results);

   
    //---  Start Pattern Locater Here ---
    let mut prefix:  Vec<usize> = Vec::new();
    let mut pattern: Vec<usize> = Vec::new();
    let mut buffer:  Vec<usize> = Vec::new();
   
    for (i, result) in results.iter().enumerate() {

        if i < 1 {
            buffer.push(result.clone());
        } else {
            if contains(&buffer, result.clone()) {
                prefix = buffer[0..i].to_vec();
            }
            println!("index:{} - result:{}", i, result);
        }
        
    }






}




fn contains(vec: &Vec<usize>, target: usize) -> bool {
    vec.iter().any(|&x| x == target)
}
