mod get_inputs;
use get_inputs::*;
mod processing;
use processing::*;

fn main() {
    //let filename = "GameData-P1-Test.dat".to_string();
    let filename = "GameData-P1.dat".to_string();
    let mut gamedata = get_gamedata(filename);

    //Stuff for calculating last value using repleating pattern evaluation.
    let mut header:Vec<usize> = Vec::new();
    let mut repeat:Vec<usize> = Vec::new();
    let mut buffer:Vec<usize> = Vec::new();
    let mut tstvec:Vec<usize> = Vec::new();

    //for cycle_count in 0..1_000_000_000 {
    gamedata = rotate_right(&gamedata);
    'outer: for _cycle_count in 0..10000 {
        for _cycle_index in 0..4 { 
            sortvec(&mut gamedata);             // Roll the balls this direction
            gamedata = rotate_right(&gamedata); // Point Direction, the first rotate, points North, then West, Sounth, East.
        }

        let mut north_load: usize = 0; 
        let gamewidth = gamedata[0].len();
        for col_index in 0..gamewidth{
            let col_string: String = gamedata.iter().map(|row| row[col_index]).collect();
            let o_count:usize = col_string.chars().filter(|&c| c == 'O').count();
            north_load += (col_index + 1) * o_count;
        }
        //println!("{}", north_load);

        tstvec.push(north_load);

        if let Some(index) = buffer.windows(tstvec.len()).position(|window| window == tstvec) {
            if tstvec.len() > 2 {
                println!("main : tstvec : {:?}", tstvec);
                println!("main : found at buffer offset : {}", index+1);
                header = buffer[0..index].to_vec();
                repeat = buffer[index..buffer.len()].to_vec();
                break 'outer ;   //**************************************************************************************************
            }    
        } else {
            buffer.append(&mut tstvec);
            tstvec.clear();
        }
    }

   println!("main : header : \n{:?}\n", header);
   println!("main : repeat : \n{:?}\n", repeat);

   let header_len = header.len(); 
   let repeat_len = repeat.len();

   println!("main : header len: {:?}", header_len);
   println!("main : repeat len: {:?}", repeat_len);
   

   let n = get_fract( 1_000_000_000 - header_len,  repeat_len );
   println!("main : n : {:?}", n);

   let offset = (repeat_len as f64 * n).round() as usize ;
   println!("main : offset : {:?}", offset);

   let final_north_load = repeat[offset-1];
   println!("main : final_north_load : {:?}", final_north_load);

}



fn get_fract(numer:usize, denom:usize) -> f64 {
    let whole_part = numer / denom;
    let fract_part = (numer as f64 / denom as f64) - whole_part as f64;
    return fract_part;
}    