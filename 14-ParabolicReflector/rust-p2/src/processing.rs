pub fn sortvec(matrix: &mut Vec<Vec<char>>) {
    for line in matrix {
        let indexes = get_char_locs(line.to_vec());
        let mut start: usize = 0;
        for i in indexes {
            line[start..i].sort();
            start = i+1;
        }
    }
}

pub fn get_char_locs(vec: Vec<char>) -> Vec<usize> {
    let mut indexes: Vec<usize> = Vec::new();
        for (i, &ch) in vec.iter().enumerate() {
            if ch == '#' {
                indexes.push(i);
            }
        }    
        indexes.push(vec.len());
    return indexes
}

// just for debugging
#[allow(dead_code)]
pub fn print_vvc(vvc: &Vec<Vec<char>>) {
    for rec in vvc {
        for ch in rec {
            print!("{}", ch);
        }
        println!("");    
    }
}

#[allow(dead_code)]
pub fn rotate_left(matrix: &Vec<Vec<char>>) -> Vec<Vec<char>> {
    let rows = matrix.len();
    let cols = matrix[0].len();
    let mut rotated = vec![vec![' '; rows]; cols];

    for i in 0..rows {
        for j in 0..cols {
            rotated[cols - 1 - j][i] = matrix[i][j]; // left
        }
    }
    rotated
}

#[allow(dead_code)]
pub fn rotate_right(matrix: &Vec<Vec<char>>) -> Vec<Vec<char>> {
    let rows = matrix.len();
    let cols = matrix[0].len();
    let mut rotated = vec![vec![' '; rows]; cols];

    for i in 0..rows {
        for j in 0..cols {
            rotated[j][rows - 1 - i] = matrix[i][j];
        }
    }
    rotated
}