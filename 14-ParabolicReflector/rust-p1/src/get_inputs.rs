
pub fn get_gamedata(filename: String) ->Vec<Vec<char>> {
    let s_gamedata = get_file(filename);

    let mut gamedata:Vec<Vec<char>> = Vec::new();
    for line in s_gamedata.lines() {
        let record:Vec<char> = line.chars().collect();
        gamedata.push(record);
    }
    return gamedata;
}


fn get_file(filename: String) -> String {
    use std::env;
    use std::fs;
    let current_dir = env::current_dir().unwrap();
    let path = current_dir.join("..").join(filename);
    if !path.exists() {
        println!("File does not exist");
        return " ".to_string();
    }else{
        let content = fs::read_to_string(path).unwrap();
        return content;
    };
}

