mod get_inputs;
use get_inputs::*;

fn main() {
    let filename = "GameData-P1-Test.dat".to_string();
    //let filename = "GameData-P1.dat".to_string();

    let gamedata = get_gamedata(filename);
    print_vvc(&gamedata);
    //let columncount = &gamedata[0].len();

    println!("---Native Vector-------------------");
    print_vvc(&gamedata);

    println!("---Rotated Vector------------------");
    let mut rgamedata = rotate_right(&gamedata);
    print_vvc(&rgamedata);
    
    println!("---sub sorted Vector---------------");
    sortvec(&mut rgamedata);
    print_vvc(&rgamedata);

    println!("---Count Os  -  line_count = col.count('O') * col_num  ");
    let mut weighted_count: usize = 0;
    for col_index in 0..rgamedata[0].len(){
        let col_string: String = rgamedata.iter().map(|row| row[col_index]).collect();
        println!("Vertical String: {}", col_string);
        let o_count:usize = col_string.chars().filter(|&c| c == 'O').count();
        weighted_count += (col_index + 1) * o_count;
        println!(" Col O count : {} - Col Index : {}", o_count, col_index, );
    }
    println!("---------\n   Weigthed Os count : {}\n----------", weighted_count);
}

fn sortvec(matrix: &mut Vec<Vec<char>>) {
    for line in matrix {
        let indexes = get_char_locs(line.to_vec());
        let mut start: usize = 0;
        for i in indexes {
            line[start..i].sort();
            start = i+1;
        }
    }
}

fn get_char_locs(vec: Vec<char>) -> Vec<usize> {
    let mut indexes: Vec<usize> = Vec::new();
        for (i, &ch) in vec.iter().enumerate() {
            if ch == '#' {
                indexes.push(i);
            }
        }    
        indexes.push(vec.len());
    return indexes
}


// just for debugging
#[allow(dead_code)]
fn print_vvc(vvc: &Vec<Vec<char>>) {
    for rec in vvc {
        for ch in rec {
            print!("{}", ch);
        }
        println!("");    
    }
}

#[allow(dead_code)]
fn rotate_left(matrix: &Vec<Vec<char>>) -> Vec<Vec<char>> {
    let rows = matrix.len();
    let cols = matrix[0].len();
    let mut rotated = vec![vec![' '; rows]; cols];

    for i in 0..rows {
        for j in 0..cols {
            rotated[cols - 1 - j][i] = matrix[i][j]; // left
        }
    }
    rotated
}

#[allow(dead_code)]
fn rotate_right(matrix: &Vec<Vec<char>>) -> Vec<Vec<char>> {
    let rows = matrix.len();
    let cols = matrix[0].len();
    let mut rotated = vec![vec![' '; rows]; cols];

    for i in 0..rows {
        for j in 0..cols {
            rotated[j][rows - 1 - i] = matrix[i][j];
        }
    }
    rotated
}