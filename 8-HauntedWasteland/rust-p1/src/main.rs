use std::{collections::HashMap, thread::current};
fn main() {
    #[derive(Debug)]
    struct Link {
        left: String,
        right: String,
    }
    //let s_gamedata = get_file("GameData-test.txt".to_string());
    //let s_gamedata = get_file("GameData-test2.txt".to_string());
    let s_gamedata = get_file("GameData-P1.txt".to_string());

    let lines: Vec<_> = s_gamedata.lines().clone().collect();
    let start: String = "AAA".to_string();
    let target: String = "ZZZ".to_string();
    let directions = lines[0].to_string();
    println!("Directions : {}", directions);

    let mut nodes: HashMap<String, Link> = HashMap::new();
    for i in 2..lines.len() {
        let line = lines[i];
        if !line.is_empty() {
            nodes.insert(
                line[0..3].to_string(),
                Link {
                    left: line[7..10].to_string(),
                    right: line[12..15].to_string(),
                },
            );      
        }
    }   

    let mut count = 0i64;
    let mut lr_index = 0usize;
    let mut current_node = start.clone();
    let mut lookup = nodes.get(&start).unwrap();
    println!("Start: {}, Left: {}, Right: {}",start, lookup.left, lookup.right);
    while current_node != target {

        let mov = match directions.chars().nth(lr_index) {
            Some(c) => {c},
            None => {' '},
        };

        current_node = match mov {
            'L' => {lookup.left.clone()},
            'R' => {lookup.right.clone()},
             _  => {"Should not happen".to_string()},
        };

        lookup = nodes.get(&current_node).unwrap();
        println!("Current Node: {}, Left: {}, Right: {}", current_node, lookup.left, lookup.right);
        lr_index += 1;
        if lr_index == directions.len() { lr_index = 0;};
        count += 1;

        if count == 50000 { break; }; //this is just hear to stop an unstoppable run.
    }
    println!("Number of moves: {}", count);

}


fn get_file(filename: String) -> String {
    use std::env;
    use std::fs;
    let current_dir = env::current_dir().unwrap();
    let path = current_dir.join("..").join(filename);
    
    let content = fs::read_to_string(path).unwrap();
    return content;
}