#[derive(Debug)]
struct Link {
    left: String,
    right: String,
}

use std::{collections::HashMap};
fn main() {
    //let (directions, nodes, mut starts) = get_game_data("GameData-test.txt".to_string());
    //let (directions, nodes, mut starts) = get_game_data("GameData-test2.txt".to_string());
    //let (directions, nodes, mut starts) = get_game_data("GameData-test3.txt".to_string());
    let (directions, nodes, mut starts) = get_game_data("GameData-P1.txt".to_string());
    println!(" Starts: {:?}",starts);
    
    let mut count = 0i64;
    let mut lr_index = 0usize;
    
    let mut all_zs: bool = false;
    while !all_zs {
        let mut count_zs = 0usize;
        let mov = match directions.chars().nth(lr_index) {
            Some(c) => {c},
            None => {' '},
        };
        //println!("move {} - ", mov);

        for i in 0..starts.len() {
            let lookup = nodes.get(&starts[i]).unwrap();
            starts[i] = match mov {
                'L' => {lookup.left.clone()},
                'R' => {lookup.right.clone()},
                _  => {"Should not happen".to_string()},
            };
            if starts[i][2..3].to_string()=="Z" { count_zs += 1; }
        }
        if count_zs >= 3 { println!(" to: {:?}", starts); };

        lr_index += 1;
        if lr_index == directions.len() { lr_index = 0; };
        count += 1;
        //actual exit case goes here.  - setup a counter if counter = starts.len() -1 all_Zs = true.
        if count_zs == starts.len() { all_zs = true; };
        if count == 10000000000 { break; }; //this is just hear to stop an unstoppable run.
    }
    println!("Number of moves: {}", count);
}

fn get_game_data(filename: String)-> (String, HashMap<String, Link>, Vec<String>) {
    let s_gamedata = get_file(filename);

    let lines: Vec<_> = s_gamedata.lines().collect();
    
    let directions = lines[0].to_string();
    println!("Directions : {}", directions);

    let nodes = get_hashmap(lines.clone());

    let mut starts: Vec<_> = Vec::new();
    for i in 2..lines.len() {
        if lines[i][2..3].to_string() == "A" {
            starts.push(lines[i][0..3].to_string());
        };
    };    

    return (directions, nodes, starts);
}

fn get_file(filename: String) -> String {
    use std::env;
    use std::fs;
    let current_dir = env::current_dir().unwrap();
    let path = current_dir.join("..").join(filename);
    
    let content = fs::read_to_string(path).unwrap();
    return content;
}

fn get_hashmap(lines: Vec<&str>) -> HashMap<String, Link> {
    let mut nodes: HashMap<String, Link> = HashMap::new();
    for i in 2..lines.len() {
        let line = lines[i];
        if !line.is_empty() {
            nodes.insert(
                line[0..3].to_string(),
                Link {
                    left: line[7..10].to_string(),
                    right: line[12..15].to_string(),
                },
            );      
        }
    }   
    return nodes;
}


/*
    new approach 3 - get a Vec<String>, of all Ending in As.
    add a loop in the existing search,, do 1 search for each branch of the new search.
    inc count, and direction_index, and index
    this way I get the performance benfit of the hashmap... all I gota do is build the loop, and setup the string[x] vec of start points (branches)

    current_node needs to be that Vec<String>

*/


