//use std::intrinsics::sqrtf64;

#[derive(Debug)]
struct  Race {
    time: f64,
    dist: f64,
}

fn main() {
    let races = get_file(("GameData_Day6_Part2.txt").to_string());
    let mut result: f64 = 1f64;
    println!("Games : {:?}", races);
    for race in races {
        result = result * check_race(race);
    }
    println!("Result for Day 6 Part 1 : {}", result);
}

fn get_file(filename: String) -> Vec<Race> {
    use std::env;
    use std::fs;

    let current_dir = env::current_dir().unwrap();
    let path = current_dir.join(filename);

    let s_races = fs::read_to_string(path).unwrap();
    let mut lines = s_races.lines();

    let s_times = lines.next().unwrap();
    let s_dists = lines.next().unwrap();

    let times:Vec<f64> = s_times.split_whitespace().skip(1).map(|x| x.parse().unwrap()).collect();
    let dists:Vec<f64> = s_dists.split_whitespace().skip(1).map(|x| x.parse().unwrap()).collect();

    let mut races = Vec::new();

    for i in 0..times.len() {
        races.push(Race {
            time: times[i],
            dist: dists[i],
        })
    }

    return races;
}

fn check_race(r: Race)-> f64 {
    let better_count;
    let best_time: f64 = r.time / 2f64;
    //x = (M - sqrt(M^2-4d))/2
    let rec_time: f64 =  (r.time - (r.time.powf(2f64) - 4f64 * r.dist).sqrt()) / 2f64;
    
    if best_time == best_time.trunc() { 
        better_count = (best_time.trunc() - 1f64 - rec_time.trunc()) * 2f64 + 1f64;
    } else {
        better_count = (best_time.trunc() - rec_time.trunc()) * 2f64;
    }

    println!("{}",format!("Race Time:{:5.1},\tBest Time:{:5.1},\tRecord Time:{:5.1},\tPossible Better Scores:{:5.1}",r.time, best_time, rec_time, better_count));
    //println!("Race Time: {},\tBest Time: {},\tRecord Time: {}",r.time, best_time, rec_time);

    return better_count;
}

