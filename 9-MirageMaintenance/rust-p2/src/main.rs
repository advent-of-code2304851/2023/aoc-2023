fn main() {
    let filename = "GameData-P1.txt".to_string();
    let (sumhistr, sumhistl) = get_gamedata(filename);
    println!("\n Sum of predictions right : {}",sumhistr);
    println!("\n Sum of predictions left  : {}",sumhistl);
}

fn get_gamedata(filename: String) -> (i64, i64) {
    let s_gamedata = get_file(filename);
    println!(" File Data :\n{}", s_gamedata);
    
    let mut sumhistr = 0i64;
    let mut sumhistl = 0i64;

     for line in s_gamedata.lines() {
        
        let mut gamedata: Vec<Vec<i64>> = Vec::new();
        let numbers: Vec<i64> = line
            .split_whitespace()
            .map(|s| s.parse().unwrap())
            .collect();
        gamedata.push(numbers);
        //println!("GameData : {:?}", gamedata);

        let mut i = 0usize;
        while !(gamedata[i].iter().all(|&x| x == 0)) {
        //while gamedata[i].iter().sum::<i64>() != 0 {
            let mut newline: Vec<i64> = Vec::new();
            for i2 in 1..gamedata[i].len() {
                let newnum = gamedata[i][i2] - gamedata[i][i2-1];
                newline.push(newnum);
            }
            gamedata.push(newline);
            i += 1;
        } 

        let predictr = predict_rightnum(&mut gamedata);
        sumhistr = sumhistr + predictr;
        let predictl = predict_leftnum(&mut gamedata);
        sumhistl = sumhistl + predictl;
        println!("GameData with prediction : {:?}", gamedata[0]);
    }
    return (sumhistr, sumhistl);
}

fn predict_rightnum(gamedata: &mut Vec<Vec<i64>>) -> i64 {   //P1 mode
    let count = gamedata.len()-1;
    gamedata[count].push(0i64);

    let mut prediction = 0i64;
    for i in (0..count).rev() {
        let curlen = gamedata[i].len()-1;
        let nexlen = gamedata[i+1].len()-1;
        let newhist = gamedata[i][curlen] + gamedata[i+1][nexlen];
        gamedata[i].push(newhist);
        prediction = newhist;
    }
    return prediction
}

fn predict_leftnum(gamedata: &mut Vec<Vec<i64>>) -> i64 {
    let count = gamedata.len()-1;
    gamedata[count].push(0i64);

    let mut prediction = 0i64;
    for i in (0..count).rev() {
        let newhist = gamedata[i][0] - gamedata[i+1][0];
        gamedata[i].insert(0,newhist);
        prediction = newhist;
    }
    return prediction
}

fn get_file(filename: String) -> String {
    use std::env;
    use std::fs;
    let current_dir = env::current_dir().unwrap();
    let path = current_dir.join("..").join(filename);
    if !path.exists() {
        //println!(" File not found : {}, {}", path.display(), path.exists());
        return " ".to_string();
    };

    let content = fs::read_to_string(path).unwrap();
    return content;
}