* Create a first-in first-out queue input object, that can be part of every other struct/object, multi input devices could have a Fifo queue, where there are mutiple pulses stored in the queue represent mutiple inputs.
  
      Create a trait for the input queue Vec<bool>
      Constructor to init all gates, will initialize the queues Vec to empty.
      create pub fn push(bool)
      create pub fn pull(bool)
      create pub fn get-count() -> bool
      create pub fn is_empty() -> bool

* Create a button object
  
      The button is the starting point for each itteration.
      Button only ever connects to a Broadcast Module
      create fn to press button
          send low pulse to connected device ( call devices receive fn )

* Create flipflop object symbol:%
  
      initialized :
          add one fifo queue for the one input
          set device memory state = low
      create an in function to receive pulse from previous device
      create an output function to send pulse as required to next device (by calling next devices receive fn)
      create Pointer for object this output connects to

* Create Conjunction (inverter)  symbol:&
  
  * create input memory f or storing last state for each input
    
        initialize 
            add on fifo queue for every intput specified
            set inputs memories = low
            set device status = low
  
  * create fn to receive inputs 
    
            store all inputs in memory
  
  * create fn to output to connected device.
    
            if all are high, then send Low
            if not send High
            (if only one input, act like inverter)
  
  * create Broadcast module 
    
        create one input queue
        create output for all connected devices

========================================
here is some sample code using traits in rust to allow us to implement a common input function to all gates. even if some gates have different numbers of inputs.

```rust
// Define a trait for logic gates with variable inputs
trait LogicGate {
 fn input(&self, inputs: &[bool]) -> bool;
}

// Implement the trait for different logic gates
struct AndGate;
struct NandGate;
struct OrGate;
struct XorGate;
struct Inverter;

impl LogicGate for AndGate {
 fn input(&self, inputs: &[bool]) -> bool {
 inputs.iter().all(|&x| x)
 }
}

impl LogicGate for NandGate {
 fn input(&self, inputs: &[bool]) -> bool {
 !inputs.iter().all(|&x| x)
 }
}

impl LogicGate for OrGate {
 fn input(&self, inputs: &[bool]) -> bool {
 inputs.iter().any(|&x| x)
 }
}

impl LogicGate for XorGate {
 fn input(&self, inputs: &[bool]) -> bool {
 inputs.iter().fold(false, |acc, &x| acc ^ x)
 }
}

impl LogicGate for Inverter {
 fn input(&self, inputs: &[bool]) -> bool {
 !inputs[0]
 }
}

// Function to process inputs using any logic gate
fn process_gate<G: LogicGate>(gate: &G, inputs: &[bool]) -> bool {
 gate.input(inputs)
}

fn main() {
 let and_gate = AndGate;
 let nand_gate = NandGate;
 let or_gate = OrGate;
 let xor_gate = XorGate;
 let inverter = Inverter;

println!("");
println!("AND Gate: 0 0 0 = {}", process_gate(&and_gate, &[false, false, false]));
println!("AND Gate: 0 0 1 = {}", process_gate(&and_gate, &[false, false, true]));
println!("AND Gate: 0 1 0 = {}", process_gate(&and_gate, &[false, true,  false]));
println!("AND Gate: 0 1 1 = {}", process_gate(&and_gate, &[false, true,  true]));
println!("AND Gate: 1 0 0 = {}", process_gate(&and_gate, &[true,  false, false]));
println!("AND Gate: 1 0 0 = {}", process_gate(&and_gate, &[true,  false, true]));
println!("AND Gate: 1 1 0 = {}", process_gate(&and_gate, &[true,  true,  false]));
println!("AND Gate: 1 1 1 = {}", process_gate(&and_gate, &[true,  true,  true]));

println!("");
println!("NAND Gate: 0 0 0 = {}", process_gate(&nand_gate, &[false, false, false]));
println!("NAND Gate: 0 0 1 = {}", process_gate(&nand_gate, &[false, false, true]));
println!("NAND Gate: 0 1 0 = {}", process_gate(&nand_gate, &[false, true,  false]));
println!("NAND Gate: 0 1 1 = {}", process_gate(&nand_gate, &[false, true,  true]));
println!("NAND Gate: 1 0 0 = {}", process_gate(&nand_gate, &[true,  false, false]));
println!("NAND Gate: 1 0 0 = {}", process_gate(&nand_gate, &[true,  false, true]));
println!("NAND Gate: 1 1 0 = {}", process_gate(&nand_gate, &[true,  true,  false]));
println!("NAND Gate: 1 1 1 = {}", process_gate(&nand_gate, &[true,  true,  true]));

println!("");
println!("OR Gate: {}", process_gate(&or_gate, &[true, false, true]));
println!("OR Gate: 0 0 0 = {}", process_gate(&or_gate, &[false, false, false]));
println!("OR Gate: 0 0 1 = {}", process_gate(&or_gate, &[false, false, true ]));
println!("OR Gate: 0 1 0 = {}", process_gate(&or_gate, &[false, true,  false]));
println!("OR Gate: 0 1 1 = {}", process_gate(&or_gate, &[false, true,  true ]));
println!("OR Gate: 1 0 0 = {}", process_gate(&or_gate, &[true,  false, true ]));
println!("OR Gate: 1 0 0 = {}", process_gate(&or_gate, &[true,  false, false]));
println!("OR Gate: 1 1 0 = {}", process_gate(&or_gate, &[true,  true,  true ]));
println!("OR Gate: 1 1 1 = {}", process_gate(&or_gate, &[true,  true,  false]));

println!("");
println!("XOR Gate: 0 0 0 = {}", process_gate(&xor_gate, &[false, false, false]));
println!("XOR Gate: 0 0 1 = {}", process_gate(&xor_gate, &[false, false, true ]));
println!("XOR Gate: 0 1 0 = {}", process_gate(&xor_gate, &[false, true,  false]));
println!("XOR Gate: 0 1 1 = {}", process_gate(&xor_gate, &[false, true,  true ]));
println!("XOR Gate: 1 0 0 = {}", process_gate(&xor_gate, &[true,  false, false]));
println!("XOR Gate: 1 0 0 = {}", process_gate(&xor_gate, &[true,  false, true ]));
println!("XOR Gate: 1 1 0 = {}", process_gate(&xor_gate, &[true,  true,  false]));
println!("XOR Gate: 1 1 1 = {}", process_gate(&xor_gate, &[true,  true,  true ]));

println!("");
println!("Inverter: 0 = {}", process_gate(&inverter, &[false]));
println!("Inverter: 1 = {}", process_gate(&inverter, &[true]));
```

Putting it together 

* I may need to multi passthrought the input data, as I think it will be helpfull to init all objects/gates in advance, but also, it will allow me to determine in advance how many inputs the NAND gate has, so I can correctly calculate the pulses it sends out, before it is officially read.  like in the example data.   Alternatively, I could still multipass, but just to create the correct gates, when I get the the output of a gate, I could loop through its inputs, but process them one at a time, becuase at this point I would know how many inputs there are, asuming the next output of this device does not increase the number of inputs. ( even if thre are mutiple possibilities throught the repeat cycle )

* Since my test code is using `return` to deal with the output of the gates, I realized I may need to use `Option`, to handle the fact that sometimes there will be no pulse to return.

* Since one input to a NAND gates must act like an inverter, it will only do this if there is not a second input that is defaulted to LOW.  Also, a three input could perform differently to a two input, depending on the signals that are send and in what order to the inputs before its officially read; so we must know in advance how many inputs.

* Flipflop should only ever have one input, as it uses it like a clock signal, and pulses only when recieving a LOW clock signal.

* button, and broadcaster should only have one input.

   

Also in investigating Options.
