# Manual test data walkthrough GD-test1

### Input Data Symbolism:

| Type Symbol | Type       | Description                                                                         |
|:-----------:|:----------:|:----------------------------------------------------------------------------------- |
| broadcaster | Buffer     | Takes one input, replicates it out to multiple outputs                              |
| Button      |            | Send 1 low pulse.                                                                   |
| %           | flip-flop  | Store last state,  (default low) high is ignored, low state toggles state & output. |
| &           | NAND / Inv | Basically NAND gate, single input acts as inverter,                                 |

### Circuit :

| from device | to device list |
|:-----------:|:--------------:|
| button      | broadcaster    |
| broadcaster | a              |
| %a          | I, C           |
| &I          | b              |
| %b          | C              |
| &C          | output         |

- `&I` is single input, there for acts like an `Inverter`

- `&C` is dual imput [a,b] so acts like a `NAND` gate

- There is also an unnamed object `output`.  this is just so we can have a destination for pulses that dont affect the circuit but we want to count, it represents the interum state of a gate.

- Object of the game  is to count how many hi and lo pulses after pressing the button 1000 times.

### Program 1 walkthrough

| From Device | **Output Pulse** | To Device | **To Dev output** |
|:-----------:|:----------------:|:---------:|:-----------------:|
| button      | **lo**           | bc        | *( bc = lo )*     |
| bc          | **lo**           | a         | *( a = hi )*      |
| bc          | **lo**           | b         | *( b = hi )*      |
| bc          | **lo**           | c         | *( c = hi )*      |
| %a          | **hi**           | b         | *no pulse*        |
| %b          | **hi**           | c         | *no pulse*        |
| %c          | **hi**           | i         | *no pulse*        |
| &i          | **lo**           | a         | *( a = lo )*      |
| %a          | **lo**           | b         | *( b = lo )*      |
| %b          | **lo**           | c         | *( c = lo )*      |
| %c          | **lo**           | i         | *( i = hi )*      |
| &i          | **hi**           | a         | *no pulse  *      |

After running the program, all states returned to start state,  pressing button again will repeat this output.

After one iteration lo pulse count is 8  hi pulse count is 4, so  (8 * 1000) * (4 * 1000) = **32,000,000**.

# Test Data - circuit walkthrough description

| Device | Outputs |
|:------:|:-------:|
| button | buffer  |
| buffer | a, b, c |
| %a     | b       |
| %b     | c       |
| %c     | i       |
| &i     | a       |

### Circuit loop 1

| from device | pulse    | destination device |
| ----------- |:--------:| ------------------ |
| button      | **low**  | broadcaster        |
| broadcaster | **low**  | a                  |
| %a          | **high** | inv                |
| %a          | **high** | con                |
| &inv        | **low**  | b                  |
| &con        | **high** | output             |
| %b          | **high** | con                |
| &con        | **low**  | output             |

### Circuit loop 2

| from device | pulse    | destination device |
| ----------- |:--------:| ------------------ |
| button      | **low**  | broadcaster        |
| broadcaster | **low**  | a                  |
| a           | **low**  | inv                |
| a           | **low**  | con                |
| inv         | **high** | b                  |
| con         | **high** | output             |

### Circuit loop 3

| from device | pulse    | destination device |
| ----------- |:--------:| ------------------ |
| button      | **low**  | broadcaster        |
| broadcaster | **low**  | a                  |
| a           | **high** | inv                |
| a           | **high** | con                |
| inv         | **low**  | b                  |
| con         | **low**  | output             |
| b           | **low**  | con                |
| con         | **high** | output             |

### Circuit loop 4

| from device       | pulse | destination device |
| ----------------- |:-----:| ------------------ |
| lo    button      | low   | broadcaster        |
| lo    broadcaster | low   | a                  |
| lo    a           | low   | inv                |
| lo    a           | low   | con                |
| hi    inv         | high  | b                  |
| hi    con         | high  | output             |

### Calculate Game Answer

To avoid having to run through the program for all 1000 presses of the button we optimize by finding how many presses to  return to the default state.

- 1000 total button presses 

- 4 itterations of program to return all gate states to default

- 1000 / 4 = **250**

| Valid Pulses | Calculated              | Expected     |
| ------------ | ----------------------- | ------------ |
| LOWs   17    | 17  * **250**    = 4250 | 4250         |
| HIGHs  11    | 11  * **250**    = 2750 | 2750         |
| 28           | **11687500**            | **11687500** |
