#[allow(unused_imports)]
use crate::gates::*;
#[allow(unused_imports)]
use crate::processing::*;

#[allow(dead_code)]
pub fn gate_testing () {
    let mut pc = Accumulator::new();
 
    println!("\n------------------------\n-- FlipFlop --\n------------------------\n");
    let mut c = FlipFlop::new();
    println!("Initial State: {} = {}", c.disp_inputs(), c.state);
    c.input(&mut pc, Some(true));
    println!("Initial State: {} = {}", c.disp_inputs(), c.state);
    c.input(&mut pc, Some(true));
    println!("Initial State: {} = {}", c.disp_inputs(), c.state);
    c.input(&mut pc, Some(false));
    println!("Initial State: {} = {}", c.disp_inputs(), c.state);
    c.input(&mut pc, Some(false));
    println!("Initial State: {} = {}", c.disp_inputs(), c.state);
    c.input(&mut pc, Some(true));
    println!("Initial State: {} = {}", c.disp_inputs(), c.state);
    c.input(&mut pc, Some(true));
    println!("Initial State: {} = {}", c.disp_inputs(), c.state);
    c.input(&mut pc, Some(false));
    println!("Initial State: {} = {}", c.disp_inputs(), c.state);
    println!("Output for: {} = {}", c.disp_inputs(), c.output());
 
    println!("{}", pc.display());
 
    println!("\n------------------------\n-- 2 input NAND Gate --\n------------------------\n");
    let mut a = Nand::new(2);
    println!("Initial State: {} = {}", a.disp_inputs(), a.state);
 
    a.input(&mut pc, Some(true));
    println!("Current State for: {} = {}", a.disp_inputs(), a.state);
 
    a.input(&mut pc, Some(true));
    println!("Current State for: {} = {}", a.disp_inputs(), a.state);
    println!("Output for: {} = {}", a.disp_inputs(), a.output());
 
    println!("{}", pc.display());
 
    println!("\n------------------------\n-- 2 input NAND Gate pass2 --\n------------------------\n");
    a.input(&mut pc, Some(false));
    println!("Current State: {} = {}", a.disp_inputs(), a.state);
    a.input(&mut pc, None);
    println!("Current State: {} = {}", a.disp_inputs(), a.state);
    println!("Output for: {} = {}", a.disp_inputs(), a.output());
 
    println!("{}", pc.display());
 
    println!("\n------------------------\n-- 1 input NAND/Inverter Gate --\n------------------------\n");
    let mut b = Nand::new(1);
    println!("Initial State: {} = {}", b.disp_inputs(), b.state);
 
    b.input(&mut pc, Some(true));
    println!("Current State: {} = {}", b.disp_inputs(), b.state);
    b.input(&mut pc, Some(false));
    println!("Current State: {} = {}", b.disp_inputs(), b.state);
    println!("Output for: {} = {}", b.disp_inputs(), b.output());
 
    b.input(&mut pc, Some(true));
    println!("Current State: {} = {}", b.disp_inputs(), b.state);
    b.input(&mut pc, Some(true));
    println!("Current State: {} = {}", b.disp_inputs(), b.state);
    println!("Output for: {} = {}", b.disp_inputs(), b.output());
 
    println!("\n-------------------\n-- Ouput Pulse Counts from accumulator  --\n-------------------\n");
    println!("{}", pc.display());
}
