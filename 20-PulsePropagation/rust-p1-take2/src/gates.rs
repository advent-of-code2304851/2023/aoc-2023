use crate::processing::*;
#[allow(dead_code)]
pub struct Fifo {
    input_count: usize,
    input_ptr: usize,
    queue: Vec<bool>,
}
 
#[allow(dead_code)]
impl Fifo {
    pub fn new(icount: usize) -> Fifo {
        Fifo{
            input_count: icount,
            input_ptr: 0,
            queue: vec![false; icount],
        }
    }
    pub fn push(&mut self, pulse: Option<bool>) {
        match pulse {
            Some(value) => { self.queue[self.input_ptr] = value },
            None =>{},
        }
        if self.input_count > 1 {
            self.input_ptr += 1;
        }
    }
    pub fn display(&self) -> String {
      format!("{:?}",self.queue)
    }
}
 
pub trait Gate {
    fn input(&mut self, pc: &mut Accumulator, pulse: Option<bool>);
    fn output(&mut self) -> bool;
    fn get_gate_type(&self) -> String;
    fn get_input_count(&self) -> usize;
    fn disp_state(&self) -> String;
    fn disp_inputs(&self) -> String;
}
//----------------------FlipFlop----------------------
pub struct FlipFlop {
    buffer: Fifo,
    pub state: bool,
    input_ptr: usize,
}
impl FlipFlop {
    pub fn new() -> FlipFlop {
        FlipFlop {
            buffer: Fifo::new(1),
            state: false,
            input_ptr: 0,
        }
    }
}
impl Gate for FlipFlop {
    fn input(&mut self, pc: &mut Accumulator, pulse: Option<bool>) {
        match pulse {
            Some(value) => {
                self.buffer.queue[self.input_ptr] = value;
                if value == false {
                    self.state = !self.state;
                    pc.put(Some(self.state))
                }
            }  
            None => {}
        }
    }   
    fn get_gate_type(&self) -> String {
        "FlipFlop".to_string()
    }
    fn get_input_count(&self) -> usize{
        self.buffer.input_count
    }    
    fn output(&mut self) -> bool {
        self.state
    }
    fn disp_state(&self) -> String {
        format!("{}",self.state)
    }
    fn disp_inputs(&self) -> String {
        format!("{:?}",self.buffer.queue)
    }
}
 
//---------------------NandGate-----------------------
#[allow(dead_code)]
pub struct Nand {
    buffer: Fifo,
    pub state: bool,
    input_ptr: usize,
}
#[allow(dead_code)]
impl Nand {
    pub fn new(input_count:usize) -> Nand {
        Nand {
            buffer: Fifo::new(input_count),
            state: true,
            input_ptr: 0,
        }
    }
}
impl Gate for Nand {
    fn input(&mut self, pc: &mut Accumulator, pulse: Option<bool>) {
        match pulse {
            Some(pulse) => {
                self.buffer.queue[self.input_ptr] = pulse;
                let newstate = !self.buffer.queue.iter().all(|&i| i);
                if newstate != self.state {
                    //send pulse to counter, but not to output
                    self.state = newstate;
                    pc.put(Some(self.state));
                }
            }  
            None => {pc.put(None)},
        }
        if self.buffer.input_count > 1 {
            self.input_ptr += 1;
        }
    }
    fn get_gate_type(&self) -> String {
        "Nand".to_string()
    }
    fn output(&mut self) -> bool {
        self.input_ptr = 0;
        self.state
    }
    fn get_input_count(&self) -> usize{
        self.buffer.input_count
    }    
    fn disp_state(&self) -> String {
        format!("{}",self.state)
    }
   fn disp_inputs(&self) -> String {
        format!("{:?}",self.buffer.queue)
    }
}
 
 
//---------------------Broadcaster-----------------------
#[allow(dead_code)]
pub struct Broadcaster {
    buffer: Fifo,
    pub state: bool,
    input_ptr: usize,
}
#[allow(dead_code)]
impl Broadcaster {
    pub fn new() -> Broadcaster {
        Broadcaster {
            buffer: Fifo::new(1),
            state: false,
            input_ptr: 1,
        }
    }
}
impl Gate for Broadcaster {
    fn input(&mut self, pc: &mut Accumulator, pulse: Option<bool>) {
        pc.put(pulse);
        if self.buffer.input_count > 1 {
            self.input_ptr += 1;
        }
    }
    fn get_gate_type(&self) -> String {
        "Broadcaster".to_string()
    }
    fn get_input_count(&self) -> usize{
        self.buffer.input_count
    }    
    fn output(&mut self) -> bool {
        self.input_ptr = 0;
        self.state
    }
    fn disp_state(&self) -> String {
        format!("{}",self.state)
    }
   fn disp_inputs(&self) -> String {
        format!("{:?}",self.buffer.queue)
    }
}
 

