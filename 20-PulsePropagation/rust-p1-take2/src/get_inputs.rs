use crate::gates::*;
use std::process::*;
use std::collections::HashMap;

pub struct Instruction {
    pub gate_type: String,
    pub name: String,
    pub dest_gates: Vec<String>,    
}
impl Instruction {
    #[allow(dead_code)]
    pub fn display_h(&self) -> String {
        let typename = match self.gate_type.as_str() {
            "%" => "Flip-Flop",
            "&" => "Nand Gate",
            "broadcaster" => "",
            _ => "--Error--",
        };
//        let display:String = format!("- name: {} - {}, to: {:?}", self.name, typename, self.dest_gates);
        
        let display:String = format!("  {} - {} : {:?}", self.name, typename, self.dest_gates);
//        MyStruct::new("a1", vec!["b1".to_string(), "b2".to_string()]),
return display.to_string()
    }

    #[allow(dead_code)]
    pub fn display_v(&self) -> String {
        let display:String = format!("----------------\ngate type:{}\nname:{}\ndestinations:{:?}",self.gate_type, self.name, self.dest_gates);
        return display.to_string()
    }
}

pub struct GameData {
    pub cmds: Vec<Instruction>,
    pub gates: Vec<Box<dyn Gate>>,
    pub gate_lookup: HashMap<String, usize>,
}
impl GameData {
    pub fn new( filename: &str ) -> Self {
        let mut gd = GameData {
            cmds: Vec::new(),
            gates: Vec::new(),   // gates.push(Box::new(and_gate));
            gate_lookup: HashMap::new(),
        };

        //get the Broadcaster on top to make looping easier.
        let mut sgd = get_file(filename);
        sgd = GameData::reorder_data(&mut sgd);
        
        //load cmd vector, and gate-lookup from gamedata_string
        for (i, cmd) in sgd.lines().map( |s| {s.to_string()} ).enumerate() {
            let v_cmd = GameData::gd_parse(cmd);
            gd.cmds.push(v_cmd);
            gd.gate_lookup.insert(gd.cmds[i].name.clone(), i);
        };

        // get the input counts per gate
        let mut counts:HashMap<&String, usize> = HashMap::new();
        for cmd in &gd.cmds {
            for item in &cmd.dest_gates {
                *counts.entry(item).or_insert(0) += 1;
            }       
        }
        
//        for cmd in &gd.cmds {
//            let thiscount = match counts.get(&cmd.name) { Some(x) => x, None => &1usize, };
//            println!("Loading coult lookup : {}, {}",cmd.name, thiscount);
//        }

        // get the gates and gate data into gd.
        for cmd in &gd.cmds {
            let name = &cmd.name;
            let gtype: &String = &cmd.gate_type;
            //println!("Load Gate {}, {}", name, gtype);
            let count = match counts.get(&name)  {
                Some(count) => *count,
                None => {println!("Input count for : {} not found", cmd.name); 1usize},
            };
            match gtype as &str {
                "broadcaster" => gd.gates.push(Box::new(Broadcaster::new())),
                "&"           => gd.gates.push(Box::new(Nand::new(count))),
                "%"           => gd.gates.push(Box::new(FlipFlop::new())),
                _ => panic!("Not a Valid Gate found in Game Data: {}", name),
            };
        };

        return gd
    }

    //------------------------------------------------------------------
    //--   parse the GameData string into datastructures              --
    //------------------------------------------------------------------
    fn gd_parse(s: String) -> Instruction {
        let left: String;
        let right: String;

        if let Some(pos) = s.find(" -> ") {
            left = s[..pos].to_string();
            right = s[pos+4..].to_string();
        } else {
            println!("Bad Input : no -> found");
            exit(1);
        }

        let cmd: Instruction;

        if left == "broadcaster" {
            cmd = Instruction {
                gate_type: left[0..].to_string(),
                name: left[0..].to_string(),
                dest_gates: right.split(",").map(|s| {s.trim().to_string()} ).collect(),
            };
        } else {
            cmd = Instruction {
                gate_type: left[0..1].to_string(),
                name: left[1..].to_string(),
                dest_gates: right.split(",").map(|s| {s.trim().to_string()} ).collect(),
            };
        }

        //println!("{}", cmd.display_v());
        return cmd

    }

    //------------------------------------------------------------------
    //--   locate Broadcaster - make it first maintain loop order     --
    //------------------------------------------------------------------
    fn reorder_data(s: &mut String) -> String {
        if let Some(pos) = s.find("broadcaster") {
            if pos == 0 {
                return s.to_string(); // The word is already at the start
            }
            let (part1, part2) = s.split_at(pos);
            let part2 = if part2.ends_with('\n') {
                part2.to_string()
            } else {
                format!("{}\n", part2)
            };
            format!("{}{}", part2, part1)
        } else {
            s.to_string() // Return the original string if the word is not found
        }
    }
}

//------------------------------------------------------------------
//--   get the GameData file into a string                        --
//------------------------------------------------------------------
pub fn get_file(filename: &str) -> String { 
    use std::env::current_dir;
    use std::fs::read_to_string;
    //use std::process::exit;

    let current_dir = current_dir().unwrap();
    let path = current_dir.join("..").join(filename);
    let sgamedata:String;

    if !path.exists() {
        println!("File does not exist : {}", path.display());
        panic!();
    } else {
        match read_to_string(path) {
            Ok(content) => {
                sgamedata = content;
            }
            Err(e) => {
                println!("Error reading game data {e}");
                panic!();
            }
        }
    };
    return sgamedata
}





