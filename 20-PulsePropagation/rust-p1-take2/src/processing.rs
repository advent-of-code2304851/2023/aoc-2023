pub struct Accumulator {
    pulses: Vec<Option<bool>>,
    hi:  usize,
    lo:  usize,
    no:  usize,
}
impl Accumulator {
    pub fn new() -> Accumulator {
        Accumulator {
            pulses: Vec::new(),
            hi: 0,
            lo: 0,
            no: 0,
        }
    }
    pub fn put(&mut self, pulse: Option<bool>) {
        self.pulses.push(pulse);
        match pulse {
            Some(true) => { self.hi += 1 },
            Some(false) => { self.lo += 1 },
            None => { self.no += 1 },
        }
    }
    pub fn display(&self) -> String {
        format!("Hi: {}, Lo: {}, None: {}",self.hi, self.lo, self.no)
    }
}
