/*
     **   To Do.   **

 |X| 1. create broadcaster - so simple, perhaps not a gate object,  jsut a silple fn, send lo pulse to Accumulator, loop { send lo pulse to all listed gates }
 |X| 2. create struct for instructions, name, type, Vec of destinations
 |X| 3. create GameData struct, include Vec<Instruction>, HashMap<String(gage name:String, i:usize)
 |?| 4. create a counter for all inputs for each gate and save in instruction vec
 |?| 5. calculate inputs per gate, and populate gate vector using input count for NandGates
 | | 6. create process to check for init state for all gates, to determin repeat count.
 |X| 7. create fn to process output_list ie  send Gate state to all ouput gates listed in vec ( cuz creating an array from import csv sucks )
 | | 8. figure out exactly where I am going to increment the count. if input(), then for multiple in nand, I might need to subtract one, when checking output()
        or check if last input and don't increment on input, but do it on output.

*/

mod get_inputs;
use get_inputs::*;

mod gates;
#[allow(unused_imports)]
use gates::*;

mod test_and_debug;
#[allow(unused_imports)]
use test_and_debug::*;

mod processing;
use processing::*;

//use std::panic;
use chrono::Local;

fn main() {
//    panic::set_hook(Box::new(|_| {
//        eprintln!("A panic occurred!");
//        process::exit(1);
//    }));

    //let gd = get_gd("GameData-Test1.dat");
    //let gd = get_gd("GameData-Test2.dat");
    //let gd = get_gd("GameData-Test3.dat");
//    let cmds = get_gd("GameData.dat");
    let mut gd = GameData::new("GameData.dat");
   

    // testing ***********************************************
    // Basic : 
     //gate_testing();
    
     // testing gates from the GameData in a loop that exists untimately when init state is replicated after n full cycles.
//        looptesting(&gd);

    match gd.gate_lookup.get("broadcaster") {
        Some(i) => println!("Index of Broadcaster: {}", i),
        None => println!("ooops"),
    }

    looptesting(&mut gd);

}



fn looptesting(gd: &mut GameData) {
    let counter = &mut Accumulator::new();
    let cmds = &gd.cmds;

    let now = Local::now();
    println!("Current time: {}", now.format("%Y-%m-%d %H:%M:%S"));

    let mut cmd_i:usize = 0;
    for cmd in cmds {
        let send_gate = &mut gd.gates[cmd_i];
        let send_type = &cmd.gate_type;
        let send_name = &cmd.name;
        let send_state = &send_gate.output();
        let dest_gates = &cmd.dest_gates;
        let input_count = send_gate.get_input_count();
        println!("Gate Type:{} ins:{} name:{} - state: {} => Dests: {:?}", send_type, input_count, send_name, send_state, dest_gates);

        for name in &cmd.dest_gates {
            let gate_i = match gd.gate_lookup.get(name) {
                Some(i)    => *i,
                None => { println!(" !!! Gate Lookup Failed - likely bad data "); break}, //manually cound a pulse & dont break
            };
            let dest_gate = &mut gd.gates[gate_i];
            let dest_type = cmds[gate_i].gate_type.clone();
            let dest_state = &dest_gate.disp_state();
            println!(" - Gate name: {} - type: {} - state: {} ", name, dest_type, dest_state);
            let _ = &dest_gate.input( counter, Some(*send_state) );
       }
       println!("\n--------------------------------------------\n");
       //if cmd_i == 10 { break }
        cmd_i += 1;
    }

/*   
    // debug sample code for accessing gates data outside of main loops.
    let mut lookup_gate = "broadcaster".to_string();
    let mut gate_i = search(gd, &lookup_gate) ;
    print!("lookup: {} - returns index: {} - ",lookup_gate, gate_i);
    let mut subgate = &mut gd.gates[gate_i];
    println!("Inputs: {} - {}", &subgate.get_input_count(), &subgate.get_gate_type());

    lookup_gate = "cf".to_string();
    gate_i = search(gd, &lookup_gate);
    print!("lookup: {} - returns index: {} - ",lookup_gate, gate_i);
    subgate = &mut gd.gates[gate_i];
    println!("Inputs: {} - {}", &subgate.get_input_count(), &subgate.get_gate_type());

    lookup_gate = "vf".to_string();
    gate_i = search(gd, &lookup_gate);
    print!("lookup: {} - returns index: {} - ",lookup_gate, gate_i);
    subgate = &mut gd.gates[gate_i];
    println!("Inputs: {} - {}", &subgate.get_input_count(), &subgate.get_gate_type());
 
    lookup_gate = "rt".to_string();
    gate_i = search(gd, &lookup_gate);
    print!("lookup: {} - returns index: {} - ",lookup_gate, gate_i);
    subgate = &mut gd.gates[gate_i];
    println!("Inputs: {} - {}", &subgate.get_input_count(), &subgate.get_gate_type());

    lookup_gate = "gp".to_string();
    gate_i = search(gd, &lookup_gate);
    print!("lookup: {} - returns index: {} - ",lookup_gate, gate_i);
    subgate = &mut gd.gates[gate_i];
    println!("Inputs: {} - {}", &subgate.get_input_count(), &subgate.get_gate_type());
*/

    println!("Counter: {}",counter.display());


}


fn search(gd: &mut GameData, gate_name: &String) -> usize {
    match gd.gate_lookup.get(gate_name) {
        Some(i)    => *i,
        None => { println!(" !!! Gate Lookup Failed - likely bad data "); panic!() },
    }
}