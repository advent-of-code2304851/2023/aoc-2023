use crate::loadgates::*;
//---------------------------------------------------------------------------
//   Test functions  only for dev and debug
//---------------------------------------------------------------------------
pub fn test_flipflop() {
    println!("\n-------------------------------\nTesting FlipFlop\n-------------------------------");
    let mut bob = FlipFlop::new();
    println!(" flipflop init state is {}", bob.state);
    
    bob.set_input(vec![Some(true)]);
    print!("1 flipflop sent high pulse");
    printif_Option("  Output is ", bob.get_output());
    
    bob.set_input(vec![Some(false)]);
    print!("2 flipflop sent low  pulse");
    printif_Option("  Output is ", bob.get_output());
    
    bob.set_input(vec![Some(true)]);
    print!("3 flipflop sent high pulse");
    printif_Option("  Output is ", bob.get_output());
    
    bob.set_input(vec![Some(false)]);
    print!("4 flipflop sent low  pulse");
    printif_Option("  Output is ", bob.get_output());
}

fn printif_Option(text: &str, pulse:Option<bool>) {
    match pulse {
        Some(pulse) => println!("{}{}",text, pulse),
        None => {println!("No pulse sent, so don't count this")},
    }
}


//--  create a GateList,  create a flipflop, push flipflop to gatelist
pub fn test_gatelist() {
    println!("\n-------------------------------\nTesting GateList\n-------------------------------");

    let mut gl = GateList::new();
    let name = "ff1".to_string();
    gl.put( Box::new(FlipFlop::new()),name.clone());

    //let mut bob = FlipFlop::new();
    //gl.put(Box::new(bob));
    // Create a mutable reference to the FlipFlop at index 0 

    let ptr = gl.get(name);
    println!("Lookup for FF1 returns: {}",ptr);

    if let Some(gate) = gl.gates.get_mut(ptr) { 
        println!(" flipflop init state is {}", gate.get_input_ptr() );

        gate.set_input(vec![Some(true)]);
        print!("1 flipflop sent high pulse");
        printif_Option("  Output is ", gate.get_output());
        
        gate.set_input(vec![Some(false)]);
        print!("2 flipflop sent low  pulse");
        printif_Option("  Output is ", gate.get_output());
        
        gate.set_input(vec![Some(true)]);
        print!("3 flipflop sent high pulse");
        printif_Option("  Output is ", gate.get_output());
        
        gate.set_input(vec![Some(false)]);
        print!("4 flipflop sent low  pulse");
        printif_Option("  Output is ", gate.get_output());
    }

}


pub fn test_nandgate(){
    println!("\n-------------------------------\nTesting NandGate 1 input ( inverter ) \n-------------------------------");

    let mut gl = GateList::new();
    let name = "ng1".to_string();
    gl.put( Box::new(NandGate::new(1)),name.clone());

    let ptr = gl.get(name);
    if let Some(gate) = gl.gates.get_mut(ptr) { 
    
        gate.set_input(vec![Some(true)]);
        print!("1 inverter sent high pulse ");
        printif_Option("  Output is ", gate.get_output());
        
        gate.set_input(vec![Some(false)]);
        print!("2 inverter sent low  pulse");
        printif_Option("  Output is ", gate.get_output());
    }

    println!("\n-------------------------------\nTesting NandGate 2 input\n-------------------------------");

    let mut gl = GateList::new();
    let name = "ng2".to_string();
    gl.put( Box::new(NandGate::new(2)),name.clone());

    let ptr = gl.get(name);
    if let Some(gate) = gl.gates.get_mut(ptr) { 
        gate.set_input(vec![Some(false),Some(false)]);
        print!("3 2-input NAND sent low, low ");
        printif_Option("  Output is ", gate.get_output());
 
        gate.set_input(vec![Some(false),Some(true)]);
        print!("3 2-input NAND sent low, high ");
        printif_Option("  Output is ", gate.get_output());
         
        gate.set_input(vec![Some(true),Some(false)]);
        print!("3 2-input NAND sent high, low ");
        printif_Option("  Output is ", gate.get_output());
         
        gate.set_input(vec![Some(true),Some(true)]);
        print!("3 2-input NAND sent high, high ");
        printif_Option("  Output is ", gate.get_output());
    }
}