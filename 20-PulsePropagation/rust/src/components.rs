
pub struct Fifo {
    elements: Vec<bool>,
}
#[allow(dead_code)]
impl Fifo {
    pub fn new() -> Self {
        Fifo {
            elements: Vec::new(),
        }
    }

    pub fn push(&mut self, item: bool) {
        self.elements.push(item);
    }

    pub fn pull(&mut self) -> Option<bool> {
        if self.elements.is_empty() {
            None
        } else {
            Some(self.elements.remove(0))
        }
    }

    fn is_empty(&self) -> bool {
        self.elements.is_empty()
    }
}

pub const HIGH: bool = true;
pub const LOW: bool = false;


#[allow(dead_code)]
pub struct FlipFlop {
    input: Fifo,
    state: bool,
}
impl FlipFlop {
    pub fn new() -> Self {
        FlipFlop {
            state: LOW,
            input: Fifo::new(),
        }
    }

    pub fn set_input(&mut self, input:bool) {
        let old_state = self.state;
        if !input {
            if self.state {
                self.state = LOW;
            } else {
                self.state = HIGH;
            }
        }
        println!("FlipFlop, input:({}) previous state: ({}) new state: ({})",input, old_state,self.state);
    }
}
