use std::process::*;
use std::collections::HashMap;
//----------------------------------------------------------------------------
pub struct Fifo {
    in_ptr: usize,
    elements: Vec<Option<bool>>,
}

impl Fifo {
    pub fn new(i: usize) -> Self {
        Fifo {
            in_ptr : 0,
//            elements: Vec::new(),
            elements: vec![Some(false); i],
        }
    }

    #[allow(dead_code)]
    pub fn push(&mut self, item: Vec<Option<bool>>) {
        self.elements.push(item[0]);
    }

    #[allow(dead_code)]
    pub fn pull(&mut self) -> Option<bool> {
        if self.elements.is_empty() {
            None
        } else {
            self.elements.remove(0)
        }
    }

    #[allow(dead_code)]
    fn is_empty(&self) -> bool {
        self.elements.is_empty()
    }
}

//----------------------------------------------------------------------------
pub trait Gate {
    fn get_input_ptr(&self) -> usize;
    fn set_input_ptr(&mut self, i:usize);
    fn set_input(&mut self, inputs: Vec<Option<bool>>);
    fn get_output(&mut self) -> Option<bool>;
}

//----------------------------------------------------------------------------
//            NAND Gate
//----------------------------------------------------------------------------
pub struct NandGate {
    input_ptr:  usize,
    inputs:     Fifo,
    memory:     Fifo,
    state:      bool,
}
impl Gate for NandGate {
    fn get_input_ptr(&self) -> usize {
        return self.input_ptr;
    }
    fn set_input_ptr(&mut self, i:usize){
        return self.input_ptr = i;
    }
    fn set_input(&mut self, pulse: Vec<Option<bool>>) {
        self.inputs.elements = pulse;
        self.input_ptr += 1;
        //return self.inputs.push(pulse)
    }
    fn get_output(&mut self) -> Option<bool> {
        self.input_ptr = 0;
        let counti = self.inputs.elements.iter().filter(|item| item.is_some()).count();
        let countm = self.memory.elements.iter().filter(|item| item.is_some()).count();
        println!("NAND input count: {}, memory count: {}",counti, countm);

        let mut result: bool = false;
        for i in 0..counti {
            self.memory.elements[i] = self.inputs.elements[i];
            result = !self.memory.elements.iter().all(|&x| x.unwrap());
            // count trues and falses here.
        }
        Some(result)
    }
}
impl NandGate {
    pub fn new(input_num: usize) -> Self {
        //println!("-------Fifo size {}", input_num);
        NandGate {
            input_ptr: 0,
            inputs: Fifo::new(input_num),   
            memory: Fifo::new(input_num),
            state: true,
        }
    }
}

//----------------------------------------------------------------------------
//            FlipFlop
//----------------------------------------------------------------------------
pub struct FlipFlop {
    input_ptr:  usize,
    inputs:     Fifo,
    pub state:  bool,
    output:     bool,
}
impl FlipFlop {
    pub fn new() -> Self {
        FlipFlop {
            input_ptr: 0,
            inputs:    Fifo::new(1),
            state:     false,
            output:    false,
        }
    }   
}

impl Gate for FlipFlop {
    fn get_input_ptr(&self) -> usize {
        return 0;
    }
    fn set_input_ptr(&mut self, i:usize) {
        self.input_ptr;
    }
    fn set_input(&mut self, pulse: Vec<Option<bool>>) {

//       * I should add a test if size is allready 1, then crash and error, cuz should never have more than 1 input !!!!!

        let checked_pulse= match pulse[0] {
            Some(value) => value,
            None => {println!("Bad Pulse sent do FlipFlop: recieved Vec<Option<None>>, expected Vec<Option<bool>>"); exit(1)},
        };
        if !checked_pulse {
            self.output = true;
            self.state = !self.state; //this is the flipflop  flipping on a negative pulse
        }
        self.inputs.push(pulse)
    }
    fn get_output(&mut self) -> Option<bool> {
       //let count = self.inputs.elements.iter().filter(|item| item.is_some()).count();
       //println!("count: {}",count);
        if self.output {
            self.output = false;
            Some(self.state)
        } else {
            None
        }
    }
}







//----------------------------------------------------------------------------
pub struct GateList {
    pub gates: Vec<Box<dyn Gate>>,
    pub gatemap: HashMap<String, usize>,
}
impl GateList {
    pub fn new()-> Self {
        GateList {
            gates: Vec::<Box<dyn Gate>>::new(),
            gatemap: HashMap::new(),
        }
    }
    pub fn put(&mut self, gate:Box<dyn Gate>, name: String) {
        self.gates.push(gate);
        let newptr = self.gates.len() -1;
        self.gatemap.insert(name, newptr );
    }
    // this currently returns a ptr to the gate in gates, but I would like to 
    // change it to return a mut& to the gate directly, so I can make gates and gatemap private
    pub fn get(&self, name: String) -> usize {
        match self.gatemap.get(&name) {
            Some(ptr) => return *ptr,
            None => { println!("Could not find gate in gatemap: {}",name); exit(1) }
        }
    }
}

