// Define a trait for logic gates with variable inputs
trait LogicGate {
    fn input(&self, inputs: &[bool]) -> bool;
}

// Implement the trait for different logic gates
struct AndGate;
struct NandGate;
struct OrGate;
struct XorGate;
struct Inverter;
struct Toggle {
    state: bool,
}
impl Toggle {
    pub fn new() -> Self {
        Toggle {
            state: false,
        }
    }   
}

//impl LogicGate for Toggle {
//    fn input(&self, inputs: &[bool]) -> bool {
        // how am I going to deal with hight state it does nothing.. it has to return bool.
        // i may have to change the return of all inputs to options<Some(bool),Nothing>
        // this would allow me to clock, as if a gate has not input it can still be clocked in  clocking loo;
        // and, jsut return Nothing if it had no input.
//    }
//}

impl LogicGate for AndGate {
    fn input(&self, inputs: &[bool]) -> bool {
        inputs.iter().all(|&x| x)
    }
}

impl LogicGate for NandGate {
    fn input(&self, inputs: &[bool]) -> bool {
        !inputs.iter().all(|&x| x)
    }
}

impl LogicGate for OrGate {
    fn input(&self, inputs: &[bool]) -> bool {
        inputs.iter().any(|&x| x)
    }
}

impl LogicGate for XorGate {
    fn input(&self, inputs: &[bool]) -> bool {
        inputs.iter().fold(false, |acc, &x| acc ^ x)
    }
}

impl LogicGate for Inverter {
    fn input(&self, inputs: &[bool]) -> bool {
        !inputs[0]
    }
}

// Function to process inputs using any logic gate
fn process_gate<G: LogicGate>(gate: &G, inputs: &[bool]) -> bool {
    gate.input(inputs)
}

pub fn test_gates() {
    let and_gate = AndGate;
    let nand_gate = NandGate;
    let or_gate = OrGate;
    let xor_gate = XorGate;
    let inverter = Inverter;

    println!("");
    println!("AND Gate: 0 0 0 = {}", process_gate(&and_gate, &[false, false, false]));
    println!("AND Gate: 0 0 1 = {}", process_gate(&and_gate, &[false, false, true]));
    println!("AND Gate: 0 1 0 = {}", process_gate(&and_gate, &[false, true,  false]));
    println!("AND Gate: 0 1 1 = {}", process_gate(&and_gate, &[false, true,  true]));
    println!("AND Gate: 1 0 0 = {}", process_gate(&and_gate, &[true,  false, false]));
    println!("AND Gate: 1 0 1 = {}", process_gate(&and_gate, &[true,  false, true]));
    println!("AND Gate: 1 1 0 = {}", process_gate(&and_gate, &[true,  true,  false]));
    println!("AND Gate: 1 1 1 = {}", process_gate(&and_gate, &[true,  true,  true]));

    println!("");
    println!("NAND Gate: 0 = {}", process_gate(&nand_gate, &[false]));
    println!("NAND Gate: 1 = {}", process_gate(&nand_gate, &[true]));
    println!("");
    println!("NAND Gate: 0 0 0 = {}", process_gate(&nand_gate, &[false, false, false]));
    println!("NAND Gate: 0 0 1 = {}", process_gate(&nand_gate, &[false, false, true]));
    println!("NAND Gate: 0 1 0 = {}", process_gate(&nand_gate, &[false, true,  false]));
    println!("NAND Gate: 0 1 1 = {}", process_gate(&nand_gate, &[false, true,  true]));
    println!("NAND Gate: 1 0 0 = {}", process_gate(&nand_gate, &[true,  false, false]));
    println!("NAND Gate: 1 0 1 = {}", process_gate(&nand_gate, &[true,  false, true]));
    println!("NAND Gate: 1 1 0 = {}", process_gate(&nand_gate, &[true,  true,  false]));
    println!("NAND Gate: 1 1 1 = {}", process_gate(&nand_gate, &[true,  true,  true]));

    println!("");
    println!("OR Gate: 0 0 0 = {}", process_gate(&or_gate, &[false, false, false]));
    println!("OR Gate: 0 0 1 = {}", process_gate(&or_gate, &[false, false, true ]));
    println!("OR Gate: 0 1 0 = {}", process_gate(&or_gate, &[false, true,  false]));
    println!("OR Gate: 0 1 1 = {}", process_gate(&or_gate, &[false, true,  true ]));
    println!("OR Gate: 1 0 0 = {}", process_gate(&or_gate, &[true,  false, true ]));
    println!("OR Gate: 1 0 1 = {}", process_gate(&or_gate, &[true,  false, false]));
    println!("OR Gate: 1 1 0 = {}", process_gate(&or_gate, &[true,  true,  true ]));
    println!("OR Gate: 1 1 1 = {}", process_gate(&or_gate, &[true,  true,  false]));

    println!("");
    println!("XOR Gate: 0 0 0 = {}", process_gate(&xor_gate, &[false, false, false]));
    println!("XOR Gate: 0 0 1 = {}", process_gate(&xor_gate, &[false, false, true ]));
    println!("XOR Gate: 0 1 0 = {}", process_gate(&xor_gate, &[false, true,  false]));
    println!("XOR Gate: 0 1 1 = {}", process_gate(&xor_gate, &[false, true,  true ]));
    println!("XOR Gate: 1 0 0 = {}", process_gate(&xor_gate, &[true,  false, false]));
    println!("XOR Gate: 1 0 1 = {}", process_gate(&xor_gate, &[true,  false, true ]));
    println!("XOR Gate: 1 1 0 = {}", process_gate(&xor_gate, &[true,  true,  false]));
    println!("XOR Gate: 1 1 1 = {}", process_gate(&xor_gate, &[true,  true,  true ]));

    println!("");
    println!("Inverter: 0 = {}", process_gate(&inverter, &[false]));
    println!("Inverter: 1 = {}", process_gate(&inverter, &[true]));
}