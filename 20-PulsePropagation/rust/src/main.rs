mod test_and_debug;
use test_and_debug::*;
//mod components;
//use components::*;
//mod gates;
//use gates::*;
mod get_inputs;
use get_inputs::*;

mod loadgates;
#[allow(unused_imports)]
use loadgates::*;

fn main() {
    //let gd = get_gd("GameData-Test1.dat");
    //let gd = get_gd("GameData-Test2.dat");
    //let gd = get_gd("GameData-Test3.dat");
    let cmds = get_gd("GameData.dat");
    for cmd in cmds {
        println!("{}", cmd.display_h());
        println!("test cmd.name: {}",cmd.name);
    }


    // testing ***********************************************
    test_flipflop();

    test_gatelist();

    test_nandgate();

}

