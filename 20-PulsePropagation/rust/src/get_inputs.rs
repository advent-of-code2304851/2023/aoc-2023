use std::process::*;

pub struct Instruction {
    pub gate_type: String,
    pub name: String,
    pub dest_gates: Vec<String>,    
}
impl Instruction {
    #[allow(dead_code)]
    pub fn display_h(&self) -> String {
        let display:String = format!("--gate type:{}, name:{}, destinations:{:?}",self.gate_type, self.name, self.dest_gates);
        return display.to_string()
    }

    #[allow(dead_code)]
    pub fn display_v(&self) -> String {
        let display:String = format!("----------------\ngate type:{}\nname:{}\ndestinations:{:?}",self.gate_type, self.name, self.dest_gates);
        return display.to_string()
    }
}


//------------------------------------------------------------------
//--   parse the GameData string into datastructures              --
//------------------------------------------------------------------
pub fn get_gd(filename: &str) -> Vec<Instruction> {
    let mut sgd:String = get_file(filename);
    sgd = reorder_data(&mut sgd);
    
    #[allow(unused_variables)]
    let cmd: String;
    let mut cmds: Vec<Instruction> = Vec::new();
    
    for cmd in sgd.lines().map( |s| {s.to_string()} ) {
        cmds.push(gd_parse(cmd));
    };
    
    return cmds
}

fn gd_parse(s: String) -> Instruction {
    let left: String;
    let right: String;

    if let Some(pos) = s.find(" -> ") {
        left = s[..pos].to_string();
        right = s[pos+4..].to_string();
    } else {
        println!("Bad Input : no -> found");
        exit(1);
    }

    let cmd: Instruction;

    if left == "broadcaster" {
        cmd = Instruction {
            gate_type: left[0..].to_string(),
            name: left[0..].to_string(),
            dest_gates: right.split(",").map(|s| {s.trim().to_string()} ).collect(),
        };
    } else {
        cmd = Instruction {
            gate_type: left[0..1].to_string(),
            name: left[1..].to_string(),
            dest_gates: right.split(",").map(|s| {s.trim().to_string()} ).collect(),
        };
    }

    //println!("{}", cmd.display_v());
    return cmd

}

fn reorder_data(s: &mut String) -> String {
    if let Some(pos) = s.find("broadcaster") {
        if pos == 0 {
            return s.to_string(); // The word is already at the start
        }
        let (part1, part2) = s.split_at(pos);
        let part2 = if part2.ends_with('\n') {
            part2.to_string()
        } else {
            format!("{}\n", part2)
        };
        format!("{}{}", part2, part1)
    } else {
        s.to_string() // Return the original string if the word is not found
    }
}

//------------------------------------------------------------------
//--   get the GameData file into a string                        --
//------------------------------------------------------------------
pub fn get_file(filename: &str) -> String { 
    use std::env::current_dir;
    use std::fs::read_to_string;
    use std::process::exit;

    let current_dir = current_dir().unwrap();
    let path = current_dir.join("..").join(filename);
    let sgamedata:String;

    if !path.exists() {
        println!("File does not exist : {}", path.display());
        exit(1);
    } else {
        match read_to_string(path) {
            Ok(content) => {
                sgamedata = content;
            }
            Err(e) => {
                println!("Error reading game data {e}");
                exit(1);
            }
        }
    };
    return sgamedata
}





