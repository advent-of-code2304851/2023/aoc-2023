mod get_inputs;
use get_inputs::*;

mod processing;
use processing::*;

fn main() {
    let (parts, rules) = get_gd("GameData.dat");
    //let (parts, rules) = get_gd("GameData-Test.dat");
    //let (parts, rules) = get_gd("GameData-Test2.dat");
    
    println!("\nRules:");
    for (key, value) in &rules { println!("rulename: {}, rule: {}", key, value); }
    println!("\nParts:");
    for part in &parts { println!("part {}", part.display()) ; }
    
    println!("\nStart Processing:");

    let mut count: u32 = 0;
    for (i, part) in parts.iter().enumerate() {
        println!("\n--Part {}:--",i);
        println!("Part: ({}):{}",i, part.display());
        count += check_part(*part, &rules);
    }
    println!("\nCount: {}",count);
}
 