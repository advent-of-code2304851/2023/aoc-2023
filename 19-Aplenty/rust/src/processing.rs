use std::collections::HashMap;
use evalexpr::eval;
use std::process::exit;

struct RuleBits {
    exp: String,
    true_bit: String,
    false_bit: String
}
impl RuleBits {
    pub fn new(rule: &String) -> RuleBits {
        let the_rest: String;
        let i =  match rule.find(":") {
            Some(i) => { i },
            None => {println!("problem finding ':' in rule:({})",rule);exit(1)},
        };
        let exp = rule[..i].to_string();
        the_rest = rule[i+1..].to_string();

        let i =  match the_rest.find(",") {
            Some(i) => { i },
            None => {println!("problem finding ':' in rule:({})",rule);exit(1)},
        };

        let true_bit = the_rest[..i].to_string();
        let false_bit = the_rest[i+1..].to_string();

        return RuleBits{ exp, true_bit, false_bit }
    }

    pub fn prep_exp(&self, part:Part) -> String {
        let var:&str = &self.exp.clone()[..1];
        let val = match var {
            "x" => part.x.to_string(),
            "m" => part.m.to_string(),
            "a" => part.a.to_string(),
            "s" => part.s.to_string(),
            _ => { println!("check_part: match xmas char not found {}", var); exit(4)}
        };
        let sub_exp:String = self.exp.replace(&var, &val.to_string());        
        return sub_exp
    }
}

use crate::get_inputs::*;

pub fn check_part(part: Part, rules: &HashMap<String, String>) -> u32 {
    let mut emergency_break_counter: u32 = 0;
    let mut count:u32 = 0;
    let name= "in";
    let srule:String = lookup_rule(name, rules);
    let mut rule = RuleBits::new(&srule);
    println!("  Pre loop srule: ({})", srule);

    'main_loop: loop {
        println!("Start Loop:\n  Rule Bits: (    Expression: ({}), True part: ({}),  False / the rest: ({})",rule.exp, rule.true_bit, rule.false_bit);
        let exp_sub = rule.prep_exp(part);
        let result = match eval_exp(&exp_sub) {
            true => &rule.true_bit,
            false => &rule.false_bit,
        };
        println!("Sub_expL: ({}) ->  Winning side : ({})",exp_sub ,result);

        match result.chars().collect::<Vec<_>>().as_slice() {
            ['A',..] => { 
                count += part.sumratings(); 
                println!("   ************Accept part - count: {}",count);
                break
            },

            ['R',..] => { println!("   ***********Reject part");  break },

            [_,'>',..]|
            [_,'<',..] => { 
                println!("      Found an equation");
                rule = RuleBits::new(result);
            },

            _ => {   // no other match means its a rule lookup *****
                println!("New Rule name : ");
                let srule:String = lookup_rule(result, rules);
                rule = RuleBits::new(&srule);
            },
        }

        emergency_break_counter += 1;
        if emergency_break_counter > 50 { break 'main_loop; }
    }
    return count
}   


fn lookup_rule(name:&str, rules: &HashMap<String, String>) -> String {
    if let Some(rule_string) = rules.get( name ) {
        let new_rule_string: String = rule_string.to_string();
        return (*new_rule_string).to_string();
    } else {
        println!("Name {} not found in rules... Somethings fucked!", name);
        exit(2)
    };

}

fn eval_exp(exp: &str) -> bool {
    match eval(&exp) {
        Ok(result) => {
            return result.as_boolean().unwrap()
        }
        Err(e) => {
            println!("test_exp({}) failed to evaluate expression:{}", exp, e);
            exit(4)
        }
    }
}