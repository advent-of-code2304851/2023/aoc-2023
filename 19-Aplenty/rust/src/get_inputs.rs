use std::collections::HashMap;
#[derive(Copy, Clone)]
pub struct Part {
    pub x:u32,
    pub m:u32,
    pub a:u32,
    pub s:u32,
}

impl Part{
    #[allow(dead_code)]
    pub fn display(&self) -> String {
        let output = format!("x:{:>5}, m:{:>5}, a:{:>5}, s:{:>5}",self.x,self.m,self.a,self.s);
        return output;
    }
    pub fn sumratings(&self) -> u32 {
        self.x + self.m + self.a + self.s
    }
}

pub fn get_gd(filename: &str) -> (Vec<Part>, HashMap<String, String>) {
    let sgd:String = get_file(filename);
    //println!("GD:\n{}",sgd);

    let mut sgd_parts: Vec<Vec<String>> = Vec::new();
     sgd_parts.push(Vec::new()); 
     sgd_parts.push(Vec::new());

    let mut part = 0;
    for line in sgd.lines() {
        if line.trim().is_empty() { 
            part = 1 
        } else {
            sgd_parts[part].push(line.to_string());
        }
    }   
    //-----Get-Rules-from String to HarmMap--------------------------------
    let rules = get_rules(&sgd_parts[0]);

    //-----Get-Parts-from String to Vec<Part>------------------------------
    let parts = get_parts(&sgd_parts[1]);
    
    return (parts, rules)
}

//------------------------------------------------------------------
fn get_rules(sgd_rules: &Vec<String>) -> HashMap<String, String> {
    let mut rules: HashMap<String, String> = HashMap::new();
    for line in sgd_rules.iter() {
        //println!("Line: {}",line);
        let splitpoint  = line.find('{').unwrap();
        let rule_name: String = line[..splitpoint].to_string();
        let rule: String = line[splitpoint+1..line.len()-1].to_string();
        rules.insert(rule_name, rule);   
    }
    return rules
}
//------------------------------------------------------------------
fn get_parts(sgd_parts: &Vec<String>)  -> Vec<Part> {
    let mut parts:Vec<Part> = Vec::new();
    for line in sgd_parts.iter() {
        let spart:String = line[1..line.len()-1].to_string();
        //println!("sPart: {}",spart);
        let mut x:u32 = 0;
        let mut m:u32 = 0;
        let mut a:u32 = 0;
        let mut s:u32 = 0;
        for srating in spart.split(',') {
            //println!("Sratings : {}",srating);
            let rating_class: &str = &srating[..1];
            let rating_value: u32 = srating[2..].parse().unwrap();
            //println!("class: {}, Value: {}",rating_class, rating_value);
            match rating_class {
                "x" => { x = rating_value; },
                "m" => { m = rating_value; },
                "a" => { a = rating_value; },
                "s" => { s = rating_value; },
                _ => {},
            }
        }    
        let temp_part:Part = Part{ x, m, a, s };
        parts.push(temp_part);
    }
    return parts
}

//------------------------------------------------------------------
pub fn get_file(filename: &str) -> String { 
    use std::env::current_dir;
    use std::fs::read_to_string;
    use std::process::exit;

    let current_dir = current_dir().unwrap();
    let path = current_dir.join("..").join(filename);
    let sgamedata:String;

    if !path.exists() {
        println!("File does not exist : {}", path.display());
        exit(1);
    } else {
        match read_to_string(path) {
            Ok(content) => {
                sgamedata = content;
            }
            Err(e) => {
                println!("Error reading game data {e}");
                exit(1);
            }
        }
    };
    return sgamedata
}