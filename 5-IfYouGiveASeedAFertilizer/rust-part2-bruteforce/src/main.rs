const SRC: usize = 1;
const DST: usize = 0;
const LEN: usize = 2;

fn main() {
    println!("------Start-------");
    let almanac = get_file(("GameData.txt").to_string());
    let(seeds,
        soil_map, 
        fert_map,
        water_map,
        light_map,
        temp_map,
        humid_map,
        loc_map ) = parce_almanac(almanac);
    //my_vec.sort_by(|a, b| a[0].cmp(&b[0]));
    //soil_map.sort_by(|a, b| a[1].cmp(&b[1]));
    
    let mut locations: Vec<u64> = Vec::new();
    for seed_type in seeds.iter().map(|&x| x as u64) {
        let soil_type = get_dst(soil_map.clone(),seed_type);
        let fert_type = get_dst(fert_map.clone(),soil_type);
        let water_type = get_dst(water_map.clone(),fert_type);
        let light_type = get_dst(light_map.clone(),water_type);
        let temp_type = get_dst(temp_map.clone(), light_type);
        let humid_type = get_dst(humid_map.clone(), temp_type);
        let loc_type = get_dst(loc_map.clone(), humid_type);
        println!(" Seed type: {}, Soil type: {}, Fertilizer type: {}, Water type: {}, Light type: {}, Temp type: {}, Humidity type: {}, Location type: {}",seed_type, soil_type,fert_type,water_type,light_type,temp_type,humid_type,loc_type);
        locations.push(loc_type);
    }
    println!("\n ****** The lowest Location number is: {:?}", locations.iter().min());
}

fn get_file(filename: String) -> String {
    use std::env;
    use std::fs;

    let current_dir = env::current_dir().unwrap();
    let path = current_dir.join(filename);
    let content = fs::read_to_string(path).unwrap();

    return content;
}

fn parce_almanac(almanac: String)  -> (Vec<u64>, Vec<Vec<u64>>, Vec<Vec<u64>>, Vec<Vec<u64>>, Vec<Vec<u64>>, Vec<Vec<u64>>, Vec<Vec<u64>>, Vec<Vec<u64>>) {
    let mut seeds:Vec<u64> = Vec::new();
    let mut seed2soil_map = Vec::new();
    let mut soil2fert_map = Vec::new();
    let mut fert2watr_map = Vec::new();
    let mut watr2light_map = Vec::new();
    let mut light2temp_map = Vec::new();
    let mut tmp2humid_map = Vec::new();
    let mut humid2loc_map = Vec::new();

    //println!("The Almanac: {}",almanac);
    let mut lines = almanac.lines();
    while let Some(line) = lines.next() {
        //println!(" Line: {}",line);
        //if line.trim().is_empty() { println!("----Found empty line"); }
        if line.contains(":") {
            //println!("-----Thisline is a section ID; {}",line);
            let section = line.split(":").nth(0).unwrap();
            match section {
                "seeds" =>  {
                        let s_seeds = line.split(": ").nth(1).unwrap();
                        //let mut seeds: Vec<u64> = s_seeds.split_whitespace().map(|s| s.parse().unwrap()).collect();
                        // WORKING!  seeds = s_seeds.split_whitespace().map(|s| s.parse().unwrap()).collect();
                        seeds = s_seeds.split_whitespace().map(|s| s.parse().unwrap()).collect();
                        //println!(" - Seeds: {:?}",seeds);
                },
                "seed-to-soil map" =>  {
                        let mut sec_line = lines.next().unwrap_or("");
                        while !sec_line.trim().is_empty() { 
                            let map: Vec<u64> = sec_line.split_whitespace().map(|s| s.parse().unwrap()).collect();
                            seed2soil_map.push(map);
                            sec_line = lines.next().unwrap_or("");
                        }    
                        //println!(" - - Map as Vec<u64> : {:?} ", seed2soil_map);
                }
                "soil-to-fertilizer map" =>  {
                        let mut sec_line = lines.next().unwrap_or("");
                        while !sec_line.trim().is_empty() { 
                            let map: Vec<u64> = sec_line.split_whitespace().map(|s| s.parse().unwrap()).collect();
                            soil2fert_map.push(map);
                            sec_line = lines.next().unwrap_or("");
                        }
                        //println!(" - - Map as Vec<u64> : {:?} ", soil2fert_map);
                }
                "fertilizer-to-water map" =>  {
                        let mut sec_line = lines.next().unwrap_or("");
                        while !sec_line.trim().is_empty() { 
                            let map: Vec<u64> = sec_line.split_whitespace().map(|s| s.parse().unwrap()).collect();
                            fert2watr_map.push(map);
                            sec_line = lines.next().unwrap_or("");
                        }
                        //println!(" - - Map as Vec<u64> : {:?} ", fert2watr_map);
                }
                "water-to-light map" =>  {
                        let mut sec_line = lines.next().unwrap_or("");
                        while !sec_line.trim().is_empty() { 
                            let map: Vec<u64> = sec_line.split_whitespace().map(|s| s.parse().unwrap()).collect();
                            watr2light_map.push(map);
                            sec_line = lines.next().unwrap_or("");
                        }
                        //println!(" - - Map as Vec<u64> : {:?} ", watr2light_map);
                }
                "light-to-temperature map" =>  {
                        let mut sec_line = lines.next().unwrap_or("");
                        while !sec_line.trim().is_empty() { 
                            let map: Vec<u64> = sec_line.split_whitespace().map(|s| s.parse().unwrap()).collect();
                            light2temp_map.push(map);
                            sec_line = lines.next().unwrap_or("");
                        }
                        //println!(" - - Map as Vec<u64> : {:?} ", light2temp_map);
                }
                "temperature-to-humidity map" =>  {
                        let mut sec_line = lines.next().unwrap_or("");
                        while !sec_line.trim().is_empty() { 
                            let map: Vec<u64> = sec_line.split_whitespace().map(|s| s.parse().unwrap()).collect();
                            tmp2humid_map.push(map);
                            sec_line = lines.next().unwrap_or("");
                        }
                        //println!(" - - Map as Vec<u64> : {:?} ", tmp2humid_map);
                }
                "humidity-to-location map" =>  {
                        let mut sec_line = lines.next().unwrap_or("");
                        while !sec_line.trim().is_empty() { 
                            let map: Vec<u64> = sec_line.split_whitespace().map(|s| s.parse().unwrap()).collect();
                            humid2loc_map.push(map);
                            sec_line = lines.next().unwrap_or("");
                        }
                        //println!(" - - Map as Vec<u64> : {:?} ", humid2loc_map);
                }
                _ => {},
            }
        }
    }
//soil_map.sort_by(|a, b| a[1].cmp(&b[1]));
    seed2soil_map.sort_by(|a, b| a[SRC].cmp(&b[SRC])); 
    soil2fert_map.sort_by(|a, b| a[SRC].cmp(&b[SRC]));
    fert2watr_map.sort_by(|a, b| a[SRC].cmp(&b[SRC]));
    watr2light_map.sort_by(|a, b| a[SRC].cmp(&b[SRC]));
    light2temp_map.sort_by(|a, b| a[SRC].cmp(&b[SRC]));
    tmp2humid_map.sort_by(|a, b| a[SRC].cmp(&b[SRC]));
    humid2loc_map.sort_by(|a, b| a[SRC].cmp(&b[SRC]));

    return( seeds, 
            seed2soil_map, 
            soil2fert_map,
            fert2watr_map,
            watr2light_map,
            light2temp_map,
            tmp2humid_map,
            humid2loc_map,
    );
}

fn get_dst(map: Vec<Vec<u64>>, key: u64)->u64 {
    let mut ret_val:u64 = key;
    //println!("Key: {}", key);

    for vec in map.iter() {
        let src = vec[SRC];
        let dst = vec[DST];
        let len = vec[LEN];
        let upper = src + len - 1;

        if key >= src && key <= upper { 
            let offset = key - src;
            ret_val = dst + offset;
        //} else {
        //    println!("Range failed\n-------");
        }
    }
    return ret_val;
}