#[derive(Clone)]
#[derive(Debug)]
#[derive(Default)]
pub struct Alamanac {
    pub seeds: Vec<SeedGroup>,
    pub soils: Vec<Map>,
    pub ferts: Vec<Map>,
    pub waters: Vec<Map>,
    pub lights: Vec<Map>,
    pub temps: Vec<Map>,
    pub humids: Vec<Map>,
    pub locs: Vec<Map>
}

#[derive(Copy, Clone)]
#[derive(Debug)]
#[derive(Default)]
pub struct Map {
    pub dst: i64,
    pub src: i64,
    pub len: i64,
}
#[derive(Copy, Clone)]
#[derive(PartialEq)]
#[derive(Default)]
#[derive(Debug)]
#[derive(Ord)]
#[derive(Eq)]
#[derive(PartialOrd)]
pub struct SeedGroup {
    pub start: i64,
    pub len: i64,
}

pub fn get_almanac(filename:&str)  -> Alamanac {
    let mut almanac: Alamanac = Default::default();
    let s_almanac = get_file((filename).to_string());
    
    let seeds_sec = get_section("seeds:", s_almanac.clone()).replace("seeds: ", "");
    almanac.seeds = get_seeds(seeds_sec.clone());

    //let s_soil_sec = get_section("seed-to-soil map:", s_almanac.clone()).replace("seed-to-soil map:\r\n", "");
    let s_soil_sec = get_section("seed-to-soil map:", s_almanac.clone());
    almanac.soils = parse_section(s_soil_sec);

    let s_fert_sec = get_section("soil-to-fertilizer map:", s_almanac.clone());
    almanac.ferts = parse_section(s_fert_sec);

    let s_water_sec = get_section("fertilizer-to-water map:", s_almanac.clone());
    almanac.waters = parse_section(s_water_sec);

    let s_light_sec = get_section("water-to-light map:", s_almanac.clone());
    almanac.lights = parse_section(s_light_sec);

    let s_temp_sec = get_section("light-to-temperature map:", s_almanac.clone());
    almanac.temps = parse_section(s_temp_sec);

    let s_humid_sec = get_section("temperature-to-humidity map:", s_almanac.clone());
    almanac.humids = parse_section(s_humid_sec);

    let s_loc_sec = get_section("humidity-to-location map:", s_almanac);
    almanac.locs = parse_section(s_loc_sec);

    /*
    println!("get_almanac, almanac.seeds :: {:?}", almanac.seeds);
    println!("get_almanac, almanac.soils :: {:?}", almanac.soils);
    println!("get_almanac, almanac.ferts :: {:?}", almanac.ferts);
    println!("get_almanac, almanac.waters :: {:?}", almanac.waters);
    println!("get_almanac, almanac.lights :: {:?}", almanac.lights);
    println!("get_almanac, almanac.temps :: {:?}", almanac.temps);
    println!("get_almanac, almanac.humids :: {:?}", almanac.humids);
    println!("get_almanac, almanac.locs :: {:?}", almanac.locs);
 */

    return almanac;
}

pub fn get_file(filename: String) -> String {
    use std::env;
    use std::fs;
    let current_dir = env::current_dir().unwrap();
    let path = current_dir.join(filename);
    let content = fs::read_to_string(path).unwrap();
    return content;
}

fn get_seeds(seeds_sec:String) -> Vec<SeedGroup> {
    let mut seeds:Vec<SeedGroup> = Vec::new();

    let seed_data:Vec<i64> = seeds_sec.split_whitespace().map(|s| s.parse().unwrap()).collect();
    for window in seed_data.chunks(2) {
        let seed_group = SeedGroup {
            start: window[0],
            len: window[1],
        };
        seeds.push(seed_group);
    }
    return seeds
}

fn get_section(sec_name: &str, s_almanac: String) -> String {
    let mut section = String::new();
    if let Some(start_index) = s_almanac.find(sec_name) {    
        let end_index = s_almanac[start_index..].find("\r\n\r\n").unwrap_or(s_almanac[start_index..].len());
        section = String::from(&s_almanac[start_index..start_index + end_index]);
     }

    let kill_string = format!("{}{}", sec_name, "\r\n");
    section = section.replace(&kill_string, ""); 
    return section;
}

fn parse_section(s_sec: String)->Vec<Map> {
    let mut section = Vec::new();
    for line in s_sec.lines() {
        let numbers: Vec<i64> = line.split_whitespace().map(|s| s.parse().unwrap()).collect();
        let map = Map {
            dst: numbers[0],
            src: numbers[1],
            len: numbers[2],
        };
        section.push(map);
    }
    return section
}
