use crate::SeedGroup;
use crate::Map;

pub fn test_group(seed_groups: Vec<SeedGroup>, section: Vec<Map> ) -> Vec<SeedGroup> {
    let mut dests = Vec::new();
    
    let mut i = 0;
    let mut seeds = seed_groups.clone();
    //println!("\n--------------\n Seeds copy \n--------------\n {:?}", seeds); //*88888888888888888888888888888888888888888888888888888888
    while i < seeds.len() {
        let sg = seeds[i];
        let sg_end = sg.start + sg.len - 1;  // added cuz I think its too long, created and over lap condition instead of an inside.

        let mut all_inner = Vec::new();
        let mut all_outer = Vec::new();
        let mut parts_in  = Vec::new();
        let mut parts_out = Vec::new();

        //println!("_________________________________________________");
        for map in section.iter() {

            let map_end = map.src + map.len - 1;
            let map_ofs = map.dst - map.src;

            match (sg.start, sg_end) {
                (start, end) if start >= map.src && end <= map_end => {
                    //println!(" -- Only Inside  ***");
                    if all_inner.is_empty() { all_inner.push(SeedGroup{ start: sg.start + map_ofs, len: sg.len}); };
                },
                (start, end) if start < map.src && end >= map.src && end <= map_end =>  {
                    //println!(" -- Overlaps start ---");
                    parts_out.push(SeedGroup{ start: sg.start, len: map.src-1 - sg.start + 1});
                    parts_in.push(SeedGroup{ start: map.src + map_ofs ,len: sg_end - map.src + 1});
                },
                (start, end) if start >= map.src && start <= map_end && end > map_end =>  {
                    //println!(" start: {}, >= map.src:  {} && start: {} <= map_end: {} && end: {} > map_end: {}", start, map.src, start, map_end, end, map_end);
                    //println!(" -- Overlaps end +++");
                    parts_in.push(SeedGroup{ start: sg.start + map_ofs, len: map_end - sg.start + 1  });
                    parts_out.push(SeedGroup{ start: (map_end+1), len: sg_end - (map_end+1) }); // why do I not have to add 1 for the len
                },
                (start, end) if start < map.src && end > map_end =>   {
                    //println!(" -- Contains ))))))))");
                    parts_out.push(SeedGroup{start: sg.start, len: map.src-1 - sg.start + 1});
                    parts_in.push(SeedGroup{start:  map.src, len: map.len});
                    parts_out.push(SeedGroup{start: (map_end+1), len: sg_end - (map_end+1) }); // why do I not have to add 1 for the len
                },
                (start, end) if start > map_end || end < map.src =>   {
                    if all_outer.is_empty() { all_outer.push(SeedGroup{ start: sg.start ,len: sg.len}); };
                    all_outer.push(SeedGroup{ start: sg.start ,len: sg.len});
                    //println!(" -- only outside");
                },
                (_,_) =>{
                    //println!(" -- Not tested for");
                },
            }
        }
        i += 1;
    
        if all_inner.len() > 0 { 
            all_inner.sort();
            all_inner.dedup();
            for part in all_inner { 
                dests.push(SeedGroup{start: part.start, len: part.len})
            }
        } else if parts_out.len() > 0 || parts_in.len() > 0 {
            parts_in.sort();
            parts_in.dedup();
            for part in parts_in {
                dests.push(SeedGroup{start: part.start, len: part.len} )
            }
            parts_out.sort();
            parts_out.dedup();
            for part in parts_out {
                seeds.push(SeedGroup{start: part.start, len: part.len})
                //dests.push(SeedGroup{start: part.start, len: part.len})
            }
        } else {
            all_outer.sort();
            all_outer.dedup();
            for part in all_outer {
                dests.push(SeedGroup{start: part.start, len: part.len})
            }
        }
    }
//    dests.sort_by(|a, b| a.start.cmp(&b.start).then(a.len.cmp(&b.len)));
    dests.sort();
    dests.dedup();
return dests;
}

