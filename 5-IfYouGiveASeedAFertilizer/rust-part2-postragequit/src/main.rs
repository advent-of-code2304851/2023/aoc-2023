mod mod_filestuff;
use mod_filestuff::*;

mod mod_processing;
use mod_processing::*;

fn main() {
    println!("------Start-------");
    let almanac = get_almanac("GameData.txt");
    //for seed in almanac.seeds.iter() { println!("seed: {:?}",seed); };

    println!("---Soil Maps-----------------------------------------------");
    //for map in almanac.soils.iter() { println!("Soils: {:?}", map)}
    let soils = test_group(almanac.seeds,almanac.soils);
    //println!("---Soil Returned-------------------------------------------");
    //for soil in soils.iter() { println!("soil: {:?}", soil); };

    println!("---Fert Maps-----------------------------------------------");
    //for map in almanac.ferts.iter() { println!("Ferts: {:?}", map)}
    let ferts  = test_group(soils,almanac.ferts);
    //println!("---Fert Returned-------------------------------------------");
    //for fert in ferts.iter() { println!("fert: {:?}", fert); };

    println!("---Water Map------------------------------------------------");
    //for map in almanac.waters.iter() { println!("Waters: {:?}", map)}
    let waters  = test_group(ferts,almanac.waters);
    //println!("---Water Returned-------------------------------------------");
    //for water in waters.iter() { println!("water: {:?}", water); };
    
    println!("---Lights Map-----------------------------------------------");
    //for map in almanac.lights.iter() { println!("lights: {:?}", map)};
    //println!("---Water Returned-------------------------------------------");
    let lights  = test_group(waters,almanac.lights);
    //for light in lights.iter() { println!("light: {:?}", light); };
    
//std::process::exit(0);
    
    println!("---Temps Map------------------------------------------------");
    //for map in almanac.temps.iter() { println!("temps: {:?}", map); };
    //println!("---Temps Returned-------------------------------------------");
    let temps  = test_group(lights,almanac.temps);
    //for temp in temps.iter() { println!("temp: {:?}", temp); };

    println!("---Humids Map-----------------------------------------------");
    //for map in almanac.humids.iter() { println!("humids: {:?}", map); };
    //println!("---Humids Returned------------------------------------------");
    let humids  = test_group(temps,almanac.humids);
    //for humid in humids.iter() { println!("humid: {:?}", humid); };

    println!("---Locs Map-------------------------------------------------");
    let mut locs  = test_group(humids,almanac.locs);
    //println!("---Humids Returned------------------------------------------");
    locs.sort_by(|a, b| a.start.cmp(&b.start));

    //for loc in locs.iter() { println!("------------loc: {:?}", loc); };
    
    let min_thing = locs.iter().min_by_key(|locs| locs.start);
    println!(" min of the thing : {:?} ", min_thing.unwrap());

//std::process::exit(0);
}

