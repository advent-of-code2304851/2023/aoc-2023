read in file.
parce file in Vecs of all maps ( seeds, seed-to-soil_map, ect. )

create a function to get the offsets setp by step
	get_offset( src, &map ) -> offset
eg: 
	offset = get_offset(seeds[0], &seed-to-soil_map)

--------------------------
This one is harder to follow int instrucitons.

This is my description

seeds: 79 14 55 13

seed-to-soil map:
Dst	Src	Range_lengh
50 	98 	2
52 	50 	48

soil-to-fertilizer map:
Dst	Src	Range_lengh
0 	15 	37
37 	52 	2
39 	0 	15

fertilizer-to-water map:
Dst	Src	Range_lengh
49 	53 	8
0 	11 	42
42 	0 	7
57 	7 	4

water-to-light map:
Dst	Src	Range_lengh
88 	18	7
18 	25 	70

light-to-temperature map:
Dst	Src	Range_lengh
45 	77 	23
81 	45 	19
68 	64 	13

Seed 79, soil 81, fertilizer 81, water 81, light 74, temperature 78, humidity 78, location 82.

walk through:
seed 79
step1-----seed to soil-----------------------------------------------------
find 79 in Src, or src+range_len  79 is greater than 50, but lower than 50+48.
	offset = 79-50 = 29
the Dst, on same record, is 52.  
the Src for the next step is                      (Dst + offset)  52+29 = 81.

Step2------soil to fert----------------------------------------------------
find 81 in Src, or src+range_len-1
81 is greater than 39, and greater than 39+15, so next new Src= old Src = 81.

Step3-------fert to water---------------------------------------------------
find 81 in Src, or src+range_len-1
81 is greater than 57, and greater than 57+4, so next new Src= old Src = 81.

Step4-------water to light---------------------------------------------------
find 81 in Src, or src+range_len-1
81 is in range 18..[18+70-1]  = [18..87]
	offset = (81-18) = 63
	next new Src = Dst+offset, 25 + 63 =                     (should be 74)
Step5----------------------------------------------------------
---- and so on.


79 is in range 52..(52+48-1)  or 52..100 of the seed-to-soid map Dst col
on same record, Src = 50, 

to calculate its Source
	The offset of 79 in the range 52..100  is 79-52=27
	the start of the src range = 50
	the source is the start of src range + offset
	src = 50+27
	src = 81 -- wrong !!!!!

soil-to-fertilizer map:  works the same way, but starts with seed source 81


When reading the gamedata.txt file, 
	search for each group like "seed-to-soil map:"

in each group, loop until find an empty line, something like this.

let my_string = "foo\n\nbar\n";
for line in my_string.lines() {
    if line.trim().is_empty() {
        println!("Empty line found!");
    }
}
----------------------------------
example of a simple loop for this,  without the file reads. ( I love https://play.rust-lang.org  !!!!!).
    let mut map = Vec::new();
    for _ in 0..5 {
        //read map record ie: 50 98 2 - from example
        let rec = "50 98 2";
        //let mut columns: Vec<&str> = rec.split_whitespace().collect();
        let columns: Vec<i32> = rec.split_whitespace().map(|s| s.parse().unwrap()).collect();
        println!(" Here is your collumns vec: {:?}", columns);
        map.push(columns);
    }
    println!(" Here is your map vec: {:?}", map);
