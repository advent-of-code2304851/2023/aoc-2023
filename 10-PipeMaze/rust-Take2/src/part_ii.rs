use crate::move_curr;

use crate::Loc;

pub fn get_inside_area(pipe: &mut Vec<Vec<char>>) {
    let mut start = Loc::new();
    for (y, row) in pipe.iter().enumerate() {
        if let Some(x) = row.iter().position(|&c| c != '+') {
            start.x = x;
            start.y = y;
            break;
        }
    }
    println!("The first char not equal to '+' is at index ({}, {})", start.y, start.x);

    let mut curr = start.clone(); 
    let mut curr_char = pipe[curr.y][curr.x];
    let mut next_move = "right";
    let mut curr_inside = "down";
    println!("Starting Char is : {}",curr_char);
    let mut i = 0;
    loop {
        move_curr(&mut curr, next_move);
        curr_char = pipe[curr.y][curr.x]; 
        (next_move, curr_inside) = get_nextmove( next_move, curr_char, curr_inside);

        print!(" curr Loc : {},{} => {}", curr.x, curr.y, curr_char);
        print!(" - next move : {}", next_move);
        println!(" - inside : {}", curr_inside);

        mark_inside(pipe, curr, curr_inside);

        i += 1;
        if curr == start {break};
        if i == 100000 {break};
    }
    println!(" Curr_char at end of game is : {}",curr_char);
}

fn mark_inside(pipe: &mut Vec<Vec<char>>, curr: Loc , curr_inside: &str) {          //  to fill in areas  for x + 1 only  loop until not not + or @  and if + change to @.
    let curr_char = pipe[curr.y][curr.x].clone();         
    match curr_inside {
        "right" =>  {   let mut ix = curr.x.clone() + 1;
                        while pipe[curr.y][ix] == '+' || pipe[curr.y][ix] == '@' {
                            if pipe[curr.y][ix] == '+' { pipe[curr.y][ix] = '@' }; 
                            ix += 1;
                        };
                        match curr_char {
                            '╗' => {if pipe[curr.y - 1][curr.x] == '+' { pipe[curr.y - 1][curr.x] = '@'}},
                            '╝' => {if pipe[curr.y + 1][curr.x] == '+' { pipe[curr.y + 1][curr.x] = '@'}},
                            _ => {},
                        }
                    },
        "down"  =>  {   if pipe[curr.y + 1][curr.x] == '+' { pipe[curr.y + 1][curr.x] = '@' }; 
                        match curr_char {
                            '╝' => {if pipe[curr.y][curr.x + 1] == '+' { pipe[curr.y][curr.x + 1] = '@'}},
                            '╚' => {if pipe[curr.y][curr.x - 1] == '+' { pipe[curr.y][curr.x - 1] = '@'}},
                            _ => {},
                        }
                    },
        "left"  =>  {   if pipe[curr.y][curr.x - 1] == '+' { pipe[curr.y][curr.x - 1] = '@' }; 
                        match curr_char {
                            '╔' => {if pipe[curr.y - 1][curr.x] == '+' { pipe[curr.y - 1][curr.x] = '@'}},
                            '╚' => {if pipe[curr.y + 1][curr.x] == '+' { pipe[curr.y + 1][curr.x] = '@'}},
                            _ => {},
                        }
                    },
        "up"    =>  {   if pipe[curr.y - 1][curr.x] == '+' { pipe[curr.y - 1][curr.x] = '@' }; 
                        match curr_char {
                            '╔' =>  {if pipe[curr.y][curr.x - 1] == '+' { pipe[curr.y][curr.x - 1] = '@'}},
                            '╗' =>  {
                                        let mut ix = curr.x.clone() + 1;
                                        while pipe[curr.y][ix] == '+' || pipe[curr.y][ix] == '@' {
                                            if pipe[curr.y][ix] == '+' { pipe[curr.y][ix] = '@' }; 
                                            ix += 1;
                                        }    
                                        if pipe[curr.y][curr.x + 1] == '+' { pipe[curr.y][curr.x + 1] = '@'}
                                    },
                            _ => {},
                        }
                    }
        _ => {},
    };
}

fn get_nextmove<'a>(last_move:&'a str, curr_char:char, mut curr_inside:&'a str)-> (&'a str, &'a str) {  // 'a is a lifetime thing I used the compilers recomendation but don't understand it.
    let nextmove:&str = match last_move {
    "right" => { match curr_char {
        '═' => {"right"},
        '╗' => {if curr_inside=="up" {curr_inside="right"}else{curr_inside="left"};"down"},
        '╝' => {if curr_inside=="up" {curr_inside="left"}else{curr_inside="right"};"up"},
        _ => {"None"},
        }
    },
    "down"  => { match curr_char {
        '║' => {"down"},
        '╚' => {if curr_inside=="left" {curr_inside="down"}else{curr_inside="up"};"right"},
        '╝' => {if curr_inside=="left" {curr_inside="up"}else{curr_inside="down"};"left"},
        _ => {"None"},
        }
    },
    "left"  => { match curr_char {
        '═' => {"left"},
        '╚' => {if curr_inside=="up" {curr_inside="right"}else{curr_inside="left"} ;"up"},
        '╔' => {if curr_inside=="up" {curr_inside="left"}else{curr_inside="right"} ;"down"},
        _ => {"None"},
        }
    },
    "up"    => { match curr_char {
        '║' => {"up"},
        '╔' => {if curr_inside=="left" {curr_inside="up"}else{curr_inside="down"} ;"right"},
        '╗' => {if curr_inside=="left" {curr_inside="down"}else{curr_inside="up"} ;"left"},
        _ => {"None"},
        }
    },
    _ => {""},
    };
    return (nextmove, curr_inside);
}
