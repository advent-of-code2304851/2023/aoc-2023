mod get_input;
use get_input::*;
pub mod part_i;
use part_i::*;
mod part_ii;
use part_ii::*;

use std::fs::File;
use std::io::Write;

fn main() {
    let gamedata = get_gamedata();                                                            // get_gamedata

    let (move_count, mut pipe) = get_first_move(&gamedata);     // get_first_move
    println!(" Long Shot: {}",move_count / 2);

    get_inside_area(&mut pipe);

    let path = "pipe_map.txt"; // Change this to your desired file path
    let mut output = File::create(path).unwrap();
    for y in 0..pipe.len() {
        let pipeline:String = pipe[y].clone().into_iter().collect();
        if let Err(err) = writeln!(output, "{:?}", pipeline) {
            eprintln!("Error writing to file: {}", err);
        };
    }

    let inner_area: usize = pipe
        .iter()
        .flatten() // Flatten the inner vectors into a single iterator
        .filter(|&c| *c == '@')
        .count();
    println!(" Number of tiles inside the loop : {}", inner_area);

}


