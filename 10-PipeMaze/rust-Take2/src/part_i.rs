#[derive(PartialEq)]
#[derive(Default)]
#[derive(Clone)]
#[derive(Copy)]
pub struct Loc {
    pub x: usize,
    pub y: usize,
}
impl Loc {
//    pub fn new(x: usize, y: usize) -> Self {
    pub fn new() -> Self {
        Loc { x:0usize, y:0usize }
    }
}

pub fn get_first_move(gamedata: &Vec<Vec<char>>) -> (u64, Vec<Vec<char>> ){
    let start = find_start(&gamedata);
    println!(" start x={} y={} : {}", start.x, start.y, gamedata[start.y][start.x]);

    let xsize = gamedata[0].len();
    let ysize: usize = gamedata.len();
    let mut pipe: Vec<Vec<char>> = vec![vec!['+'; xsize]; ysize];

    let mut next_move: &str = "";
    let mut end_move: &str = "";

    for test_move in vec!["up", "right", "down", "left"] {
        match test_move {
            a if a == "up"    && start.y > 0     && vec!['|', 'F', '7'].contains(&gamedata[start.y-1][start.x]) => { 
                if next_move == "" {next_move = "up";} else {end_move = "up"}; 
            },
            a if a == "right" && start.x < xsize && vec!['-', 'J', '7'].contains(&gamedata[start.y][start.x+1]) => { 
                if next_move == "" {next_move = "right"} else {end_move = "right"}; 
            },
            a if a == "down"  && start.y < xsize && vec!['|', 'J', 'L'].contains(&gamedata[start.y+1][start.x]) => { 
                if next_move == "" {next_move = "down";} else {end_move = "down"}; 
            },
            a if a == "left"  && start.x > 0     && vec!['-', 'L', 'F'].contains(&gamedata[start.y][start.x-1]) => { 
                if next_move == "" {next_move = "left";} else {end_move = "left"}; 
            },
            _ => {},
        };
    };
    println!("*** last_move:{}   end_move:{} ",next_move, end_move);

    let mut curr_char = get_startchar(next_move, end_move);
    let mut curr = start.clone();
    let mut move_count = 1u64;
    loop {
        pipe[curr.y][curr.x] = get_pipe_for(curr_char);
        move_curr(&mut curr, next_move);
        curr_char = gamedata[curr.y][curr.x]; 
        next_move = get_nextmove(next_move, curr_char);
        //print!(" curr Loc : {}, {} => {}",curr.y, curr.x, curr_char);
        //println!(" next move : {}",next_move);

        move_count += 1;
        if curr == start {break};
        if move_count == 100000 {break};
    }
    return (move_count, pipe);
}

pub fn find_start(gamedata: &Vec<Vec<char>>) -> Loc {
    let mut start = Loc::default();
    if let Some((y, x)) = gamedata.iter().enumerate().find_map(|(y, v)| v.iter().position(|&a| a == 'S').map(|x| (y, x))) {
        //println!("The char 'S' is located at index ({}, {})", y, x);
        start = Loc {
            x: x,
            y: y,
        };
    }
    return start;
}

fn get_startchar(next_move:&str, end_move:&str) -> char {
    let start_char = match next_move {
        "up" => match end_move {
            "left"  => 'J', //'╝',
            "down"  => '|', //'║', 
            "right" => 'L', //'╚',
            _ => 'X',
        }
        "right" => match end_move {
            "down"  => 'F', //'╔',
            "left"  => '-', //'═',
            "up"    => 'L', //'╚',
            _ => 'X',
        }
        "down" => match end_move {
            "left"  => '7', //'╗',
            "up"    => '|', //'║',
            "right" => 'F', //'╔',
            _ => 'X',
        }
        "left" => match end_move {
            "up"    => 'J', //'╝',
            "right" => '-', //'═',
            "down"  => '7', //'╗',
            _ => 'X',
        }
        _ => 'X',
    };
    return start_char;
}

pub fn move_curr( curr: &mut Loc, curr_move: &str) {
    match curr_move {
       "right" => {curr.x = curr.x + 1; },
       "down"  => {curr.y = curr.y + 1; },
       "left"  => {curr.x = curr.x - 1; },
       "up"    => {curr.y = curr.y - 1; },
       _ => {},
   }
}

fn get_nextmove(last_move:&str, curr_char:char)-> &str {
    let nextmove:&str = match last_move {
    "right" => { match curr_char {
        '-' => {"right"},
        '7' => {"down"},
        'J' => {"up"},
        _ => {"None"},
        }
    },
    "down"  => { match curr_char {
        '|' => {"down"},
        'L' => {"right"},
        'J' => {"left"},
        _ => {"None"},
        }
    },
    "left"  => { match curr_char {
        '-' => {"left"},
        'L' => {"up"},
        'F' => {"down"},
        _ => {"None"},
        }
    },
    "up"    => { match curr_char {
        '|' => {"up"},
        'F' => {"right"},
        '7' => {"left"},
        _ => {"None"},
        }
    },
    _ => {""},
    };
    return nextmove;
}

fn get_pipe_for(curr_char: char) -> char {
   match curr_char {
    '|' => '║',
    '-' => '═',
    'F' => '╔',
    'L' => '╚',
    '7' => '╗',
    'J' => '╝',
    _ => ' ',
   }
}