#[derive(Default)]
#[derive(Clone)]
struct Loc {
    x: usize,
    y: usize,
}


fn main() {
    let filename = "GameData.txt".to_string();
    //let filename = "GameData-test1.txt".to_string();
    //let filename = "GameData-test2.txt".to_string();
    //let filename = "GameData-test3.txt".to_string();
    //let filename = "GameData-test4.txt".to_string();
    let gamedata = get_gamedata(filename);
    let start = find_start(&gamedata);
    println!(" start x={} y={} : {}", start.x, start.y, gamedata[start.y][start.x]);
    let move_count = get_first_move(&gamedata, start);
    println!(" Long Shot: {}",move_count / 2);
}

fn get_first_move(gamedata: &Vec<Vec<char>>, mut start: Loc) -> u64 {
    let size = gamedata[0].len();
    let mut last_move: &str = "";
    for test_move in vec!["up", "right", "down", "left"] {
        match test_move {
            a if a == "up"    && start.y > 0 && vec!['|', 'F', '7'].contains(&gamedata[start.y-1][start.x]) => { last_move = "up";    start.y -= 1; break},
            a if a == "right" && start.x < size && vec!['-', 'J', '7'].contains(&gamedata[start.y][start.x+1]) => { last_move = "right"; start.x += 1; break},
            a if a == "down"  && start.y < size && vec!['|', 'J', 'L'].contains(&gamedata[start.y+1][start.x]) => { last_move = "down";  start.y += 1; break},
            a if a == "left"  && start.x > 0 && vec!['-', 'L', 'F'].contains(&gamedata[start.y][start.x-1]) => { last_move = "left";  start.x -= 1; break},
            _ => " ",
        };
    };

    let mut move_count = 1u64;
    let mut cur_char = gamedata[start.y][start.x] as char;
    
    while cur_char != 'S' {
        println!(" Current Char {}", cur_char);
        match last_move {
            "up" => {
                match cur_char {
                    '|' => {last_move = "up"; start.y -= 1; },
                    'F' => {last_move = "right"; start.x += 1;},
                    '7' => {last_move = "left"; start.x -=1; },
                    _ => {last_move = "dead"},
                }
            }
            "right" => {
                match cur_char {
                    '-' => {last_move = "right"; start.x += 1; },
                    'J' => {last_move = "up"; start.y -= 1;},
                    '7' => {last_move = "down"; start.y +=1; },
                    _ => {last_move = "dead"},
                }
            }
            "down" => {
                match cur_char {
                    '|' => {last_move = "down"; start.y += 1; },
                    'L' => {last_move = "right"; start.x += 1;},
                    'J' => {last_move = "left"; start.x -=1; },
                    _ => {last_move = "dead"},
                }
            }
            "left" => {
                match cur_char {
                    '-' => {last_move = "left"; start.x -= 1; },
                    'L' => {last_move = "up"; start.y -= 1;},
                    'F' => {last_move = "down"; start.y +=1; },
                    _ => {last_move = "dead"},
                }
            }
            _ => last_move = "dead",
        }
        move_count += 1;
        //println!(" --Next move is {}", last_move);
        //println!(" --Move count   {}", move_count);
        cur_char = gamedata[start.y][start.x] as char;
    }
    return move_count;

}

fn find_start(gamedata: &Vec<Vec<char>>) -> Loc {
    let mut start = Loc::default();
    if let Some((y, x)) = gamedata.iter().enumerate().find_map(|(y, v)| v.iter().position(|&a| a == 'S').map(|x| (y, x))) {
        //println!("The char 'S' is located at index ({}, {})", y, x);
        start = Loc {
            x: x,
            y: y,
        };
    }
    return start;
}

fn get_gamedata(filename: String) -> Vec<Vec<char>> {
    let s_gamedata = get_file(filename);
    let mut gamedata:Vec<Vec<char>> = Vec::new();
    for line in s_gamedata.lines() {
        let vline:Vec<char> = line.chars().collect();
        gamedata.push(vline);
    }
    return gamedata;
}

fn get_file(filename: String) -> String {
    use std::env;
    use std::fs;
    let current_dir = env::current_dir().unwrap();
    let path = current_dir.join("..").join(filename);
    if !path.exists() {
        return " ".to_string();
    };
    let content = fs::read_to_string(path).unwrap();
    return content;
}