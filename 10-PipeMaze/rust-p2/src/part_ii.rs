//mod part_i;
use crate::part_i::move_curr;
use crate::Loc;

pub fn get_inside_area(pipe: &Vec<Vec<char>>) {
    //let area = 0u32;
    let mut start: Loc = Loc { x:0, y:0 };
    for (y, row) in pipe.iter().enumerate() {
        if let Some(x) = row.iter().position(|&c| c != '+') {
            start.x = x;
            start.y = y;
            break;
        }
    }
    println!("The first char not equal to '+' is at index ({}, {})", start.y, start.x);

    let mut curr = start.clone();
    let mut curr_char = pipe[curr.y][curr.x];
    let mut next_move = "right";
    let mut curr_inside = "down";

    let mut i = 0;
    loop {
        print!(" curr Loc : {},{} => {}", curr.x, curr.y, curr_char);
        print!(" - next move : {}", next_move);
        // do something interesting like cound inside tiles

        move_curr(&mut curr, next_move);
        curr_char = pipe[curr.y][curr.x]; 
        (next_move, curr_inside) = get_nextmove(next_move, curr_char, curr_inside);
        println!(" - inside : {}", curr_inside);

        i += 1;
        if curr == start {break};
        if i == 100000 {break};
    }
    println!(" Curr_char at end of game is : {}",curr_char);
}


fn get_nextmove(last_move:&str, curr_char:char, curr_inside:&str)-> (&str, &str) {
    let nextmove:&str = match last_move {
    "right" => { match curr_char {
        '═' => {"right"},
        '╗' => {if curr_inside="up" {curr_inside="right"}else{curr_inside="left"};"down"},
        '╝' => {if curr_inside="up" {curr_inside="left"}else{curr_inside="right"};"up"},
        _ => {"None"},
        }
    },
    "down"  => { match curr_char {
        '║' => {"down"},
        '╚' => {if curr_inside="left" {curr_inside="down"}else{curr_inside="up"};"right"},
        '╝' => {if curr_inside="left" {curr_inside="up"}else{curr_inside="down"};"left"},
        _ => {"None"},
        }
    },
    "left"  => { match curr_char {
        '═' => {"left"},
        '╚' => {if curr_inside="up" {curr_inside-"right"}else{curr_inside="left"} ;"up"},
        '╔' => {if curr_inside="up" {curr_inside-"left"}else{curr_inside="right"} ;"down"},
        _ => {"None"},
        }
    },
    "up"    => { match curr_char {
        '║' => {"up"},
        '╔' => {if curr_inside="left" {curr_inside-"up"}else{curr_inside="down"} ;"right"},
        '╗' => {if curr_inside="left" {curr_inside-"down"}else{curr_inside="up"} ;"left"},
        _ => {"None"},
        }
    },
    _ => {""},
    };
    return (nextmove,curr_inside);
}
