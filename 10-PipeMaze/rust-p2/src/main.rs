use std::fs::File;
use std::io::Write;
//use std::string;
//use std::io::{Error, Write};
#[derive(Default)]
#[derive(Clone)]
struct Loc {
    x: usize,
    y: usize,
}


fn main() {
    //let filename = "GameData-test2.txt".to_string();
    //let filename = "GameData-test3.txt".to_string();
    //let filename = "GameData-test1.txt".to_string();

    //let filename = "GameData-test4.txt".to_string();
    //let filename = "GameDataP2-Test3.txt".to_string();
    let filename = "GameData.txt".to_string();
    let gamedata = get_gamedata(filename);                              // get_gamedata
    let start = find_start(&gamedata);                                             // find_start
    println!(" start x={} y={} : {}", start.x, start.y, gamedata[start.y][start.x]);

    let (move_count, xray, pipe) = get_first_move(&gamedata, start);     // get_first_move
    println!(" Long Shot: {}",move_count / 2);

    get_inside_area(&pipe);

    let path = "xray.txt"; // Change this to your desired file path
    let mut output = File::create(path).unwrap();
    for y in 0..xray.len() {
        //let gameline:String = gamedata[y].clone().into_iter().collect();
        let pipeline:String = pipe[y].clone().into_iter().collect();
        let xrayline:String = xray[y].clone().into_iter().collect();
        //println!("{:?}\t{:?}", gamedata[y], xray[y]);
        if let Err(err) = writeln!(output, "{:?}\t{:?}", pipeline, xrayline) {
            eprintln!("Error writing to file: {}", err);
        };
    }
}

fn get_inside_area(pipe: &Vec<Vec<char>>) {
    let area = 0u32;
    let mut start: Loc = Loc { x:0, y:0 };

    for (y, row) in pipe.iter().enumerate() {
        if let Some(x) = row.iter().position(|&c| c != '+') {
            start.x = x;
            start.y = y;
            break;
        }
    }
    println!("The first char not equal to '+' is at index ({}, {})", start.y, start.x);
}

fn get_first_move(gamedata: &Vec<Vec<char>>, mut start: Loc) -> (u64, Vec<Vec<char>>, Vec<Vec<char>> ){
    let xsize = gamedata[0].len();
    let ysize: usize = gamedata.len();
    let mut xray: Vec<Vec<char>> = vec![vec!['+'; xsize]; ysize];
    let mut pipe: Vec<Vec<char>> = vec![vec!['+'; xsize]; ysize];
    xray[start.y][start.x] = '#';
    pipe[start.y][start.x] = '#';

    let mut ydif = 0i8; let mut xdif = 0i8;
    let mut last_move: &str = "";
    let mut end_move: &str = "";

    for test_move in vec!["up", "right", "down", "left"] {
        match test_move {
            a if a == "up"    && start.y > 0     && vec!['|', 'F', '7'].contains(&gamedata[start.y-1][start.x]) => { 
                if last_move == "" {last_move = "up";   ydif = -1i8;} else {end_move = "up"}; 
            },
            a if a == "right" && start.x < xsize && vec!['-', 'J', '7'].contains(&gamedata[start.y][start.x+1]) => { 
                if last_move == "" {last_move = "right"; xdif = 1i8;} else {end_move = "right"}; 
            },
            a if a == "down"  && start.y < xsize && vec!['|', 'J', 'L'].contains(&gamedata[start.y+1][start.x]) => { 
                if last_move == "" {last_move = "down"; ydif = 1i8;} else {end_move = "down"}; 
            },
            a if a == "left"  && start.x > 0     && vec!['-', 'L', 'F'].contains(&gamedata[start.y][start.x-1]) => { 
                if last_move == "" {last_move = "left"; xdif = -1i8;} else {end_move = "left"}; 
            },
            _ => {},
        };
    };

    let start_char = match last_move {
        "up" => match end_move {
            "left"  => '╝',
            "down"  => '║',
            "right" => '╚',
            _ => 'X',
        }
        "right" => match end_move {
            "down"  => '╔',
            "left"  => '═',
            "up"    => '╚',
            _ => 'X',
        }
        "down" => match end_move {
            "left"  => '╗',
            "up"    => '║',
            "right" => '╔',
            _ => 'X',
        }
        "left" => match end_move {
            "up"    => '╝',
            "right" => '═',
            "down"  => '╗',
            _ => 'X',
        }
        _ => 'X',
    };

    println!("*** last_move:{}   end_move:{} ",last_move, end_move);
    println!("*** xdif: {}    ydif: {}", xdif, ydif);
    println!("*** Start_char : {}", start_char);

    pipe[start.y][start.x] = start_char;

    start.x = start.x + xdif as usize;
    start.y = start.y + ydif as usize ;

    let mut move_count = 1u64;
    let mut cur_char = gamedata[start.y][start.x] as char; 
    
    println!("*** Cur_Char at start of loop : {}", cur_char);

    while cur_char != 'S' {
        xray[start.y][start.x] = '#';
        pipe[start.y][start.x] = match cur_char {
            '|' => '║',
            '-' => '═',
            '7' => '╗',
            'J' => '╝',
            'L' => '╚',
            'F' => '╔',
            'S' => 'X',
            _ => ' ',
        };

        println!(" Current Char {} : {}", cur_char, pipe[start.y][start.x]);
        match last_move {
            "up" => {
                match cur_char {
                    '|' => {last_move = "up"; start.y -= 1; },
                    'F' => {last_move = "right"; start.x += 1;},
                    '7' => {last_move = "left"; start.x -=1; },
                    _ => {last_move = "dead"},
                }
            }
            "right" => {
                match cur_char {
                    '-' => {last_move = "right"; start.x += 1; },
                    'J' => {last_move = "up"; start.y -= 1;},
                    '7' => {last_move = "down"; start.y +=1; },
                    _ => {last_move = "dead"},
                }
            }
            "down" => {
                match cur_char {
                    '|' => {last_move = "down"; start.y += 1; },
                    'L' => {last_move = "right"; start.x += 1;},
                    'J' => {last_move = "left"; start.x -=1; },
                    _ => {last_move = "dead"},
                }
            }
            "left" => {
                match cur_char {
                    '-' => {last_move = "left"; start.x -= 1; },
                    'L' => {last_move = "up"; start.y -= 1;},
                    'F' => {last_move = "down"; start.y +=1; },
                    _ => {last_move = "dead"},
                }
            }
            _ => last_move = "dead",
        }
        move_count += 1;
        cur_char = gamedata[start.y][start.x] as char;
    }
    return (move_count, xray, pipe);
}

fn find_start(gamedata: &Vec<Vec<char>>) -> Loc {
    let mut start = Loc::default();
    if let Some((y, x)) = gamedata.iter().enumerate().find_map(|(y, v)| v.iter().position(|&a| a == 'S').map(|x| (y, x))) {
        //println!("The char 'S' is located at index ({}, {})", y, x);
        start = Loc {
            x: x,
            y: y,
        };
    }
    return start;
}

fn get_gamedata(filename: String) -> Vec<Vec<char>> {
    let s_gamedata = get_file(filename);
    let mut gamedata:Vec<Vec<char>> = Vec::new();
    for line in s_gamedata.lines() {
        let vline:Vec<char> = line.chars().collect();
        gamedata.push(vline);
    }
    return gamedata;
}

fn get_file(filename: String) -> String {
    use std::env;
    use std::fs;
    let current_dir = env::current_dir().unwrap();
    let path = current_dir.join("..").join(filename);
    if !path.exists() {
        return " ".to_string();
    };
    let content = fs::read_to_string(path).unwrap();
    return content;
}