use std::process;

pub fn get_gamedata(filename: &str) -> Vec<String> {
    let s_gamedata = get_file(filename);
    println!(" sGameData : \n{}", s_gamedata);
    
    let gamedata:Vec<String>= s_gamedata.split(',').map(|s| s.to_string()).collect();
    return gamedata;
}


fn get_file(filename: &str) -> String {
    use std::env;
    use std::fs;
    let current_dir = env::current_dir().unwrap();
    let path = current_dir.join("..").join(filename);
    if !path.exists() {
        println!("File does not exist : {}", path.display());
        process::exit(1);
    }else{
        let content = fs::read_to_string(path).unwrap();
        return content;
    };
}

