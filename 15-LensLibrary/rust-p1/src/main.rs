mod get_inputs;
use get_inputs::*;
mod processing;
use processing::*;

fn main() {
     let gamedata = get_gamedata("GameData.dat");
     //let gamedata = get_gamedata("GameData-test.dat");

     println!("****  Part 1  *****");   //***************************************************************************************
     let mut sum_val:u32 = 0;
     for arg in &gamedata {
               let cur_val = get_hash(&arg.to_string());
               println!("Arg : {} -> Result : {}", arg, cur_val);
               sum_val += cur_val as u32;
     }
     println!("Total : {}", sum_val);

     println!("*****  Part 2  *****");   //***************************************************************************************
     //let mut boxes: [Vec<Lens>; 10] = Default::default();  // for test data
     //let mut boxes: [Vec<Lens>; 10] = array_init::array_init(|_| Vec::new());
     let mut boxes: [Vec<Lens>; 256] = array_init::array_init(|_| Vec::new());

     for arg in gamedata {
          let step = get_step(arg);

           match step.oper.as_str() {
               "=" => {
                    match label_exists(&step.label, &boxes[step.boxnum]) {
                         Some(index)=> {
                             boxes[step.boxnum][index].label = step.label.clone();
                             boxes[step.boxnum][index].focus = step.focus.clone();
                         },
                         None       => {
                             boxes[step.boxnum].push(Lens { label:step.label.clone(), focus:step.focus.clone()});
                         },
                    }
               },        
               "-" => {
                    match label_exists(&step.label, &boxes[step.boxnum]) {
                         Some(index) => { boxes[step.boxnum].remove(index);}
                         None        => {}
                    }
                    println!(" lenses in this box {:?}", boxes[step.boxnum]);
               },
               _ => {}
          }
     };
     println!("  Print Boxes ---------------------------------");
     printboxes(&boxes); 
}