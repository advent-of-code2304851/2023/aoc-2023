#[derive(Debug)]
pub struct Lens {
    pub label: String ,
    pub focus: u32,
}

pub struct Step {
    pub label: String,
    pub boxnum: usize,
    pub oper: String,
    pub focus: u32,
}

pub fn get_hash(arg: &String) -> usize {
    let mut cur_val:usize = 0; 
    for c in arg.chars() {
        cur_val += c as usize;
        cur_val = cur_val * 17usize;
        cur_val = (cur_val as f64 % 256f64) as usize;  //modulus
    } 
    return cur_val
}

pub fn get_step(arg: String) ->  Step {               //seg fault at "gzlc=3"   line 27
    let oper_i = get_oper_index(&arg);
    let templabel = arg[0..oper_i].to_string();
    let mut step = Step { 
        boxnum:    get_hash(&templabel),
        label:     templabel,
        oper:      arg[oper_i..oper_i+1].to_string(),
        focus: 0,
    };
    if step.oper == "=" {
        step.focus = arg[oper_i+1..].parse().unwrap();
    };
    println!("Input = {} : Label = {} : Box = {} : Operator = {} : Focal Length = {}", arg, step.label, step.boxnum, step.oper, step.focus  );
    return step;
}

//----- Part 2 stuff -----
pub fn label_exists(label: &String , lbox: &Vec<Lens>) -> Option<usize> {
    lbox.iter().position(|x| x.label == *label)
}

pub fn get_oper_index(oper: &String) -> usize {
    match oper.chars().position(|c| c == '=' || c == '-') {
        Some(i) => {return i;}
        None    => {println!("****** BAD DATA *******"); return 0 as usize;}
    }
}

pub fn printboxes(boxes: &[Vec<Lens>; 256]) {
    let mut total_power:u32 = 0;
    for (ib,lbox) in boxes.iter().enumerate() {
        let thisbox = ib as u32 + 1 ;
         //println!(" Box:{}",ib+1);
         for (il,lens) in lbox.iter().enumerate() {
            let slot = (il + 1) as u32;
            let current_power = thisbox * slot * lens.focus;
              total_power += current_power;
              println!(" Box:{} slot:{}  lens.label={} : lense.focus={} : current Power={} : total_power={}",ib+1,slot,lens.label, lens.focus,current_power, total_power);
         }
    }
    println!("*** Total Power is {}",total_power);
}