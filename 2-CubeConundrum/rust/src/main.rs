use std::env;
use std::fs;

fn main() {
    struct Round {
        red: u32,
        green: u32,
        blue: u32,
    }
    impl Round {
        fn new() -> Self {
            Round {
                red: 0,
                green: 0,
                blue: 0,
            }
        }
    }
    struct Game {
        id: usize,
        rounds: Vec<Round>,
        maxred: u32,
        maxgreen: u32,
        maxblue: u32,
        power: u64,
    }
    let mut part1: usize = 0;
    let mut part2: u64 = 0;
 
//-----------------------------------------------------------------------------------------
    let current_dir = env::current_dir().unwrap();
    //let filename = current_dir.join("GameData-small.txt");
    let filename = current_dir.join("GameData.txt");

    let gamedata = fs::read_to_string(filename).unwrap();
    for (linei,line) in gamedata.lines().enumerate() {
        //println!("Game : {}   Game data : {}", linei, line);
        let mut game = Game { rounds: Vec::new(), maxred : 0, maxgreen : 0, maxblue : 0, id : 0, power : 0};
        if let Some(index) = line.find(':') {
            let data_only = &line[index + 1..];
            //println!("\nGame ID : {} - Game Data : {}", linei+1, data_only);
            let grabs = data_only.split(";");
            for grab in grabs {
                //println!(" - Full Grab {:?}",grab);
                let mut round = Round::new();
                let cubes = grab.split(",");
                for cube in cubes {
                    let mut cubeparts = cube.trim().split(" ");
                    let cubecount  = cubeparts.next().unwrap();
                    let cubecolour = cubeparts.next().unwrap();
                    //println!(" - - String - _{}_ : _{}_", cubecolour, cubecount);
                    match cubecolour {
                        "red"   => round.red    = cubecount.trim().parse().unwrap(),
                        "green" => round.green  = cubecount.trim().parse().unwrap(),
                        "blue"  => round.blue   = cubecount.trim().parse().unwrap(),
                        &_    => (),
                    }
                }
                //println!(" - - R G B : {} : {} : {}", round.red, round.green, round.blue);
                game.rounds.push(round);
            }
        }
        game.id = linei + 1;
        game.maxred = game.rounds.iter().map(|s| s.red).max().unwrap();
        game.maxgreen = game.rounds.iter().map(|s| s.green).max().unwrap();
        game.maxblue = game.rounds.iter().map(|s| s.blue).max().unwrap();
        for test in game.rounds {
            println!(" + + R G B : {} : {} : {}", test.red, test.green, test.blue);
        }
        println!("Max  R G B : {} : {} : {}", game.maxred, game.maxgreen, game.maxblue);
        //her is where I can .push(game)  to a games vec later if I want to.
        
        let testred: u32 = 12;
        let testgreen: u32 = 13;
        let testblue: u32 = 14;
        
        if game.maxred <= testred && game.maxgreen <= testgreen && game.maxblue <= testblue {
            println!("Game {} would have worked with criteria RGB 12,13,14.", linei+1);
            part1 += linei+1;
        }
        game.power = game.maxred as u64 * game.maxgreen as u64 * game.maxblue as u64;
        part2 += game.power;

        println!("The Part 1 solution (sum of sucessfull game IDs) is : {}\n", part1 );
        println!("The Part 2 solution (power of sucessfull games ) is : {}\n", part2 );
    }
}

