//use std::{path::PathBuf, io::BufRead};

fn main() {
    use std::env;
    use std::fs;
    //use std::fs::File;
    //use std::io::{BufRead, BufReader};
    //use std::path::Path;

    let current_dir = env::current_dir().unwrap();
    //let calibdoc = current_dir.join("calibration document.txt");
    let calibdoc = current_dir.join("d1p2_input.txt");
    
    


    let mut strcalibdoc = fs::read_to_string(calibdoc).unwrap();
    
/*  This method allows for error handling.  errors can not be propogated from strings, so I have to use the file type.
    let mut file = File::options()
        .read(true)
        .open(calibdoc).unwrap();
    let mut strcalibdoc = String::new();
    match file.read_to_string(&mut strcalibdoc) {
        Err(error) => panic!("Error reading file: {:?}", error),
        _ => {},
    }
*/
 
    //------------------------------------------------------------------------------------------
    //This function call takes care of the -Part 2- stuff :
    //I am passing the String version of the calibrationdoc content,to change all the numerical words to numbers as strings, and return the updated bufRead as a String
    allnumstonumerical(&mut strcalibdoc);
    //------------------------------------------------------------------------------------------

    let mut total:i64 = 0;
    for (linei,line) in strcalibdoc.lines().enumerate() {
        let oline = line;
        print!("{}. {}", linei + 1, oline);

        let mut calibval = String::new();
        let mut lastchar = '0';
        for char in oline.chars() {
                if char >= '0' && char <= '9' {
                if calibval.len() == 0 {
                    calibval.push(char);
                    lastchar = char;
                } else {
                    lastchar = char;
                }
            }
        }
        calibval.push(lastchar);
        println!(" : {:?}",calibval);
        total = total + calibval.parse::<i64>().unwrap();
    }
    println!("The total of all claibration values is {}",total);
}
    

//------------------------------------------------------------------------------------------
// --PART 1-- check each line for numbers, 
//      concatenate first and last found number on each line to a two digit number
//      convert to Int
//      sum all Ints for whole file
//      return sum
//------------------------------------------------------------------------------------------
//fn deriveandsumcalcvalues(calibdoc:Sting)->Int {
//}



//------------------------------------------------------------------------------------------
// --PART 2-- requires that any occurences of number represented as words such as 'one' 'two' etc be replaced by numerical strings such as '1' '2'
// this function does a mas replacement of all occurences in the entire file, rather than line by line, 
// it accepts the BufRead  - converts it to String - replaces the numerical nums - returns the updated string for same processing as part 1.
//------------------------------------------------------------------------------------------

//fn allnumstonumerical(file: BufRead)->String {
fn allnumstonumerical(content: &mut String) {
    
    //fucked up combo strings cuz they bitches
    /*
    content.replace_range(.., content.replace("oneight", "18").as_str());
    content.replace_range(.., content.replace("twone", "21").as_str());
    content.replace_range(.., content.replace("threeight", "38").as_str());
    content.replace_range(.., content.replace("fiveight", "58").as_str());
    content.replace_range(.., content.replace("sevenine", "79").as_str());
    content.replace_range(.., content.replace("eightwo", "82").as_str());
    content.replace_range(.., content.replace("eighthree", "83").as_str());
    content.replace_range(.., content.replace("eighthree", "83").as_str());
    */

    //Normal number searches - updated to combo case.  
    content.replace_range(.., content.replace("zero", "0o").as_str());
    content.replace_range(.., content.replace("one", "o1e").as_str());
    content.replace_range(.., content.replace("two", "t2o").as_str());
    content.replace_range(.., content.replace("three", "t3e").as_str());
    content.replace_range(.., content.replace("four", "4").as_str());
    content.replace_range(.., content.replace("five", "5e").as_str());
    content.replace_range(.., content.replace("six", "6").as_str());
    content.replace_range(.., content.replace("seven", "7n").as_str());
    content.replace_range(.., content.replace("eight", "e8t").as_str());
    content.replace_range(.., content.replace("nine", "9e").as_str());
 
}


