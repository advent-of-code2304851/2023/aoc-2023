fn main() {
    use std::fs::File;
    use std::io::{BufRead, BufReader};
    use std::env;
    //use std::path::Path;

    let current_dir = env::current_dir().unwrap();
    let calibdoc = current_dir.join("calibration document.txt");
    //let calibdoc = current_dir.join("calibration document small.txt");

    let file = File::options()
        .read(true)
        .open(calibdoc).unwrap();
    let reader = BufReader::new(file);

    //read the file line by line using the lines() iterator from BufRead.
    let mut total:i32 = 0;
    for (linei,line) in reader.lines().enumerate() {
        let oline = line.unwrap();
        println!("{}. {}", linei + 1, oline);

//        for char in oline.chars() {
        let mut calibval = String::new();
        let mut lastchar = '0';
//      for (i, char) in oline.chars().enumerate() {
        for char in oline.chars() {
                if char >= '0' && char <= '9' {
                //println!("The character {} at index {} is between 0 and 9",char,i);
                if calibval.len() == 0 {
                    calibval.push(char);
                    lastchar = char;
                } else {
                    lastchar = char;
                }
            }
        }
        calibval.push(lastchar);
        println!("the calibration value for this record is {:?}",calibval);
        total = total + calibval.parse::<i32>().unwrap();
    }
    println!("The total of all claibration values is {}",total);
}
    