
--- Day 1: Trebuchet?! ---

1) Create a Hello World doc to start from - open it in vscode.
2) figure out how to open a text file and read line by line.
2) What kind of string, and how to access it.
3) How to scan search the string for numbers vs chars left to right and vice versa.
4) concatenate first num and last num
5) convert to Int
6) add to running sum of all Ints.


1)	(if folder does not yet exist)
	  cd g:\data\Coding\AdventOfCode\Trebuchet
	  carbo new "Rust"
	(if folder does not yet exist)
	  cd g:\data\Coding\AdventOfCode\Trebuchet
	  carbo Init "Rust"

	cd g:\data\Coding\AdventOfCode\Trebuchet\Rust
2)

use std::fs::File;

let mut file = File::options()
    .read(true)
    .write(true)
    .open("foo.txt")?;