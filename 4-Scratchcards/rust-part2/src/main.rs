fn main() {
    let games = get_file(("GameData.txt").to_string());
    
    //Stuff for part I
    let mut total_points:u32 = 0;
    let mut matches  = Vec::new();
    for (_,game) in games.lines().enumerate() {
        let (gmatches, points) = get_points(game.to_string());
        matches.push(gmatches);
        total_points += points;
    }
    println!("  Part I : Total Points is: {}\n", total_points);

    //Stuff for part II
    let mut game_coppies = vec![1; matches.len()];
    for (game_i, _) in matches.iter().enumerate() {
        let upperend = matches[game_i] as usize;
        if upperend > 0 {
            for card_count in 0..upperend {
                let i = card_count + 1 + game_i;
                game_coppies[i] += game_coppies[game_i];
            }
        }
    }
    let sum: u64= game_coppies.iter().sum();
    println!("  Part II : Sum of Game Coppies: {}\n", sum);

}

fn get_file(filename: String) -> String {
    use std::env;
    use std::fs;
    let current_dir = env::current_dir().unwrap();
    let path = current_dir.join(filename);
    let games = fs::read_to_string(path).unwrap();
    return games;
}

fn get_points(game: String) -> (u32, u32) {
    let mut iter = game.split('|');
    let sdraw = iter.next().unwrap();
    let spics = iter.next().unwrap();

    let mut draw: Vec<&str> = sdraw.trim().split_whitespace().collect();
    draw.drain(..2);
    let idraw: Vec<i32> = draw.iter().map(|draw| draw.parse().unwrap()).collect();
    
    let pics: Vec<&str> = spics.trim().split_whitespace().collect();
    let ipics: Vec<i32> = pics.iter().map(|pics| pics.parse().unwrap()).collect();

    let count = ipics.iter().filter(|&n| idraw.contains(n)).count() as u32;

    let mut points: u32 = 0;
    if count > 0 {
        points = 2u32.pow(count-1);
    }
    return (count, points);
}

