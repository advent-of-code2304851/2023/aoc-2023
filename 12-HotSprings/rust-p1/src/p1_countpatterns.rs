use crate::get_inputs::*;

pub fn get_valid_pattern_count_p1(rec:Record) -> usize {
    let mut valid_pattern_count: usize = 0;
    let grouplen = rec.grouping.len();
    let fixedspaces: usize = rec.grouping.iter().sum();
    let pad_size = rec.pattern.len() - fixedspaces - grouplen + 1;
    let pad_count = grouplen+1;
        
    let mut pads:Vec<usize> = Vec::new();
    for _ in 0..pad_count-1 {
        pads.push(0);
    }
    pads.push(pad_size);

    let pattern: String = get_image(&rec, pads.clone()); 

    //println!(" sum of pads from 1 to n : {} is : {}", pad_count, pad_size);
    if is_valid_pattern(&rec.pattern, &pattern) {
//        println!("level: {},Pad Count: {}, {:?},{}", pad_count, pads[pad_count-1], pads, pattern);  // ********************************************************************
        println!("{}", pattern);  // ********************************************************************
        valid_pattern_count += 1;
    }
    valid_pattern_count += next_pad(&rec, &mut pads,0);

    return valid_pattern_count;
}

fn next_pad(rec: &Record, pads: &mut Vec<usize>, level: usize) -> usize {
    let mut valid_pattern_count:usize = 0;
    let levelpad_max: usize = pads[level..].iter().sum();
    let padslen = pads.len();
    while pads[level] != levelpad_max {
        if level == padslen -1 {
            pads[level+1] -= 1;
            pads[level] += 1;
        } else {
            valid_pattern_count += next_pad(&rec, pads, level+1);
        }
    
        let pattern: String = get_image(&rec, pads.clone()); 
        if is_valid_pattern(&rec.pattern, &pattern) {
//            println!("level: {}, Pad Count: {}, {:?},{}", level, pads[level], pads, pattern);  // ********************************************************************
            println!("{}", pattern);  // ********************************************************************
            valid_pattern_count += 1;
        }
    }
    if level > 0 {
        pads[level-1] += 1;
        for i in level..padslen-1{
            pads[i] = 0;
        }
        pads[padslen-1] = levelpad_max -1;
    }
    return valid_pattern_count;
}

fn get_image(rec: &Record, pads:Vec<usize> ) -> String {
    let grouplen = rec.grouping.len();
    let mut pattern: String = "".to_string(); 
    let mut groups: Vec<String> = Vec::new();
    let mut fixedpad: &str;
    for i in 0..grouplen {
        if i < grouplen-1 { fixedpad = "."; } else {  fixedpad = ""; };
        let sub_pattern = ".".repeat(pads[i]) + &"#".repeat(rec.grouping[i]) + fixedpad ;
        groups.push(sub_pattern.clone()); 
        pattern = pattern + &sub_pattern;
    };
    pattern = pattern + &".".repeat(pads[pads.len()-1]) ;
    return pattern;
}

fn is_valid_pattern(s1: &String, s2: &String) -> bool {
    let cmp:String = s1.chars().zip(s2.chars()).map(|(c1, c2)| {
        if c1 == c2 {
            c1
        } else {
            '?'
        }
    }).collect();
    return s1 == &cmp
}