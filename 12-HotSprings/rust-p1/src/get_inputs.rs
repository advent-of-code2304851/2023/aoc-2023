#[derive(Debug, Clone)]
pub struct Record {
    pub pattern: String,
    pub grouping: Vec<usize>,
}

pub fn get_gamedata() ->Vec<Record> {
    let filename = "GameData.dat".to_string();
    let s_gamedata = get_file(filename);
    let mut gamedata:Vec<Record> = Vec::new();
    for line in s_gamedata.lines(){
        let bits: Vec<&str> = line.split(" ").collect();
        let row = Record {
            pattern: bits[0].to_string(),
            grouping: bits[1].split(',').map(|s| s.parse().expect("Parse error")).collect(),
        };
        gamedata.push(row);
    }
    return gamedata;
}

fn get_file(filename: String) -> String {
    use std::env;
    use std::fs;
    let current_dir = env::current_dir().unwrap();
    let path = current_dir.join("..").join(filename);
    if !path.exists() {
        println!("File does not exist");
        return " ".to_string();
    }else{
        let content = fs::read_to_string(path).unwrap();
        return content;
    };
}
