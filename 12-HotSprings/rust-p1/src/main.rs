mod get_inputs;
use get_inputs::*;
mod p1_countpatterns;
use p1_countpatterns::*;

fn main() {
    let gamedata = get_gamedata();

    let mut total_pattern_count: usize = 0;

    for rec in gamedata.clone() {
        let pattern_count:usize = get_valid_pattern_count_p1(rec.clone());
        println!("Pattern Count: {}", pattern_count.clone());
        total_pattern_count += pattern_count;
    }
    println!("Total Pattern Count: {}", total_pattern_count);

//    let shit: usize = get_valid_pattern_count_p2(&gamedata[0]);
//    println!(" part 2: {}",  shit);
}