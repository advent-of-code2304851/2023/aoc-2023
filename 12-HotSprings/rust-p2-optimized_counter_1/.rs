fn main() {
    let mut count = 0;
    let power:u32 = 25;
    let upper:u32 = 2_u32.pow(power);
    for i in 0..upper {
        if i.count_ones() == 15 {
            count += 1;
            let s: String = (0..power).map(|j| if (i & (1 << j)) != 0 {'X'} else {'_'}).collect();
            println!("Combination {}: {}", count, s);
        }
    }
}