mod get_inputs;
use get_inputs::*;
mod get_patterns; 
use get_patterns::*;
//use std::time::{Duration, Instant};
use crossterm::{execute, Result, cursor::MoveTo};
use std::io::stdout;

fn main() -> Result<()> {
    let gamedata = get_gamedata();

    let mut total_optimized: u64 = 0;
    for (i, line) in gamedata.iter().enumerate() {
        //println!("{}: {} - {:?}",i+1, line.pattern, line.grouping);
        let _ = at(0,0,format!("{}",i+1));
        let total_unknown:u32 = line.pattern.matches("?").count() as u32;
        let placed_bad:u32 = line.pattern.matches("#").count() as u32;
        let total_bad:usize = line.grouping.iter().sum();
        let unplaced_bad:u32 = total_bad as u32 - placed_bad;
        
        // to fix the seg fault when (groupings known # == pattern # count) means there is only 1 match, all ? = .
        if placed_bad == total_bad as u32{
            let only_option = line.pattern.replace('?',".");
            let _ = at(0,2,format!(" only option = {} - record count {}                       ",only_option,1));
            total_optimized += 1;
            let _ = at(0,4,format!("Running Count : {}",total_optimized));
        } else { 
            //total_bitcounter += find_matches_bincounter(total_unknown, unplaced_bad, line.pattern.clone());
            total_optimized += find_matches_optimized(total_unknown, unplaced_bad, line.clone());
            let _ = at(0,4,format!("Running Count : {}",total_optimized));
        }
    }
    let _ = at(0,5,format!("Total Optimized : {}", total_optimized));
    Ok(())
}

fn at(x:u16, y:u16, s:String) -> Result<()>  {
    execute!(stdout(), MoveTo(x, y))?;
    print!("{}",s);
    Ok(())
}