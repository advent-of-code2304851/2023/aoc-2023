//use std::thread::panicking;

use crate::Record;
use crate::at;
//use std::time::{Instant};

/*
pub fn find_matches_bincounter(total_unknown: u32, unplaced_bad:u32, pattern:String) -> u64 {
    let min = 2u128.pow(unplaced_bad) -1;
    let max = min << (total_unknown - unplaced_bad);
    //println!("min:{:b} - max:{:b}",min, max );

    let mut count: u64 = 0;
    //let start_time = Instant::now(); 
    for i in min..=max {
        if i.count_ones() == unplaced_bad {
            let s: String = (0..total_unknown).map(|j| if (i & (1 << j)) != 0 {'#'} else {'.'}).collect();
            let mut s_chars = s.chars();
            let c:char = '?';
            let test_pat: String = pattern.chars().map(|x| if x == c {  s_chars.next().unwrap_or(c)} else { x }).collect();
            if is_valid_pattern(&pattern, &test_pat) {
                //println!(" Bin Pattern : {}",test_pat);
                count += 1;
            };
        }
    }
    //let duration = start_time.elapsed();
    //println!("    bin count : {:?}",duration);
    //println!(" -combo count : {}",count);
    return count;
}
*/

pub fn find_matches_optimized(total_unknown: u32, unplaced_bad:u32, rec:Record) -> u64 {
    let min = 2u128.pow(unplaced_bad) -1;
    let max = min << (total_unknown - unplaced_bad);
    let pattern: String = rec.pattern;
    //let grouping: Vec<usize> = Vec::new();

    let mut count: u64 = 0;
    //let start_time = Instant::now(); 
    let mut i:u128 = min;
    while i <=max {
        let s: String = (0..total_unknown).map(|j| if (i & (1 << j)) != 0 {'#'} else {'.'}).collect();
        let mut s_chars = s.chars();
        let c:char = '?';
        let test_pat: String = pattern.chars().map(|x| if x == c {  s_chars.next().unwrap_or(c)} else { x }).collect();
        //println!("All test_pat : {}",test_pat);

//        if is_valid_pattern(&pattern, &test_pat) && is_valid_grouping(&test_pat, &rec.grouping) {
        if is_valid_pattern(&pattern, &test_pat) {
            let _ = at(0,1,format!("Pattern OK test_pat : {}",test_pat));
            if is_valid_grouping(&test_pat, &rec.grouping) {
                count += 1;
                let _ = at(0,2,format!("Confirmed test_pat : {}",test_pat));
            };
        };

        let inc = 2u128.pow(i.trailing_zeros());
        i += inc;
        let i_ones = i.count_ones();
        if i_ones < unplaced_bad {
            i += 2u128.pow(unplaced_bad-i_ones) -1;
        }
    }
    //let duration = start_time.elapsed();

    //println!(" -combo count : {}",count);
    return count;
}

fn is_valid_pattern(s1: &String, s2: &String) -> bool {
    let cmp:String = s1.chars().zip(s2.chars()).map(|(c1, c2)| {
        if c1 == c2 {
            c1
        } else {
            '?'
        }
    }).collect();
    return s1 == &cmp
}

fn is_valid_grouping(a: &String, b: &Vec<usize>) -> bool {
    let c: Vec<usize> = count_groups(&a);
//    println!("b : {:?} - c : {:?} - a : {}", b, c, a);
    return b == &c;
}

fn count_groups(s: &String) -> Vec<usize> {
    let mut counts = Vec::new();
    let mut count = 0;
    let c: char = '#';

    for ch in s.chars() {
        if ch == c {
            count += 1;
        } else if count > 0 {
            counts.push(count);
            count = 0;
        }
    }

    if count > 0 {
        counts.push(count);
    }

    return counts
}
