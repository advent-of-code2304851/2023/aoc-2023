use std::env;
use std::fs;
#[derive(Debug, Clone)]
pub struct Record {
    pub pattern: String,
    pub grouping: Vec<usize>,
}

pub fn get_gamedata() ->Vec<Record> {

    let mut curdir:String = "".to_string();
    match env::current_dir() {
        Ok(path) => {
            curdir = path.display().to_string();
            //println!("current dir : {}", curdir);
        },
        Err(e) => {
            println!("Failed to get current directory: {}", e);
        }
    }

    //let filename = curdir + "\\GameData.dat";
    let filename = curdir + "\\GameData-P1-SimpleTest.dat";
    //let filename = curdir + "\\GameData-P1-SimplerTest.dat";
    //println!("File: {}",filename);
    let s_gamedata = get_file(filename);
    let mut gamedata:Vec<Record> = Vec::new();
    for line in s_gamedata.lines(){
        let bits: Vec<&str> = line.split(" ").collect();
        let p1_row = Record {
            pattern: bits[0].to_string(),
            grouping: bits[1].split(',').map(|s| s.parse().expect("Parse error")).collect(),
        };
        let p2_row = Record {
            pattern: std::iter::repeat(p1_row.pattern).take(5).collect::<Vec<_>>().join("?"),
            grouping: p1_row.grouping.iter().cloned().cycle().take(p1_row.grouping.len() * 5).collect(),
        };
        gamedata.push(p2_row);
    }
    return gamedata;
}

fn get_file(filename: String) -> String {
    let current_dir = env::current_dir().unwrap();
    let path = current_dir.join("..").join(filename);
    if !path.exists() {
        println!("File does not exist");
        return " ".to_string();
    }else{
        let content = fs::read_to_string(path).unwrap();
        return content;
    };
}
