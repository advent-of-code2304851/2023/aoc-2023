For Advent of Code 2024, I want to try doing using GIT sub modules.
This way each day can be commited/pushed/pulled independently.


I can use a script like : 

```batch
@echo off
setlocal

REM Define the repository URL and directory
set REPO_URL=https://example.com/your-repository.git
set TARGET_DIR=d:\main

REM Clone the main repository
git clone %REPO_URL% %TARGET_DIR%
if %errorlevel% neq 0 (
    echo Error: Failed to clone the repository
    exit /b %errorlevel%
)

REM Change directory to the target directory
cd %TARGET_DIR%

REM Update submodules recursively
git submodule update --init --recursive
if %errorlevel% neq 0 (
    echo Error: Failed to update submodules
    exit /b %errorlevel%
)

echo Repository and submodules successfully cloned and updated: 
```

The same script in BASH :

```bash
#!/bin/bash

# Define the repository URL and directory
REPO_URL=https://example.com/your-repository.git
TARGET_DIR=/path/to/main

# Clone the main repository
git clone "$REPO_URL" "$TARGET_DIR"
if [ $? -ne 0 ]; then
    echo "Error: Failed to clone the repository"
    exit 1
fi

# Change directory to the target directory
cd "$TARGET_DIR" || exit

# Update submodules recursively
git submodule update --init --recursive
if [ $? -ne 0 ]; then
    echo "Error: Failed to update submodules"
    exit 1
fi

echo "Repository and submodules successfully cloned and updated"
```



### Using Submodules

Submodules allow you to keep a Git repository as a subdirectory of another Git repository. This way, you can manage each project as a separate repository within the main repository.

1. **Initialize the Main Repository**: Create a new Git repository or navigate to your existing main repository.

2. **Add Submodules**: For each project, add it as a submodule. For example, to add a submodule:
   
   ```bash
   git submodule add <URL_of_project_repository>
   git commit -m "Added submodule for Project X
   ```

3. **Commit and Push Changes**: Changes within each submodule are managed independently. When you commit and push changes, only the specific submodule is affected, and other submodules remain unchanged.

### Using Subtrees

Subtrees allow you to nest one repository inside another as a sub-directory, but without the complexities of submodules.

1. **Initialize the Main Repository**: Create a new Git repository or navigate to your existing main repository.

2. **Add Subtrees**: For each project, add it as a subtree. For example, to add a subtree:
   
   ```bash
   git subtree add --prefix=<path_to_project_directory> <URL_of_project_repository> <branch>
   git commit -m "Added subtree for Project X
   ```

3. **Commit and Push Changes**: Similar to submodules, you can manage changes within each subtree independently. When you commit and push changes, only the specific subtree is affected.

### Example Workflow

If you decide to use submodules or subtrees, your directory structure might look like this:

```
AdventOfCode/
├── day01/
|   ├── Part1
|   └── Part2
├── day02/
|   ├── Part1
|   └── Part2
└── day02/
    ├── Part1
    └── Part2

```

When you want to commit changes to `day01`, you navigate to its directory, make changes, commit, and push:

```bash
cd day01
# Make changes
git add .
git commit -m "Updated day01"
git push
```

This way, changes to `day01` do not affect `day02` or `day03`.

Using submodules or subtrees helps you manage multiple projects within a single repository while ensuring that changes to one project do not interfere with others.



Both submodules and subtrees allow you to incorporate multiple projects within a single repository, but they have some key differences in how they work and are managed. Here’s a breakdown:

### Submodules

- **Separate Repositories**: Each submodule is a separate repository within the main repository.

- **Version Control**: You need to commit and push changes to each submodule individually.

- **Initialization**: When cloning the main repository, you have to initialize and update each submodule separately using `git submodule init` and `git submodule update`.

- **Dependency Management**: Changes in the main repository do not automatically affect submodules. You need to manually update submodules if their versions change.

### Subtrees

- **Merged Repositories**: Subtrees integrate the content of one repository directly into a subdirectory of another repository.

- **Unified Commit**: You can commit and push changes to the main repository and subtrees together, although you can also handle them separately if needed.

- **Single Clone**: When you clone the main repository, the subtrees are automatically included. No additional initialization is required.

- **Integrated Management**: You can manage the entire repository, including subtrees, in a more integrated and streamlined way. This makes it easier to keep everything up to date.

### Comparison Table

Here’s a quick comparison for a better visual understanding:

| Feature              | Submodules                        | Subtrees                              |
| -------------------- | --------------------------------- | ------------------------------------- |
| Repository Structure | Separate repositories             | Merged into main repository           |
| Commit and Push      | Separately for each submodule     | Can be unified with main repository   |
| Initialization       | Requires manual init and update   | Automatic inclusion on clone          |
| Management           | Manual management of dependencies | Integrated and streamlined management |

Choosing between submodules and subtrees depends on your workflow and preferences. Submodules are useful if you want to keep projects more loosely coupled, while subtrees offer a more integrated approach.



### Adding a new submodule options:

When adding a submodule in Git, you typically use a URL that points to a remote repository, such as one hosted on GitHub or GitLab. However, if you want to add a local submodule, you can use a local path. Here's the syntax for adding a local submodule on a Windows PC:

1. **Navigate to the Main Repository**: Open a command prompt and navigate to the directory of your main project:
   
   ```bash
   cd d:\main
   ```
2. **Add the Submodule**: Use the following command to add the submodule. Since it is a local path, you can use a relative path:
   
   ```bash
   git submodule add /d/main/submod1
   or
   git submodule add ../submod1
   ```
3. **Commit the Changes**: After adding the submodule, commit the changes to the main repository:
   
   ```bash
   git commit -m "Added submodule submod1"
   git push
   ```

This will add the submodule located at `d:\main\submod1` to your main repository in `d:\main`.



### Cloning just one day or part exclusively

If you only want to work on a specific part, such as `AdventOfCode/day01/Part1`, and avoid cloning the entire repository with all submodules, you can use sparse-checkout to achieve this. Sparse-checkout allows you to check out only the parts of the repository you need. Here’s how you can do it:

1. **Clone the Main Repository**: First, clone the main repository without checking out any files:
   
   ```bash
   git clone --no-checkout <URL_of_main_repository> AdventOfCode
   cd AdventOfCode
   ```
2. **Initialize Sparse-Checkout**: Enable the sparse-checkout feature:
   
   ```bash
   git sparse-checkout init --cone
   ```
3. **Set Sparse-Checkout Paths**: Specify the paths you want to check out. In this case, you only want `day01/Part1`:
   
   ```bash
   git sparse-checkout set day01/Part1
   ```
4. **Pull the Required Files**: Check out the specified paths:
   
   ```bash
   git checkout
   ```

### Example Commands

Here's the full set of commands put together:

```bash
git clone --no-checkout <URL_of_main_repository> AdventOfCode
cd AdventOfCode
git sparse-checkout init --cone
git sparse-checkout set day01/Part1
git checkout
```

By using sparse-checkout, you can work only on the `day01/Part1` directory without cloning the entire repository. This approach helps you manage large repositories more efficiently.


