
pub fn get_expanded(mut gd: Vec<Vec<char>>) -> Vec<Vec<char>> {
        let empty_rows: Vec<usize> = gd
            .iter()
            .enumerate()
            .filter_map(|(i, row)| {
                if row.iter().all(|&c| c == '.') {
                    Some(i)
                } else {
                    None
                }
            })
            .collect();
 
        println!("Rows with only '.': {:?}", empty_rows.clone());

        for i in (0..empty_rows.len()).rev() {
            let empty_space: Vec<char> = vec!['.'; gd[0].len()];
            gd.insert(empty_rows[i], empty_space);
        };
    
    let mut gd = transpose(&gd); //***************************

    let empty_rows: Vec<usize> = gd
    .iter()
    .enumerate()
    .filter_map(|(i, row)| {
        if row.iter().all(|&c| c == '.') {
            Some(i)
        } else {
            None
        }
    })
    .collect();

    println!("Rows with only '.': {:?}", empty_rows.clone());

    for i in (0..empty_rows.len()).rev() {
        let empty_space: Vec<char> = vec!['.'; gd[0].len()];
        gd.insert(empty_rows[i], empty_space);
    };

    gd = transpose(&gd); //****************************

    return gd;
}

fn transpose(matrix: &Vec<Vec<char>>) -> Vec<Vec<char>> {
    let n = matrix.len();
    let m = matrix[0].len();
    (0..m).map(|j| (0..n).map(|i| matrix[i][j]).collect()).collect()
}
