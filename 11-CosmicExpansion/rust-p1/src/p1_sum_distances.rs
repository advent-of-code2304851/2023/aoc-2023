#[derive(Debug)]
struct Loc {
    x: usize,
    y: usize,
}

pub fn get_path_sum(gd: Vec<Vec<char>>) -> usize {

    let mut galaxies: Vec<Loc> = Vec::new();
    for (y, row) in gd.iter().enumerate() {
        for (x, &cell) in row.iter().enumerate() {
            if cell == '#' {
                galaxies.push(Loc { x, y });
            }
        }
    }    
    println!("Galaxies by Loc : {:?}", galaxies);
        
    let mut sum_of_lens:usize = Default::default();
    for g1 in 0..galaxies.len() - 1 {
        for g2 in g1+1..galaxies.len() {
            let len = diff(galaxies[g1].y, galaxies[g2].y) + diff(galaxies[g1].x, galaxies[g2].x);
            sum_of_lens += len;
            println!(" from: {} to: {} => {:?} to {:?} => len:{} ",g1,g2,galaxies[g1],galaxies[g2],len);
        }
    }
    println!(" sum of lens : {}",sum_of_lens);
    return sum_of_lens;
}

fn diff(a1: usize, a2: usize) -> usize {
    if a1 > a2 {
        a1 - a2
    } else {
        a2 - a1
    }
}

