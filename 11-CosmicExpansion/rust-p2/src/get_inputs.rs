
pub fn get_gamedata() ->Vec<Vec<char>> {
    //let filename = "GamgeData-P1-Test.dat".to_string();
    let filename = "GameData.dat".to_string();
    let s_gamedata = get_file(filename);
    let mut gamedata:Vec<Vec<char>> = Vec::new();
    for line in s_gamedata.lines() {
        let vline:Vec<char> = line.chars().collect();
        gamedata.push(vline);
    }
    return gamedata;
}

fn get_file(filename: String) -> String {
    use std::env;
    use std::fs;
    let current_dir = env::current_dir().unwrap();
    let path = current_dir.join("..").join(filename);
    if !path.exists() {
        return " ".to_string();
    };
    let content = fs::read_to_string(path).unwrap();
    return content;
}
