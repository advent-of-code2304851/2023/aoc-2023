mod get_inputs;
mod expand_gamedata;
mod p1_sum_distances;

use get_inputs::get_gamedata;
use expand_gamedata::get_expanded;
use p1_sum_distances::get_path_sum;

fn main() {
    let mut gamedata = get_gamedata();

    for line in gamedata.clone() {
        let s_line: String = line.into_iter().collect();
        println!("{:?}",s_line);
    }

    gamedata = get_expanded(gamedata.clone());

    for line in gamedata.clone() {
        let s_line: String = line.into_iter().collect();
        println!("{:?}",s_line);
    }

    let path_sum = get_path_sum(gamedata);
    println!(" Sum Total of all Galactic Paths is : {}",path_sum)
    
}
