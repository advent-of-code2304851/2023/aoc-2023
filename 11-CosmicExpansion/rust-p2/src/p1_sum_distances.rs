#[derive(Debug)]
struct Loc {
    x: usize,
    y: usize,
}

static EXPANSION_RATE:usize = 1000000;

pub fn get_path_sum(gd: Vec<Vec<char>>) -> usize {

    let mut expanded_x_spaces:Vec<usize> = Vec::new();
    let mut expanded_y_spaces:Vec<usize> = Vec::new();

    let mut galaxies: Vec<Loc> = Vec::new();
    for (y, row) in gd.iter().enumerate() {
        for (x, &cell) in row.iter().enumerate() {
            if cell == '#' {
                galaxies.push(Loc { x, y });
            }
            if y == 0 { if gd[0][x] == '*' { expanded_x_spaces.push(x); } }
            if x == 0 { if gd[y][0] == '*' { expanded_y_spaces.push(y); } }
        }
    }    
    println!("Galaxies by Loc : {:?}", galaxies);
    println!("Expanded x spaces : {:?}", expanded_x_spaces);
    println!("Expanded y spaces : {:?}", expanded_y_spaces);
        
    let mut sum_of_lens:usize = Default::default();
    for g1 in 0..galaxies.len() - 1 {
        for g2 in g1+1.. galaxies.len() {
            let x1 = galaxies[g1].x;
            let x2 = galaxies[g2].x;
            let y1 = galaxies[g1].y;
            let y2 = galaxies[g2].y;
            let mut xlen = diff(x1, x2);
            let mut bigspaces_in_rangex: usize = Default::default();
            if x1 < x2 {
                bigspaces_in_rangex = expanded_x_spaces.iter().filter(|&&num| num > x1 && num < x2).count();
            } else {
                bigspaces_in_rangex = expanded_x_spaces.iter().filter(|&&num| num > x2 && num < x1).count();
            }
            xlen = xlen + (bigspaces_in_rangex * (EXPANSION_RATE -1));

            let mut ylen = diff(y1, y2);
            let mut bigspaces_in_rangey: usize = Default::default();
            if y1 < y2 {
                bigspaces_in_rangey = expanded_y_spaces.iter().filter(|&&num| num > y1 && num < y2).count();
            } else {
                bigspaces_in_rangey = expanded_y_spaces.iter().filter(|&&num| num > y1 && num < y2).count();
            }
            ylen = ylen + (bigspaces_in_rangey * (EXPANSION_RATE -1));
      
            let len = xlen + ylen;

            sum_of_lens += len;
        }
    }
    println!(" sum of lens : {}",sum_of_lens);
    return sum_of_lens;
}

fn diff(a1: usize, a2: usize) -> usize {
    if a1 > a2 {
        a1 - a2
    } else {
        a2 - a1
    }
}
