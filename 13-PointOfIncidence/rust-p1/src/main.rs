mod get_inputs;
use get_inputs::*;
mod processing;
use processing::*;
mod processing_p2;
use processing_p2::*;


fn main() {
    let gamedata = get_gamedata();
    println!("{:?}", gamedata.len());

    let mut p1_total: usize = 0;
    for (i, game) in gamedata.iter().enumerate() {
        //print!("Index : {} - ",i);
        let mirror_val = find_mirror(game.clone());
        //println!(" Mirror value is : {}", mirror_val);
        p1_total += mirror_val;
    }
    println!("D13 P1 : Total Mirror Values : {}", p1_total);
    println!("{}", "=".repeat(50));

    let mut p2_total: usize = 0;
    for(i, game) in gamedata.iter().enumerate() {
        print!("Index : {} ---- ",i);
        let smudge_mirror = find_smudge_mirrors(&game);
        println!(" Mirror value is : {}", smudge_mirror);
        p2_total += smudge_mirror;
    }
    println!("D13 P2 Toltal smudge Mirrors : {}", p2_total);
}
