//------------------------------------------------------------------------------------------------------------
pub fn find_mirror(game: Vec<String>) -> usize {

    //let mirror = get_pairs(&game);
    let mut mirror = get_pairs(&game);
    if mirror > 0 {
        //println!("Mirror as of first pass : {:?}", mirror * 100);
        return mirror * 100
    }

    let rgame = rotate_game(&game);
    //println!("Rotate game! ******* ");
    mirror = get_pairs(&rgame);
    if mirror < 1 {
        println!("Something went wrong, no mirror found based on identified pairs! : {:?}", game);
    }
    return mirror;
}

//------------------------------------------------------------------------------------------------------------
fn get_pairs(game: &Vec<String>) -> usize {

    let pairs: Vec<usize> = game
      .windows(2)
      .enumerate()
      .filter_map(|(index, pair)| (pair[0] == pair[1]).then(|| index))
      .collect()
    ;

    let gmelen = game.len();
    let pair_count = pairs.len();
    //println!("Game : {:?} - Game Len : {} - Pair Count : {}", game, gmelen, pair_count);

    let mut mirror: usize;
    if pair_count > 0 {
        mirror = 0;
        for pair in pairs.clone() {
            let pleft = pair+1;
            let pright = gmelen - pleft;
            let plen: usize;
            if pright >= pleft {
                plen = pleft;
            } else {
                plen = pright;
            }
            //println!("Pleft : {} - Pright : {}", pleft, pright);
            //println!("Plen : {}", plen);
    
            let left: &Vec<String> = &game[pair+1-plen..pair+1].to_vec();
            let right: &Vec<String> = &game[pair+1..pair+1+plen].iter().rev().cloned().collect();

            //println!("left : {:?}", left);
            //println!("right: {:?}", right);
            if left == right {
                //println!(" Left == Right");
                mirror = pleft;
                break;
            } else {
                //println!(" Left != Right");
                mirror = 0;
            }
        }
    } else {
        mirror = 0;
    }

    return mirror;
}

//------------------------------------------------------------------------------------------------------------
pub fn rotate_game(game:&Vec<String>) -> Vec<String> {
    // Create a 2D vector (columns)
    let mut rotated_vec: Vec<Vec<char>> = vec![vec!['.'; game.len()]; game[0].len()];

    // Populate the columns from the rows
    for (row_index, row) in game.iter().enumerate() {
        for (col_index, char_value) in row.chars().enumerate() {
            rotated_vec[col_index][row_index] = char_value;
        }
    }

    // Convert the 2D vector back to strings (columns)
    let rotated_strings: Vec<String> = rotated_vec
        .iter()
        .map(|col| col.iter().collect())
        .collect();

   return rotated_strings;
}


