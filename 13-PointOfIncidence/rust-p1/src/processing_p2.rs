//------------------------------------------------------------------------------------------------------------
pub fn find_smudge_mirrors(game: &Vec<String>) -> usize {

    let mut mirror = get_smudge_pairs(&game);
    if mirror > 0 {
        //return mirror * 100
        return mirror
    }

    let rgame = crate::processing::rotate_game(&game);
    println!("Rotate game! ******* ");

    mirror = get_smudge_pairs(&rgame);
    if mirror > 0 {
        return mirror;
    } else {
        println!("Something went wrong, no mirror found based on identified pairs! : {:?}", game);
        return 0;
    }
}

fn get_smudge_pairs(game: &Vec<String>) -> usize {
 
    for (i, pair) in game.windows(2).enumerate() { 
        let s1 = pair[0].clone().replace(".","0").replace("#","1");
        let s2 = pair[1].clone().replace(".","0").replace("#","1");
        let i1 = u32::from_str_radix(&s1,2).unwrap();
        let i2 = u32::from_str_radix(&s2,2).unwrap();
        let ixor = i1 ^ i2;
        let n = s1.len();
        let sxor:String = format!("{:032b}", ixor)[32-n..].to_string();
        print!(" index : {} - pair 1 : {}:{} - xor - pair 2 : {}:{} => {}:{}", i, s1,i1, s2,i2, sxor,ixor);
        if ixor.count_ones() == 1 {
            //let power = (ixor as f64).log2() as u32;
            if let Some(si) = sxor.find("1") {
                println!(" Smudge Found at index = {}", si);
                return si;
            };
        } else { println!(""); };
        return 0;
    }
    
//    let pairs: Vec<usize> = game
//      .windows(2)
//      .enumerate()
//      .filter_map(|(index, pair)| (pair[0] == pair[1]).then(|| index))
//      .collect()
//    ;
/*
    let gmelen = game.len();
    let pair_count = pairs.len();
    println!("Game : {:?} - Game Len : {} - Pair Count : {}", game, gmelen, pair_count);

    let mut mirror: usize;
    if pair_count > 0 {
        mirror = 0;
        for pair in pairs.clone() {
            let pleft = pair+1;
            let pright = gmelen - pleft;
            let plen: usize;
            if pright >= pleft {
                plen = pleft;
            } else {
                plen = pright;
            }
            println!("Pleft : {} - Pright : {}", pleft, pright);
            println!("Plen : {}", plen);
    
            let left: &Vec<String> = &game[pair+1-plen..pair+1].to_vec();
            let right: &Vec<String> = &game[pair+1..pair+1+plen].iter().rev().cloned().collect();

            println!("left : {:?}", left);
            println!("right: {:?}", right);
            if left == right {
                println!(" Left == Right");
                mirror = pleft;
                break;
            } else {
                println!(" Left != Right");
                mirror = 0;
            }
        }
    } else {
        mirror = 0;
    }
*/
    //return mirror;
    return 5;
}

//------------------------------------------------------------------------------------------------------------
/*
fn rotate_smudge_game(game:&Vec<String>) -> Vec<String> {
    // Create a 2D vector (columns)
    let mut rotated_vec: Vec<Vec<char>> = vec![vec!['.'; game.len()]; game[0].len()];

    // Populate the columns from the rows
    for (row_index, row) in game.iter().enumerate() {
        for (col_index, char_value) in row.chars().enumerate() {
            rotated_vec[col_index][row_index] = char_value;
        }
    }

    // Convert the 2D vector back to strings (columns)
    let rotated_strings: Vec<String> = rotated_vec
        .iter()
        .map(|col| col.iter().collect())
        .collect();

   return rotated_strings;
}
*/
