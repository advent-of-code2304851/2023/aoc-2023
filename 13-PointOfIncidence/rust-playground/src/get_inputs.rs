
pub fn get_gamedata() ->Vec<Vec<String>> {
    //let filename = "GameData-P1-Test.dat".to_string();
    let filename = "GameData-P1.dat".to_string();
    let s_gamedata = get_file(filename);
    let mut gamedata:Vec<Vec<String>> = Vec::new();
    let mut record:Vec<String> = Vec::new();

    let gd_len: usize = s_gamedata.lines().count();
    //println!("{}",gd_len);

    for (i, value) in s_gamedata.lines().enumerate() {
        let i: usize = i;

        if value == "" {
            //println!(" found a blank line");
            gamedata.push(record);
            record = Vec::new();
        } else {
            //println!("index: {}  =>  value: {}",i,value);   
            record.push(value.to_string());
            if i == gd_len - 1 {gamedata.push(record.clone());};      
        };
   }
    return gamedata;
}

fn get_file(filename: String) -> String {
    use std::env;
    use std::fs;
    let current_dir = env::current_dir().unwrap();
    let path = current_dir.join("..").join(filename);
    if !path.exists() {
        println!("File does not exist");
        return " ".to_string();
    }else{
        let content = fs::read_to_string(path).unwrap();
        return content;
    };
}
