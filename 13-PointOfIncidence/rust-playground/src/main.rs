mod get_inputs;
use get_inputs::*;

fn main() {
    let gamedata = get_gamedata();

    for game in gamedata {
        //let horrizontal: uzise = get_line_of_reflection(game);

        //let mut pairs:Vec<usize> = Vec::new();
        let pairs:Vec<usize> = game
            .windows(2)
            .enumerate()
            .filter_map(|(index, pair)| (pair[0] == pair[1]).then(|| index))
            .collect();

        println!(" Number of consecutive equal Pairs : {}", pairs.len() );

        if pairs.len() > 0 {
            let gmelen = game.len();
            let pleft = pairs[0]+1;
            let pright = gmelen - pleft;
            let plen: usize;
            if pright >= pleft {
                plen = pleft;
            } else {
                plen = pright;
            }
            let left: &Vec<String> = &game[pairs[0]+1-plen..pairs[0]+1].to_vec();
            let right: &Vec<String> = &game[pairs[0]+1..pairs[0]+1+plen].iter().rev().cloned().collect();

//            println!("Game : {:?} - Game Len : {}", game, gmelen);
//            println!("Pleft : {} - Pright : {}", pleft, pright);
//            println!("Plen : {}", plen);
            println!("left : {:?}", left);
            println!("right: {:?}", right);
            if left == right {
                //return pleft
                println!(" Left == Right");
                //break;
            }
        println!("-----------------------------------------------");
        }
        
    }

}

//fn get_line_of_reflection(game) -> usize{

//}