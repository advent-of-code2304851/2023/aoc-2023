mod get_inputs;
//use std::intrinsics::breakpoint;

use get_inputs::*;
mod processing;
use processing::*;

fn main() {
    let gamedata = get_gamedata();

    let mut p1_total: usize = 0;
    for game in gamedata {
        //println!("Game Index : {} - ",i);
        let mirror_val = get_mirror(&game);
        //println!(" Mirror value is : {}", mirror_val);
        p1_total += mirror_val;
    }
    println!("{}", "=".repeat(50));
    println!("D13 P1 : Total Mirror Values : {}", p1_total);
    println!("{}", "=".repeat(50));

}
