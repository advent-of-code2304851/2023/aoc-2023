
pub fn get_gamedata() ->Vec<Vec<u32>> {
    //let filename = "GameData-P1-Test.dat".to_string();
    let filename = "GameData-P1.dat".to_string();
    let s_gamedata = get_file(filename).replace("#","1").replace(".","0");

    let mut gamedata:Vec<Vec<u32>> = Vec::new();
    let mut record:Vec<u32> = Vec::new();
    let gd_len: usize = s_gamedata.lines().count();

    for (i, value) in s_gamedata.lines().enumerate() {
        let i: usize = i;

        if value != "" {
            record.push(u32::from_str_radix(&value,2).unwrap());
            if i == gd_len - 1 {gamedata.push(record.clone());};      
        } else {
            //println!(" found a blank line");
            gamedata.push(record);
            record = Vec::new();
        };
   }
    return gamedata;
}


fn get_file(filename: String) -> String {
    use std::env;
    use std::fs;
    let current_dir = env::current_dir().unwrap();
    let path = current_dir.join("..").join(filename);
    if !path.exists() {
        println!("File does not exist");
        return " ".to_string();
    }else{
        let content = fs::read_to_string(path).unwrap();
        return content;
    };
}

