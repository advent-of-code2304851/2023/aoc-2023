//------------------------------------------------------------------------------------------------------------
// locate mirror point - check horizontal and vertical.
pub fn get_mirror(game: &Vec<u32>) -> usize {

    //locate Horizontal mirror point.
    let mut mirror = find_mirror(&game);
    if mirror > 0 {
        return mirror * 100
    }

    //locate rotated / vertical mirror point.
    let rgame = rotate_game(&game);
    
    //locate Vertical mirror point.
    mirror = find_mirror(&rgame);
    if mirror < 1 {
        println!("Something went wrong, no mirror found based on identified pairs! : {:?}", game);
    }

    return mirror;
}

//------------------------------------------------------------------------------------------------------------
fn find_mirror(game: &Vec<u32>) -> usize {
    let gmelen = game.len();

    let mut mirror:usize = 0;
    //for (i, pair) in game.windows(2).enumerate() {
    for (i, _) in game.windows(2).enumerate() {
            let pleft = i + 1;
        let pright = gmelen - pleft;
        let plen: usize;
        if pright >= pleft {
            plen = pleft;
        } else {
            plen = pright;
        }
        let left: &Vec<u32> = &game[i+1-plen..i+1].to_vec();
        let right: &Vec<u32> = &game[i+1..i+1+plen].iter().rev().cloned().collect();

        if check_xor_sum(left,right) {
            mirror = i;
            return mirror + 1;
        }

    }

    return mirror;
}

//------------------------------------------------------------------------------------------------------------
//  Rotate the game -90
pub fn rotate_game(game:&Vec<u32>) -> Vec<u32> {

    // get the largest number in game to determin the max number of its requred for rotation.
    let num:u32 = game.iter().max().cloned().unwrap() as u32;
    let old_bits:u32 = 32 - num.leading_zeros();
    let new_bits:u32 = game.len() as u32;

    let mut rotated_game = Vec::new();
    for x in (0..old_bits).rev() {
        let mut rotated_int: u32 = 0;
        for y in 0..new_bits {
            if game[y as usize] & 2u32.pow(x as u32) != 0 {
                rotated_int +=  2u32.pow(y as u32);
            }
        }
        rotated_game.push(rotated_int);
    }
    return rotated_game;
}

// Function to check if the sum of count_ones for each a ^ b in the subset is 1
fn check_xor_sum(subset1: &Vec<u32>, subset2: &Vec<u32>) -> bool {
    subset1.iter().zip(subset2.iter())
        .map(|(&a, &b)| (a ^ b).count_ones())
        .sum::<u32>() == 1
        //.sum::<u32>() == 0
}

