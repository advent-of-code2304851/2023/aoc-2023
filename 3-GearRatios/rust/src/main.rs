fn main() {
    println!("------Start-------");
    //ascii();
    let schematic = get_file(("GameData.txt").to_string());
    //println!("{}",schematic);

    let mut map = Vec::new();
    find_symbol(&schematic, &mut map);
    //show_map(&schematic, &mut map);

    let pnums: Vec<i64> = find_partnums(&schematic, &mut map);
    let sumpnums: i64 = pnums.iter().sum();
    //println!("{:?} - Sum : {}",pnums, sumpnums);
    println!(" - Partnumber Sum : {}", sumpnums);

}

fn get_file(filename: String) -> String {
    use std::env;
    use std::fs;

    let current_dir = env::current_dir().unwrap();
    let path = current_dir.join(filename);
    let content = fs::read_to_string(path).unwrap();

    return content;
}

fn find_symbol(s: &String, map: &mut Vec<usize>) {
    let set = (48..58).chain(std::iter::once(46)).chain(std::iter::once(10)).chain(std::iter::once(13)).collect::<Vec<_>>();
    let mut index = 0;
    while let Some(i) = s[index..].chars().position(|c| !set.contains(&(c as u32))) {
        let i = index + i;
        //println!("****************Found '{}' at i {}.", s.chars().nth(i).unwrap(), i);
        map.push(i);
        index = i+1;
    }
}

//fn show_map(s: &String, map: &mut Vec<usize>){
    //for i in map {
        //println!("Symbol at {} - is {}",*i, s.chars().nth(*i).unwrap());
    //}
//}

fn find_partnums(s: &String, map: &mut Vec<usize>)->Vec<i64> {
    let reclen: usize = 142;
    let mut pnums: Vec<i64>= Vec::new();
    for i in map {
        let me = s.chars().nth(*i).unwrap();
        println!("Symbol: {} at i: {}, is: {}", me, i, s.chars().nth(*i).unwrap());
        //-------------------------
        let left = *i - 1;
        //println!("Left one of {} at i {}, is : {}", me, i, s.chars().nth(left).unwrap());
        if s.chars().nth(left).unwrap().is_numeric() {
            //println!("{} : is numeric",s.chars().nth(left).unwrap());
            pnums.push(get_partnum(&s,left));
        }
        //-------------------------
        let right = *i + 1;
        //println!("Right one of {} at i {}, is : {}", me, i, s.chars().nth(right).unwrap());
        if s.chars().nth(right).unwrap().is_numeric() {
            //println!("{} : is numeric",s.chars().nth(right).unwrap());
            pnums.push(get_partnum(&s,right));
        }
        //-------------------------
        if *i <= s.len() + reclen {
            let down = *i + reclen;
            //println!("Down one of {} is : {}", me, s.chars().nth(down).unwrap());
            if s.chars().nth(down).unwrap().is_numeric() {
                //println!("{} : is numeric",s.chars().nth(down).unwrap());
                pnums.push(get_partnum(&s,down));
            } else {
                    //-------------------------
                    let dleft = down - 1;
                    //println!("dLeft one of {} at i {}, is : {}", me, i, s.chars().nth(dleft).unwrap());
                    if s.chars().nth(dleft).unwrap().is_numeric() {
                        //println!("{} : is numeric",s.chars().nth(dleft).unwrap());
                        pnums.push(get_partnum(&s,dleft));
                    }
                   //-------------------------
                    let dright = down + 1;
                    //println!("dRight one of {} at i {}, is : {}", me, i, s.chars().nth(dright).unwrap());
                    if s.chars().nth(dright).unwrap().is_numeric() {
                        //println!("{} : is numeric",s.chars().nth(dright).unwrap());
                        pnums.push(get_partnum(&s,dright));
                    }
                    //-------------------------
            }
        }
        //-------------------------
        if *i <= s.len() - reclen {
            let up = *i - reclen;
            //println!("Up one of {} is : {}", me, s.chars().nth(up).unwrap());
            if s.chars().nth(up).unwrap().is_numeric() {
                //println!("{} : is numeric",s.chars().nth(up).unwrap());
                pnums.push(get_partnum(&s,up));
            } else {
                //---------------BAD!!!! check before beginning of string ----------
                let uleft = up - 1;
                //println!("uLeft one of {} at i {}, is : {}", me, i, s.chars().nth(uleft).unwrap());
                if s.chars().nth(uleft).unwrap().is_numeric() {
                    //println!("{} : is numeric",s.chars().nth(uleft).unwrap());
                    pnums.push(get_partnum(&s,uleft));
                }
               //-------------------------
                let uright = up + 1;
                //println!("Right one of {} at i {}, is : {}", me, i, s.chars().nth(uright).unwrap());
                if s.chars().nth(uright).unwrap().is_numeric() {
                    //println!("{} : is numeric",s.chars().nth(uright).unwrap());
                    pnums.push(get_partnum(&s,uright));
                }
                //-------------------------
            }
        }
        //-------------------------
    }
    return pnums;
}

fn get_partnum(s: &String, i: usize)-> i64 {
    let testleft = s[..i].rfind(|c: char| c == '\r' || c == '\n' || !c.is_numeric());
    let left: usize;
    if let None = testleft {
        left = 0;
    } else {
        left = testleft.unwrap()+1;
    }
    //let left = s[..i].rfind(|c: char| c == '\r' || c == '\n' || !c.is_numeric()).unwrap_or(0) + 1;
    //let index = s.find(|c: char| c == '\r' || c == '\n' || c == '.').unwrap();
    //let left = s[..i].rfind('.').unwrap()+1;
    //let left = s[..i].rfind(|c: char| !c.is_numeric()).unwrap();
    let right = s[i..].find(|c: char| c == '\r' || c == '\n' || !c.is_numeric()).unwrap()+i;
    //let right = s[..i].find('.').unwrap()+i - 2;
    //let right = s[..i].rfind(|c: char| !c.is_numeric()).unwrap();
    let partstr = &s[left..right]; 
    //println!(" - - - The number found as string  is : __{}__", partstr);
    let partnum: i64 = partstr.parse().unwrap();
    println!("The number found as integer is :{}", partnum);
    return partnum;
}
