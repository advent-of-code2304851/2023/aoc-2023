1) Read File to String.
2) Figure out how to address it by (X,Y)
3) make a function to extract nums
	Note :	s.rfind(substr), and s.find(substr) to look back and for. 
		finging "." can bookend the num
		s.substring(6, 11);
		--------------------------------
		let s = String::from("hello world");
		let substr = &s[3..7];

4) Decide wheather to :
	search for number - check adjacent chars
   or  
	search for chars - check adjacent nums.

4-1) if search for chars... easier to look.
	1) look left
	2) look right
	3) look up   ( if blank look up_left, and up_right )
	4) look down ( if blank look down_left and down_right )




Note 1:
special chars are between 33-47, 58-64, 91-96, 123-126
. = 46
numbers are between 48-57

there are four sections in ascii for special chars.
there is one range for nums and . is one char.
it is likely that it will be eiasier to search for (nums), or (notnumbs & not ".")

Note 2:
this is kinda like collision detection,, or minesweeper, does this help.
are their any nice algorythms for collision that I can use?



467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..

Actual :    
35
467 *
633
617
592
664
598
755 *
-----
4361
